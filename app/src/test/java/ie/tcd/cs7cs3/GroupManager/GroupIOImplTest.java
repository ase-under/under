package ie.tcd.cs7cs3.GroupManager;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import android.os.Handler;
import android.os.Handler.Callback;
import com.google.common.base.Optional;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOException;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOImpl;
import ie.tcd.cs7cs3.under.GroupManager.JoinRequest;
import ie.tcd.cs7cs3.under.GroupManager.LeaveRequest;
import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.storage.APIClient;
import ie.tcd.cs7cs3.under.storage.GroupPojo;
import ie.tcd.cs7cs3.under.storage.UserPojo;
import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import ie.tcd.cs7cs3.under.nearby.Nearbyer;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

@SuppressWarnings("deprecation")
public class GroupIOImplTest {

  private final static UUID uuidA = TerribleUUIDUtil.terribleUUIDFromString("userA");
  private final static UUID uuidB = TerribleUUIDUtil.terribleUUIDFromString("userB");
  private final static UserPojo userA = new UserPojo(uuidA.toString(), "genderA");
  private final static UserPojo userB = new UserPojo(uuidB.toString(), "genderB");

  private final static Group testGroup1 = new Group.Builder()
      .setID(Optional.of(1L))
      .setOwner(uuidA)
      .setCapacity(1)
      .setCreatedAt(Date.from(Instant.now()))
      .setStart(new Location("somewhere", 0, 0))
      .setDestination(new Location("elsewhere", 1, 1))
      .build();

  private GroupIOImpl groupIO;
  private APIClient mockAPIClient;
  private Nearbyer mockNearbyer;
  private Handler mockHandler;
  private Handler.Callback mockSuccessCallback;
  private Handler.Callback mockFailureCallback;

  @Before
  public void setUp() {
    mockAPIClient = Mockito.mock(APIClient.class);
    mockNearbyer = Mockito.mock(Nearbyer.class);
    mockHandler = Mockito.mock(Handler.class);
    mockSuccessCallback = Mockito.mock(Callback.class);
    mockFailureCallback = Mockito.mock(Callback.class);
    groupIO = new GroupIOImpl(mockAPIClient, mockNearbyer, mockHandler);
  }

  @Test
  public void testGroupIOImpl_startListening() {
    groupIO.startListening();
    verify(mockNearbyer).startListening();
    verify(mockHandler).post(any(Runnable.class));
  }

  @Test
  public void testGroupIOImpl_stopListening() {
    groupIO.stopListening();
    verify(mockNearbyer).stopListening();
    verify(mockHandler).removeCallbacksAndMessages(null);
  }

  @Test
  public void testGroupIOImpl_startAdvertising_AlreadyCreated()
      throws GroupIOException, IOException {
    groupIO.startAdvertising(testGroup1);
    verify(mockNearbyer).startAdvertising(testGroup1);
  }

  @Test
  public void testGroupIOImpl_startAdvertising_NotCreatedYet()
      throws GroupIOException, IOException {
    final Group testGroup2 = new Group.Builder()
        .setOwner(UUID.fromString("00000000-0000-0000-0000-000000000000"))
        .setCapacity(1)
        .setCreatedAt(Date.from(Instant.now()))
        .setStart(new Location("somewhere", 0, 0))
        .setDestination(new Location("elsewhere", 1, 1))
        .build();
    final long groupID = 123L;
    final GroupPojo testGroupPojo = GroupPojo.fromGroup(testGroup2);
    GroupPojo udpatedGroupPojoWithID = GroupPojo.fromGroup(testGroup2);
    udpatedGroupPojoWithID.groupId = groupID;
    when(mockAPIClient.create(testGroupPojo)).thenReturn(udpatedGroupPojoWithID);

    groupIO.startAdvertising(testGroup2);

    verify(mockNearbyer).startAdvertising(testGroup2);
    verify(mockAPIClient).create(testGroupPojo);
    assert (testGroup2.getId().isPresent());
    assert (testGroup2.getId().get().equals(groupID));
  }


  @Test
  public void testGroupIOImpl_stopAdvertising() throws GroupIOException, IOException {
    final GroupPojo testGroupPojo = GroupPojo.fromGroup(testGroup1);
    groupIO.stopAdvertising(testGroup1);
    verify(mockNearbyer).stopAdvertising(testGroup1);
    verify(mockAPIClient).delete(testGroupPojo);
  }

  @Test
  public void testGroupIOImpl_requestJoinGroup_justFobsItOffToNearbyer() {
    final JoinRequest joinRequest = new JoinRequest.Builder()
        .setOwnerID(testGroup1.getOwner().toString())
        .setJoiner(userA)
        .build();
    groupIO.requestJoinGroup(joinRequest, mockSuccessCallback, mockFailureCallback);
    verify(mockNearbyer).requestJoinGroup(joinRequest, mockSuccessCallback, mockFailureCallback);
  }

  @Test
  public void testGroupIOImpl_requestLeaveGroup_justFobsItOffToNearbyer() {
    final LeaveRequest leaveRequest = new LeaveRequest.Builder()
        .setOwnerID(testGroup1.getOwner().toString())
        .setLeaver(userB)
        .build();
    groupIO.requestLeaveGroup(leaveRequest, mockSuccessCallback, mockFailureCallback);
    verify(mockNearbyer).requestLeaveGroup(leaveRequest, mockSuccessCallback, mockFailureCallback);
  }

  @Test
  public void testGroupIOImpl_offline() throws GroupIOException {
    final JoinRequest joinRequest = new JoinRequest.Builder()
        .setOwnerID(testGroup1.getOwner().toString())
        .setJoiner(userB)
        .build();
    final LeaveRequest leaveRequest = new LeaveRequest.Builder()
        .setOwnerID(testGroup1.getOwner().toString())
        .setLeaver(userB)
        .build();
    groupIO.setOffline(true);
    assert(groupIO.isOffline());
    groupIO.nearbyGroups();
    groupIO.stopListening();
    groupIO.startListening();
    groupIO.startAdvertising(testGroup1);
    groupIO.stopAdvertising(testGroup1);
    groupIO.requestJoinGroup(joinRequest, mockSuccessCallback, mockFailureCallback);
    groupIO.requestLeaveGroup(leaveRequest, mockSuccessCallback, mockFailureCallback);
    verifyNoInteractions(mockAPIClient);
  }
}
