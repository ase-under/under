package ie.tcd.cs7cs3.GroupManager;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.storage.TransportMode;
import ie.tcd.cs7cs3.under.storage.UserPojo;
import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import java.util.Date;
import java.util.UUID;
import org.junit.Test;

import static org.junit.Assert.*;

public class GroupTest {

    @Test
    public void test_addAndRemoveUsers() {
        final UserPojo userA = new UserPojo(TerribleUUIDUtil.terribleUUIDFromString("userA").toString(), "female");
        final UserPojo userB = new UserPojo(TerribleUUIDUtil.terribleUUIDFromString("userB").toString(), "female");
        final UserPojo userC = new UserPojo(TerribleUUIDUtil.terribleUUIDFromString("userC").toString(), "female");
        final UserPojo userD = new UserPojo(TerribleUUIDUtil.terribleUUIDFromString("userC").toString(), "male");
        final Group g = new Group.Builder()
            .setID(Optional.of(123L))
            .setCapacity(2)
            .setCreatedAt(new Date(12345))
            .setFemaleOnly(true)
            .setStart(new Location("", -56.78, 12.34))
            .setDestination(new Location("", -12.34, 56.78))
            .setTransportMode(TransportMode.TAXI)
            .setOwner(UUID.fromString(userA.uuid))
            .build();

        // already present check
        assertFalse(g.addUserToGroup(userA));

        // capacity check
        assertTrue(g.addUserToGroup(userB));
        assertFalse(g.addUserToGroup(userC));

        // femaleOnly check
        assertTrue(g.removeUserFromGroup(userB));
        assertFalse(g.addUserToGroup(userD));

        // group started check
        g.startJourney();
        assertFalse(g.addUserToGroup(userB));
    }
}