package ie.tcd.cs7cs3.storage;

import com.google.common.collect.ImmutableList;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOException;
import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.storage.GroupPojo;
import ie.tcd.cs7cs3.under.storage.TransportMode;
import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Test;

public class GroupPojoTest {
  final static UUID uuidA = TerribleUUIDUtil.terribleUUIDFromString("test1");
  final static UUID uuidB = TerribleUUIDUtil.terribleUUIDFromString("test2");
  final static Date dateA = new Date(1587894685000L);
  final static Date dateB = new Date(1587894685001L);
  final static Location locA = new Location("", -12.34, 56.78);
  final static Location locB = new Location("", 12.34, -56.78);

  final static Group groupA = new Group.Builder()
      .setOwner(uuidA)
      .setCreatedAt(dateA)
      .setStart(locA)
      .setDestination(locB)
      .setCapacity(3)
      .setFemaleOnly(true)
      .setMembers(ImmutableList.of(uuidB))
      .setTransportMode(TransportMode.TAXI)
      .build();

  final static Group groupB = new Group.Builder()
      .setOwner(uuidB)
      .setCreatedAt(dateB)
      .setStart(locB)
      .setDestination(locA)
      .setCapacity(99)
      .setTransportMode(TransportMode.BIKE) // ¯\_(ツ)_/¯
      .setFemaleOnly(false)
      .setMembers(ImmutableList.of(uuidA))
      .build();

  @Test
  public void testGroupPojo_FromGroup() throws GroupIOException {
    final GroupPojo expected = new GroupPojo();
    expected.groupId = -1;
    expected.groupState = "FORMING";
    expected.points = "MULTIPOINT ((56.78 -12.34), (-56.78 12.34))";
    expected.memberUUIDs = ImmutableList.of("5a105e8b-9d40-e132-9780-d62ea2265d8a");
    expected.createTime = 1587894685L;
    expected.depTime = 0;
    expected.restrictions = new HashMap<String, Integer>() {{
      put("MaxPeople", 3);
      put("FemaleOnly", 1);
      put("TransportMode", TransportMode.TAXI.ordinal());
    }};

    final GroupPojo actual = GroupPojo.fromGroup(groupA);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void testGroupPojo_ToGroup() throws GroupIOException {
    final GroupPojo groupPojo = new GroupPojo();
    groupPojo.groupId = -1;
    groupPojo.groupState = "FORMING";
    groupPojo.points = "MULTIPOINT ((56.78 -12.34), (-56.78 12.34))";
    groupPojo.memberUUIDs = ImmutableList.of("5a105e8b-9d40-e132-9780-d62ea2265d8a");
    groupPojo.createTime = 1587894685L;
    groupPojo.depTime = 0;
    groupPojo.restrictions = new HashMap<String, Integer>() {{
      put("MaxPeople", 3);
      put("FemaleOnly", 1);
      put("TransportMode", TransportMode.TAXI.ordinal());
    }};

    final Group actual = groupPojo.toGroup();
    Assert.assertEquals(groupA, actual);
  }
}
