package ie.tcd.cs7cs3.util;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.util.GeometryUtil;
import java.util.List;
import org.junit.Test;
import org.locationtech.jts.io.ParseException;

public class GeometryUtilTest {

  private static final Location[] testLocations = {
      new Location("", 1, 2),
      new Location("", 3, 4)
  };

  private static final String testMultipointWKT = "MULTIPOINT ((2 1), (4 3))";

  @Test
  public void testGeometryUtil_locationsToString() {
    final String actual = GeometryUtil.locationsToString(testLocations);
    assertEquals(testMultipointWKT, actual);
  }

  @Test
  public void testGeometryUtil_stringToLocations() throws ParseException {
    final Location[] actual = GeometryUtil.stringToLocations(testMultipointWKT)
        .toArray(new Location[0]);
    assertArrayEquals(testLocations, actual);
  }
}
