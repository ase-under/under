package ie.tcd.cs7cs3.util;

import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import java.util.UUID;
import org.junit.Test;
import org.locationtech.jts.util.Assert;

// Even though it's a terrible idea, should at least make sure it works.
public class TerribleUUIDUtilTest {
  @Test
  public void testTerribleUUIDFromString() {
    final String input = "foo@bar";
    final UUID expected = UUID.fromString("cca21031-1c3c-af70-e4a3-35aad6fa1047");
    final UUID actual = TerribleUUIDUtil.terribleUUIDFromString(input);
    Assert.equals(expected, actual);
  }
}
