package ie.tcd.cs7cs3.under.appViews.activities;

import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class UserHomeTest {
    private UserHome userHome= new UserHome();
    private Point point=null;

    @Test
    public void onMapReady() {

    }

    @Test
    public void onMapClick() {
    }

    @Test
    public void onNavigationItemSelected() {
    }

    @Test
    public void test_getDestinationPoint_class() {
        point = userHome.getDestinationPoint(new LatLng(-6.2727432, 53.3399345));
        assertEquals(point.getClass().toString(),"class com.mapbox.geojson.Point");
    }

    @Test
    public void test_getDestinationLatitude() {
        Double lat=userHome.getDestinationLatitude(new LatLng(-6.2727432, 53.3399345));
        assertEquals(lat,-6.2727432,0.0);
    }

    @Test
    public void getDestinationLongitude() {
        Double longitude=userHome.getDestinationLongitude(new LatLng(-6.2727432, 53.3399345));
        assertEquals(longitude,53.3399345,0.0);
    }

      // TODO: fix this test
//    @Test
//    public void reverseGeocoding() throws IOException {
//
//
//        String name=userHome.reverseGeocoding(-6.2727432, 53.3399345);
//        System.out.println(name);
//    }
}