package ie.tcd.cs7cs3.under.nearby;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import android.os.Handler;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.JoinRequest;
import ie.tcd.cs7cs3.under.GroupManager.LeaveRequest;
import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.storage.UserPojo;
import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import ie.tcd.cs7cs3.under.nearby.Nearbyer.Endpoint;
import ie.tcd.cs7cs3.under.nearby.Nearbyer.State;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;

public class NearbyerTest {
  private final static Group testGroup = new Group.Builder()
      .setID(Optional.of(1L))
      .setOwner(UUID.fromString("00000000-0000-0000-0000-000000000000"))
      .setCapacity(1)
      .setCreatedAt(Date.from(Instant.now()))
      .setStart(new Location("somewhere", 0, 0))
      .setDestination(new Location("elsewhere", 1, 1))
      .build();

  private final static String testUserName = "test";
  private Nearbyer nearbyer;
  private ConnectionsClient mockConnectionsClient;
  private Moshi moshi = new Moshi.Builder().build();
  private JsonAdapter<NearbyMsg> nearbyMsgJsonAdapter = moshi.adapter(NearbyMsg.class);
  private JsonAdapter<JoinRequest> joinRequestJsonAdapter = moshi.adapter(JoinRequest.class);
  private JsonAdapter<LeaveRequest> leaveRequestJsonAdapter = moshi.adapter(LeaveRequest.class);

  @Before
  public void setUp() {
    mockConnectionsClient = mock(ConnectionsClient.class);
    nearbyer = new Nearbyer(testUserName, mockConnectionsClient);
  }

  @Test
  public void testNearbyer_nearbyGroups() {
    final Endpoint src = new Endpoint("test", "ing");
    nearbyer.addDiscoveredGroup(src, testGroup);
    final List<Group> expected = ImmutableList.of(testGroup);
    final List<Group> actual = nearbyer.nearbyGroups();
    assertEquals(expected, actual);
  }

  @Test
  public void testNearbyer_startListening() {
    final Task<Void> mockTask = (Task<Void>) mock(Task.class);
    when(mockConnectionsClient.startDiscovery(any(), any(), any())).thenReturn(mockTask);
    when(mockTask.addOnSuccessListener(any(OnSuccessListener.class))).thenReturn(mockTask);
    when(mockTask.addOnFailureListener(any(OnFailureListener.class))).thenReturn(mockTask);
    nearbyer.startListening();
    verify(mockConnectionsClient).startDiscovery(eq(Nearbyer.SERVICE_ID), any(), any());
  }

  @Test
  public void testNearbyer_stopListening() {
    nearbyer.setStateNoChange(State.LISTENING);

    nearbyer.stopListening();

    verify(mockConnectionsClient).stopAllEndpoints();
    verify(mockConnectionsClient).stopDiscovery();
    verify(mockConnectionsClient).stopAdvertising();
  }

  @Test
  public void testNearbyer_startAdvertising() {
    final Task<Void> mockTask = (Task<Void>) mock(Task.class);
    when(mockConnectionsClient.startAdvertising(any(), any(), any(), any()))
        .thenReturn(mockTask);
    when(mockTask.addOnSuccessListener(any())).thenReturn(mockTask);
    when(mockTask.addOnFailureListener(any())).thenReturn(mockTask);
    nearbyer.startAdvertising(testGroup);
    verify(mockConnectionsClient).stopDiscovery();
    verify(mockConnectionsClient).startAdvertising(
        eq(testUserName),
        eq(Nearbyer.SERVICE_ID),
        any(),
        any());
  }

  @Test
  public void testNearbyer_stopAdvertising() {
    nearbyer.setStateNoChange(State.ADVERTISING);

    nearbyer.stopAdvertising(testGroup);
    verify(mockConnectionsClient).stopAllEndpoints();
    verify(mockConnectionsClient).stopDiscovery();
    verify(mockConnectionsClient).stopAdvertising();
  }

  @Test
  public void testNearbyer_requestJoinGroup_Success() {
    final Handler.Callback onSuccess = (msg) -> true;
    final Handler.Callback onFailure = (msg) -> true;
    final UUID uuidA = TerribleUUIDUtil.terribleUUIDFromString("userA");
    final UUID uuidB = TerribleUUIDUtil.terribleUUIDFromString("userB");
    final UserPojo joiner = new UserPojo(uuidA.toString(), "test");
    final JoinRequest joinRequest = new JoinRequest(joiner, uuidB.toString());
    final NearbyMsg msg = new NearbyMsg(NearbyMsg.REQUEST_JOIN_GROUP, joinRequestJsonAdapter.toJson(joinRequest));
    final Payload expectedPayload = Payload.fromBytes(nearbyMsgJsonAdapter.toJson(msg).getBytes());

    final Task<Void> mockTask = mock(Task.class);

    when(mockConnectionsClient.sendPayload(ArgumentMatchers.<List<String>>any(), any())).thenReturn(mockTask);
    when(mockTask.addOnFailureListener(any())).thenReturn(null);
    nearbyer.requestJoinGroup(joinRequest, onSuccess, onFailure);

    ArgumentMatcher<Payload> matchesPayload = argument -> Arrays
        .equals(argument.asBytes(), expectedPayload.asBytes());
    verify(mockConnectionsClient).sendPayload(ArgumentMatchers.<List<String>>any(), argThat(matchesPayload));
  }

  @Test
  public void testNearbyer_requestLeaveGroup() {
    final Handler.Callback onSuccess = (msg) -> true;
    final Handler.Callback onFailure = (msg) -> true;
    final UUID uuidA = TerribleUUIDUtil.terribleUUIDFromString("userA");
    final UUID uuidB = TerribleUUIDUtil.terribleUUIDFromString("userB");
    final UserPojo leaver = new UserPojo(uuidA.toString(), "test");
    final LeaveRequest leaveRequest = new LeaveRequest(leaver, uuidB.toString());
    final NearbyMsg msg = new NearbyMsg(NearbyMsg.REQUEST_LEAVE_GROUP, leaveRequestJsonAdapter.toJson(leaveRequest));
    final Payload expectedPayload = Payload.fromBytes(nearbyMsgJsonAdapter.toJson(msg).getBytes());

    final Task<Void> mockTask = mock(Task.class);

    when(mockConnectionsClient.sendPayload(ArgumentMatchers.<List<String>>any(), any())).thenReturn(mockTask);
    when(mockTask.addOnFailureListener(any())).thenReturn(null);
    nearbyer.requestLeaveGroup(leaveRequest, onSuccess, onFailure);

    ArgumentMatcher<Payload> matchesPayload = argument -> Arrays
        .equals(argument.asBytes(), expectedPayload.asBytes());
    verify(mockConnectionsClient).sendPayload(ArgumentMatchers.<List<String>>any(), argThat(matchesPayload));
  }
}
