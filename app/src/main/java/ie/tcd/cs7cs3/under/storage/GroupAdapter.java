package ie.tcd.cs7cs3.under.storage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ie.tcd.cs7cs3.under.R;

public class GroupAdapter extends ArrayAdapter<GroupInfo_POJO> {

    public GroupAdapter(Context context, ArrayList<GroupInfo_POJO> groups) {
        super(context,0,groups);

    }

//    public GroupAdapter(Context applicationContext, int position, ArrayList<GroupInfo_POJO> resource) {
//        GroupInfo_POJO group = getItem(position);
//        // Check if an existing view is being reused, otherwise inflate the view
//        // Lookup view for data population
//        TextView groupId = (TextView) convertView.findViewById(R.id.groupId);
//        TextView groupState = (TextView) convertView.findViewById(R.id.groupState);
//        // Populate the data into the template view using the data object
//        groupId.setText(group.getGroupId());
//        groupState.setText(group.getGroupState());
//        // Return the completed view to render on screen
//        return convertView;
//    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        GroupInfo_POJO group = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_list_groups, parent, false);
        }
        // Lookup view for data population
        TextView groupId = (TextView) convertView.findViewById(R.id.groupId);
        TextView groupState = (TextView) convertView.findViewById(R.id.groupState);
        // Populate the data into the template view using the data object
        groupId.setText(group.getGroupId());
        groupState.setText(group.getGroupState());
        // Return the completed view to render on screen
        return convertView;
    }
}
