package ie.tcd.cs7cs3.under.appViews.activities;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import ie.tcd.cs7cs3.under.R;

public class Menu extends AppCompatActivity {

    private TextView textViewUserProfile;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button buttonOne = findViewById(R.id.logout);
        buttonOne.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("Button Clicked");
                Intent intent = new Intent(getApplicationContext(), Login.class);
                startActivity(intent);
            }
        });

        initViews();
        initObjects();
        Log.d("#911","onCreate Menu");

    }

    /**
     * This method is to initialize views
     */
    private void initViews() {
        textViewUserProfile = (TextView) findViewById(R.id.user_name);
    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        String emailFromIntent = getIntent().getStringExtra("EMAIL");
        Log.d("#911","email at drawer ="+emailFromIntent);
        textViewUserProfile.setText(emailFromIntent);
    }

}