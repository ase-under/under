package ie.tcd.cs7cs3.under.appViews.model;

public class ListModel {
    String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ListModel(String title) {
        this.title = title;
    }
}

