package ie.tcd.cs7cs3.under.storage;

import org.jetbrains.annotations.NotNull;

public enum TransportMode {
  WALK {
    @NotNull
    public String toString() {
      return "walk";
    }
  },
  BIKE {
    @NotNull
    public String toString() {
      return "bike";
    }
  },
  CAR {
    @NotNull
    public String toString() {
      return "car";
    }
  },
  TAXI {
    @NotNull
    public String toString() {
      return "taxi";
    }
  };

  private static TransportMode[] allValues = values();
  public static TransportMode fromOrdinal(int n) {
    return allValues[n];
  }
}
