package ie.tcd.cs7cs3.under.wifiP2P;

import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class P2PServer extends Thread {

  private static final String TAG = "P2PServer";

  final String MESSAGE_ECHO = "echo";

  private WifiP2PListenerActivity mActivity;
  private int mPort;
  private boolean mShutdown;
  private ServerSocket mServerSocket;

  public P2PServer(WifiP2PListenerActivity activity, int port) {
    this.mActivity = activity;
    this.mPort = port;
    this.mShutdown = false;
  }

  @Override
  public void run() {
    try {
      // TODO: figure out why this fails with EADDRINUSE
      this.mServerSocket = new ServerSocket();
      this.mServerSocket.setReuseAddress(true);
      this.mServerSocket.bind(new InetSocketAddress(mPort));
    } catch (IOException e) {
      Log.e(TAG, "starting server: " + e.getMessage());
      return;
    }
    while (!mShutdown) {
      try {
        handleOne();
      } catch (final IOException e) {
        Log.e(TAG, String.format("handling a request: %s", e.getMessage()));
      }
    }
  }

  void handleOne() throws IOException {
    final Socket sock = mServerSocket.accept();
    final InputStream inputStream = sock.getInputStream();
    final OutputStream outputStream = sock.getOutputStream();
    final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
    final BufferedWriter bo = new BufferedWriter(new OutputStreamWriter(outputStream));
    final String body = br.readLine();
    if (body == null) {
      Log.e(TAG, "empty message");
      return;
    }
    final String[] parts = body.split(" ", 2);
    final String verb = parts[0].toLowerCase();
    final String rest;
    if (parts.length > 1) {
      rest = parts[1];
    } else {
      rest = "";
    }
    switch (verb) {
      case MESSAGE_ECHO:
        handleEcho(rest, bo);
      default:
        Log.e(TAG, String.format("no handler for message: %s", body));
    }
    inputStream.close();
    outputStream.close();
  }

  public void handleEcho(final String msg, final BufferedWriter output) {
    try {
      mActivity.onEchoReceived(msg);
      output.write(msg);
    } catch (IOException e) {
      Log.i(TAG, String.format("handleEcho writing response: %s", e.getMessage()));
    }
  }

  public void shutdown() {
    this.mShutdown = true;
  }
}
