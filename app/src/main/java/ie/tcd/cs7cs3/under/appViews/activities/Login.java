package ie.tcd.cs7cs3.under.appViews.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.appViews.ActivityList;
import ie.tcd.cs7cs3.under.appViews.helpers.InputValidation;
import ie.tcd.cs7cs3.under.appViews.model.User;
import ie.tcd.cs7cs3.under.appViews.sql.DatabaseHelper;
import ie.tcd.cs7cs3.under.storage.Prefs;

import static java.lang.Boolean.TRUE;
/**
 * Created by Vishal on 04.02.2020
 */


public class Login extends AppCompatActivity implements View.OnClickListener{
    private String TAG = "Login";
    private final AppCompatActivity activity = Login.this;
    private long backPressed;
    private Toast backToast;

    private NestedScrollView nestedScrollView;

    private TextInputLayout textInputLayoutName;
    private TextInputLayout textInputLayoutPassword;

    private TextInputEditText textInputEditTextName;
    private TextInputEditText textInputEditTextPassword;

    private AppCompatButton appCompatButtonLogin;
    private AppCompatTextView textViewLinkRegister;

    private AppCompatImageView carView;
    private InputValidation inputValidation;
    private DatabaseHelper databaseHelper;

    private Boolean mUserPref;
    private Gson gson;
    private String curUserInfoStr;
    private int clickCount;
    private int welcomeClickCount;
    @Override
    public void onBackPressed()
    {
        if (backPressed+2000>System.currentTimeMillis())
        {
            backToast.cancel();
            super.onBackPressed();
            return;
        }
        else
        {
            backToast=Toast.makeText(getBaseContext(),"get press again to exit",Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressed=System.currentTimeMillis();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_login);

        initViews();
        initListeners();
        initObjects();
        gson = new Gson();
        mUserPref = Prefs.getBooleanPref(this.getApplicationContext());
    }
    /**
     * This method is to initialize views
     */
    private void initViews() {

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        textInputLayoutName = (TextInputLayout) findViewById(R.id.textInputLayoutName);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);

        //Input email and password
        textInputEditTextName = (TextInputEditText) findViewById(R.id.textInputEditTextName);
        textInputEditTextPassword = (TextInputEditText) findViewById(R.id.textInputEditTextPassword);

        //Log in
        appCompatButtonLogin = (AppCompatButton) findViewById(R.id.appCompatButtonLogin);

        //Sign Up for New user
        textViewLinkRegister = (AppCompatTextView) findViewById(R.id.textViewLinkRegister);

        carView = (AppCompatImageView) findViewById(R.id.car2);

        clickCount = 0;
        carView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCount++;
                if(clickCount%3 == 0){
                    String spyUserInfoStr;
                    Intent directIntent = new Intent(activity, UsersListActivity.class);
                    User spyUserInfo = new User();
                    spyUserInfo.setId(7);
                    spyUserInfo.setName("James");
                    spyUserInfo.setEmail("James@Bond.007");
                    spyUserInfo.setPassword("Bond");
                    spyUserInfo.setGender("Male");
                    spyUserInfoStr = gson.toJson(spyUserInfo);
                    directIntent.putExtra("CURUSERINFO", spyUserInfoStr);
                    startActivity(directIntent);
                }
                else
                    Toast.makeText(getApplicationContext(), "Click: " +clickCount%3+ " of 3 for entry", Toast.LENGTH_SHORT).show();
            }
        });
        welcomeClickCount = 0;
        AppCompatTextView welcomeView = (AppCompatTextView) findViewById(R.id.welcomeView);
        welcomeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCount++;
                if(clickCount%3 == 0){
                    Intent intent = new Intent(getApplicationContext(), ActivityList.class);
                    startActivity(intent);
                }
                else
                    Toast.makeText(getApplicationContext(), "Click: " +clickCount%3+ " of 3 for entry List", Toast.LENGTH_SHORT).show();
            }
        });
    }
    /**
     * This method is to initialize listeners
     */
    private void initListeners() {
        appCompatButtonLogin.setOnClickListener(this);
        textViewLinkRegister.setOnClickListener(this);
    }
    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        databaseHelper = new DatabaseHelper(activity);
        inputValidation = new InputValidation(activity);
     }

    /**
     * This implemented method is to listen the click on view
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.appCompatButtonLogin:
                verifyFromSQLite();
                break;
            case R.id.textViewLinkRegister:
                // Navigate to RegisterActivity
                Intent intentRegister = new Intent(getApplicationContext(), SignUp.class);
                startActivity(intentRegister);
                break;
        }
    }

     /**
     * This method is to validate the input text fields and verify login credentials from SQLite
     */
    private void verifyFromSQLite() {

        if (!inputValidation.isInputEditTextFilled(textInputEditTextName, textInputLayoutName, getString(R.string.error_message_valid_name))) {
            return;
        }
        if (!inputValidation.isInputEditTextFilled(textInputEditTextPassword, textInputLayoutPassword, getString(R.string.error_message_password))) {
            return;
        }

        if (databaseHelper.checkUser(textInputEditTextName.getText().toString().trim(), textInputEditTextPassword.getText().toString().trim())) {

            emptyInputEditText();
            User curUserInfo = databaseHelper.getUserInfo(textInputEditTextName.getText().toString());
            Log.d(TAG, "verifyFromSQLite userGender="+curUserInfo.getGender());

            Intent homeIntent = new Intent(activity, UserHome.class);
            if(curUserInfo.getGender()!=null){
                curUserInfoStr = gson.toJson(curUserInfo);
                homeIntent.putExtra("CURUSERINFO", curUserInfoStr);
            }
            Prefs.setBooleanPref(this.getApplicationContext(),TRUE);
            startActivity(homeIntent);

        } else {
            // Snack Bar to show success message that record is wrong
            Snackbar.make(nestedScrollView, getString(R.string.error_valid_name_password), Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * This method is to empty all input edit text
     */
    private void emptyInputEditText() {
//        textInputEditTextEmail.setText(null);
//        textInputEditTextPassword.setText(null);
    }
}
