package ie.tcd.cs7cs3.under.wifiP2P;

import android.os.StrictMode;
import android.widget.ArrayAdapter;
import androidx.appcompat.app.AppCompatActivity;

import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Optional;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.User;
import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.storage.TransportMode;
import ie.tcd.cs7cs3.under.wifiP2P.P2PHelper.GroupService;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;
import java.util.List;

import ie.tcd.cs7cs3.under.R;
import java.util.UUID;

public class WifiMainActivity extends AppCompatActivity implements WifiP2PListenerActivity {

  private final String TAG = "WifiMainActivity";
  final int PORT = 22222;
  Button btnOnOff, btnDiscover, btnSend;
  ListView listView;
  public TextView read_msg_box, connectionStatus;
  EditText WriteMsg;

  private P2PHelper p2pHelper;

  boolean discovering = false;
  Handler mHandler;

  P2PServer p2pServer;
  P2PClient p2pClient;
  Thread p2pServerThread;
  Thread p2pClientThread;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    allowingNetworkOnMainThreadIsBadButWeDoItAnyway();
    setContentView(R.layout.main_wifip2p_activity);
    initialWork();
    exqListener();
    startDemoGroup();
  }

// TODO: use this again instead?
//  Handler handler = new Handler(new Handler.Callback() {
//    @Override
//    public boolean handleMessage(Message msg) {
//      switch (msg.what) {
//        case MESSAGE_READ:
//          byte[] readBuff = (byte[]) msg.obj;
//          String tempMsg = new String(readBuff, 0, msg.arg1);
//          read_msg_box.setText(tempMsg);
//          break;
//      }
//      return true;
//
//    }
//  });

  @SuppressWarnings({"MissingPermission"})
  private void exqListener() {
    btnOnOff.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (p2pHelper.isWifiEnabled()) {
          Log.d(TAG, "wifi disabled");
          p2pHelper.disableWifi();
          btnOnOff.setText("enable wifi");
        } else {
          Log.d(TAG, "wifi enabled");
          p2pHelper.enableWifi();
          btnOnOff.setText("disable wifi");
        }
      }
    });

    btnDiscover.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (discovering) {
          discovering = false;
          p2pHelper.cancelDiscoverServices();
          mHandler.removeCallbacksAndMessages(null);

          btnDiscover.setText("enable discovery");
          return;
        }
        discovering = true;
        p2pHelper.discoverServices("_undergroup");
        updateGroupsInBackground();
        btnDiscover.setText("disable discovery");
      }
    });

    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final GroupService selected = (GroupService) parent.getItemAtPosition(position);
        p2pHelper
            .connectToDevice(selected.mMacAddress, new WifiP2pManager.ActionListener() {
              @Override
              public void onSuccess() {
                Toast.makeText(getApplicationContext(), "Connected to" + selected.mInstanceName,
                    Toast.LENGTH_SHORT).show();
              }

              @Override
              public void onFailure(int reason) {
                Toast.makeText(getApplicationContext(), "Not Connected to" + selected.mInstanceName,
                    Toast.LENGTH_SHORT)
                    .show();
              }
            });
      }
    });
    btnSend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (p2pClient == null) {
          Log.e(TAG, "not connected");
          return;
        }
        try {
          p2pClient.echo(WriteMsg.getText().toString());
        } catch (final IOException e) {
          Log.e(TAG, "send client echo: " + e.getMessage());
        }
      }
    });
  }

  private void initialWork() {
    btnOnOff = (Button) findViewById(R.id.onOff);
    btnDiscover = (Button) findViewById(R.id.discover);
    btnSend = (Button) findViewById(R.id.sendButton);
    listView = (ListView) findViewById(R.id.peerListView);
    read_msg_box = (TextView) findViewById(R.id.readMsg);
    WriteMsg = (EditText) findViewById(R.id.writeMsg);
    connectionStatus = (TextView) findViewById(R.id.connectionStatus);

    p2pHelper = new P2PHelper(this);
    btnSend.setEnabled(false);
  }

  void allowingNetworkOnMainThreadIsBadButWeDoItAnyway() {
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);
  }

  private void startDemoGroup() {    // just for demonstration purposes
    final Location demoStart = new Location("somewhere", -1, 1);
    final Location demoEnd = new Location("somewhere", 1, -1);
    final User demoUser = new User(
        UUID.fromString("deadbeef-dead-beef-dead-deadbeefdead"),
        50,
        -6,
        10,
        10,
        "female",
        ""
    );
    final Group demoGroup = new Group(Optional.of(1L), demoStart, demoEnd, 3, "", TransportMode.CAR, false, demoUser.getUid(), new Date(0), Optional
        .absent());
    p2pHelper.advertiseService(demoGroup, PORT);
    // TODO: figure out why this fails with EADDRINUSE
    p2pServer = new P2PServer(this, PORT);
    p2pServerThread = new Thread(p2pServer);
    p2pServerThread.start();
  }

  public void updateServicesAvailable() {
    final List<GroupService> groupServices = p2pHelper.getDiscoveredServices();
    if (groupServices.size() == 0) {
      Toast.makeText(getApplicationContext(), "No groups found", Toast.LENGTH_SHORT).show();
      return;
    }
    Toast.makeText(getApplicationContext(), String.format("%d groups found", groupServices.size()),
        Toast.LENGTH_SHORT).show();
    ArrayAdapter<GroupService> adapter = new ArrayAdapter<>(getApplicationContext(),
        android.R.layout.simple_list_item_1, groupServices);
    listView.setAdapter(adapter);
  }

  void updateGroupsInBackground() {
    if (mHandler != null) {
      mHandler.removeCallbacksAndMessages(null);
    }
    mHandler = new Handler();
    mHandler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (p2pHelper == null) {
          return;
        }
        updateServicesAvailable();
        mHandler.postDelayed(this, 1000);
      }
    }, 0);
  }

  public WifiP2pManager.ConnectionInfoListener connectionInfoListener = new WifiP2pManager.ConnectionInfoListener() {
    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo wifiP2pinfo) {
      final InetAddress groupOwnerAddress = wifiP2pinfo.groupOwnerAddress;
      if (wifiP2pinfo.groupFormed && wifiP2pinfo.isGroupOwner) {
        connectionStatus.setText("Host: " + groupOwnerAddress.getHostAddress());
      } else if (wifiP2pinfo.groupFormed) {
        connectionStatus.setText("Client of " + groupOwnerAddress.getHostAddress());
        try {
          p2pClient = new P2PClient(groupOwnerAddress, PORT);
          p2pClientThread = new Thread(p2pClient);
          p2pClientThread.start(); // TODO: thread leak?
          btnSend.setEnabled(true);
          final String response = p2pClient.echo("hello");
          Toast.makeText(getApplicationContext(),
              groupOwnerAddress.getHostAddress() + " said " + response, Toast.LENGTH_SHORT).show();
        } catch (final IOException e) {
          Log.e(TAG, String.format("saying hello: %s", e.getMessage()));
        }
      }
    }
  };

  @Override
  protected void onResume() {
    super.onResume();
    p2pHelper.registerReceiver();
//        p2PHelper.advertiseService(GROUP_GOES_HERE, PORT_GOES_HERE);
    p2pHelper.discoverServices("_undergroup");
    startDemoGroup();
  }

  @Override
  protected void onPause() {
    super.onPause();
    p2pHelper.unregisterReceiver();
    p2pHelper.cancelDiscoverServices();
    p2pHelper.cancelAdvertiseService();
    // TODO: stop server thread
  }

  @Override
  public WifiP2pManager.ConnectionInfoListener GetConnectionInfoListener() {
    return connectionInfoListener;
  }

  @Override
  public void SetConnectionStatusText(String text) {
    connectionStatus.setText(text);
  }

  @Override
  public void onEchoReceived(String msg) {
    runOnUiThread(() -> {
      read_msg_box.setText(msg);
    });
  }
}
