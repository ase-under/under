package ie.tcd.cs7cs3.under.appViews.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;
import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import ie.tcd.cs7cs3.under.locationmanager.MapsManagerActivity;
import ie.tcd.cs7cs3.under.storage.TransportMode;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOException;
import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static ie.tcd.cs7cs3.under.appViews.activities.UserHome.mGroupIOImpl;

public class GroupCreateFragment extends Fragment implements MapboxMap.OnMapClickListener, PermissionsListener, NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, AdapterView.OnItemSelectedListener  {
    String TAG = "GroupCreateFragment";
    Context mContext = getApplicationContext();
    View view;

    NavigationMapRoute navigationMapRoute;
    DirectionsRoute currentRoute;
    GroupDetailFragment groupCreateFragment_mock;
    //Map
    private MapView mapView;
    MapboxMap mapBoxMap;
    LocationComponent locationComponent;
    PermissionsManager permissionManager;
    Button startNavigationButton;

    private String mOwnerIDVal;
    private String mUserName=null;
    private String mUserSource = null;
    private String mUserDest = null;
    private String mUserGender = null;
    private String mUserTransportMode = null;
    private String mGroupCapacityAllowed = null;
    private String mGroupfemaleOnly = null;
    private Point source, destination;


    TextView ownerIDView;
    TextView tranportModeView;
    TextView srcAddressView;
    TextView destAddressView;
    TextView capacityValView;
    TextView femaleOnlyFlagView;
    TextView textView3FemaleOnly;

    static Group mCurrentGroup;

    Gson gson = null;

    private Spinner spinerTransportMode;
    private Spinner spinerGroupCapacity;
    private Spinner spinnerFemaleOnly;
    private String spinerTransportModeStr;
    private String spinerGroupCapacityStr;
    private String spinnerFemaleOnlyStr;

    private Point mUserSourcePt = null;
    private Point mUserDestPt = null;

    public Point getSource() {
        return source;
    }

    public void setSource(Point source) {
        this.source = source;

    }

    public Point getDestination() {
        return destination;
    }

    public void setDestination(Point destination) {
        this.destination = destination;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate()");
        gson = new Gson();
        mOwnerIDVal = getArguments().getString("OWNERID");
        mUserName = getArguments().getString("NAME");
        mUserGender = getArguments().getString("GENDER");
        mUserSource = getArguments().getString("SRC");
        mUserDest = getArguments().getString("DEST");
        //points are used in publishing
        mUserSourcePt = gson.fromJson(getArguments().getString("SRC_PT"), Point.class);
        mUserDestPt = gson.fromJson(getArguments().getString("DEST_PT"), Point.class);
//        mUserGender =
        Log.d(TAG,"mUserSourcePt="+mUserSourcePt.coordinates().toString()+" mUserDestPt="+mUserDestPt.coordinates().toString());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG,"onCreateView()");
        //MOCKED FOR PATH
        GroupDetailFragment groupDetailFragment= new GroupDetailFragment();
        groupDetailFragment.setDestination(mUserDestPt);
        groupDetailFragment.setSource(mUserSourcePt);
        groupCreateFragment_mock=groupDetailFragment;

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_group_create, container, false);
        ownerIDView = view.findViewById(R.id.ownerID);
        tranportModeView = view.findViewById(R.id.tranportMode);
        srcAddressView = view.findViewById(R.id.srcAddress);
        destAddressView = view.findViewById(R.id.destAddress);
        capacityValView = view.findViewById(R.id.capacityVal);
        femaleOnlyFlagView = view.findViewById(R.id.femaleOnlyFlag);

        //for dialog
        View promptsView = inflater.inflate(R.layout.alert_dialog_create_group, null);

        spinerTransportMode = (Spinner) promptsView.findViewById(R.id.spinnerTransportMode);
        spinerTransportMode.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(mContext,R.array.group_transport_mode_arrays, android.R.layout.simple_spinner_item);

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerTransportMode.setAdapter(adapter1);

        spinerGroupCapacity = (Spinner) promptsView.findViewById(R.id.spinnerCapacity);
        spinerGroupCapacity.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(mContext,R.array.group_capacity_arrays, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerGroupCapacity.setAdapter(adapter2);

        spinnerFemaleOnly = (Spinner) promptsView.findViewById(R.id.spinnerFemaleOnly);
        spinnerFemaleOnly.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(mContext,R.array.female_only_arrays, android.R.layout.simple_spinner_item);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFemaleOnly.setAdapter(adapter3);

        ownerIDView.setText(mOwnerIDVal);
        srcAddressView.setText(mUserSource);
        destAddressView.setText(mUserDest);

        textView3FemaleOnly = promptsView.findViewById(R.id.textView3);
        if(mUserGender.equals("Female")) {
            spinnerFemaleOnly.setVisibility(View.VISIBLE);
            textView3FemaleOnly.setVisibility(View.VISIBLE);
        }
        else
            spinnerFemaleOnlyStr = "False";

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("Publish",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to fields to publish group
                                mUserTransportMode = spinerTransportModeStr;
                                tranportModeView.setText(mUserTransportMode);

                                mGroupCapacityAllowed = spinerGroupCapacityStr;
                                capacityValView.setText(mGroupCapacityAllowed);

                                mGroupfemaleOnly = spinnerFemaleOnlyStr;
                                femaleOnlyFlagView.setText(mGroupfemaleOnly);

                                boolean result = false;
                                try {
                                    result = publishGroupCreated();
                                } catch (GroupIOException e) {
                                    e.printStackTrace();
                                }
                                if(result){
                                    Toast.makeText(getApplicationContext(),"Group Created Successfully! wait for others to join",Toast.LENGTH_SHORT).show();
                                }

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                                getActivity().onBackPressed();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();


        /* Map */
        Mapbox.getInstance(getActivity(), getString(R.string.access_token));
        mapView = view.findViewById(R.id.groupMapView);
        startNavigationButton = view.findViewById(R.id.appCompatUpdateGroupBtn);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        // For starting Navigation
        startNavigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                        .directionsRoute(currentRoute)
                        .shouldSimulateRoute(true)
                        .build();
                NavigationLauncher.startNavigation(getActivity(), options);
            }
        });

        // get detail about group ownerID again
        return view;
    }

    private boolean publishGroupCreated() throws GroupIOException {
        //set data and publish group.
        final Location srcLocation = new Location(mUserSource, mUserSourcePt.latitude(), mUserSourcePt.longitude());
        final Location destLocation = new Location(mUserDest, mUserDestPt.latitude(),mUserDestPt.longitude());
        Log.d(TAG,"Creating & Publishing Group: srcLocation"+srcLocation+
                " mUserSourcePt.latitude()="+mUserSourcePt.latitude()+" mUserSourcePt.longitude()"+mUserSourcePt.longitude()+
                " destLocation="+destLocation+
                " mUserDestPt.latitude()="+mUserDestPt.latitude()+" mUserDestPt.longitude()"+mUserDestPt.longitude()+
                " mGroupCapacityAllowed="+mGroupCapacityAllowed+
                " mUserTransportMode="+mUserTransportMode+
                " mGroupfemaleOnly="+mGroupfemaleOnly+
                " Owner="+mUserName);
        mCurrentGroup = new Group.Builder()
                .setStart(srcLocation)
                .setDestination(destLocation)
                .setCapacity(Integer.valueOf(mGroupCapacityAllowed))
                .setTransportMode(getTransportModeValue(mUserTransportMode))
                .setFemaleOnly(Boolean.valueOf(mGroupfemaleOnly))
                .setOwner(TerribleUUIDUtil.terribleUUIDFromString(mUserName))
                .setCreatedAt(new Date())
                .build();

        mGroupIOImpl.startAdvertising(mCurrentGroup);
        return true;
    }

    //Map start
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        return false;
    }


    private void getRoute(Point origin , Point destination) {
        Log.d(TAG,"getRoute started ");
        NavigationRoute.builder(getApplicationContext()).accessToken(Mapbox.getAccessToken()).origin(origin).destination(destination).build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        if (response.body() == null) {
                            Log.e(TAG, "No routes foundm check right user and access token");
                            return;
                        } else if (response.body().routes().size() == 0) {
                            Log.e(TAG, "No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);
                        if(navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapBoxMap);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                        Log.e(TAG, "Error: " + t.getMessage());
                    }
                });
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapBoxMap = mapboxMap;
//        this.mapBoxMap.setMinZoomPreference(15);
        this.mapBoxMap.setMaxZoomPreference(22);
        this.mapBoxMap.setMinZoomPreference(10);

        mapboxMap.setStyle(Style.LIGHT, new Style.OnStyleLoaded()
        {
            @Override
            public void onStyleLoaded(@NonNull Style style)
            {
                enableLocationComponent(style);
                addDestinationIconLayer(style);
//                Log.d("lat"+groupDetailFragment_mock.getDestination().toString());
                getRoute(groupCreateFragment_mock.getSource(),groupCreateFragment_mock.getDestination());
                // Enabling startNavigationButton
                startNavigationButton.setEnabled(true);
                mapboxMap.addOnMapClickListener(GroupCreateFragment.this);
            }
        });
    }

    private  void addDestinationIconLayer(Style style)
    {

        Log.d(TAG,"addDestinationIconLayer style="+style);
        style.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(),R.drawable.mapbox_marker_icon_default));

        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        style.addSource(geoJsonSource);

        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id","destination-source-id");

        destinationSymbolLayer.withProperties(iconImage("destination-icon-id" ),iconAllowOverlap(true),
                iconIgnorePlacement(true));

        style.addLayer(destinationSymbolLayer);
    }
    private void enableLocationComponent(Style loadedMapStyle)
    {
        // Check if permissions are enabled and if not request
        Boolean locPerm = PermissionsManager.areLocationPermissionsGranted(mContext);
        Log.d(TAG, "Location Permission enabled = " + locPerm);
        if(PermissionsManager.areLocationPermissionsGranted(mContext))
        {
            // Get an instance of the component
            locationComponent = mapBoxMap.getLocationComponent();
            // Activate with a built LocationComponentActivationOptions object
            locationComponent.activateLocationComponent(mContext, loadedMapStyle);

            // Enable to make component visible, change to false to hide device location
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        }
        else
        {
            permissionManager = new PermissionsManager(this);
            permissionManager.requestLocationPermissions(getActivity());
        }
    }

//    private  void addDestinationIconLayer(Style style)
//    {
//        Log.d(TAG,"addDestinationIconLayer style="+style);
//        style.addImage("destination-icon-id",
//                BitmapFactory.decodeResource(this.getResources(),R.drawable.mapbox_marker_icon_default));
//
//        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
//        style.addSource(geoJsonSource);
//
//        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id","destination-source-id");
//
//        destinationSymbolLayer.withProperties(iconImage("destination-icon-id" ),iconAllowOverlap(true),
//                iconIgnorePlacement(true));
//
//        style.addLayer(destinationSymbolLayer);
//    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain)
    {
        // user denies the access to location the first time and when second time you request it you can present a dialogue to explain why the access is needed
    }
    @Override
    public void onPermissionResult(boolean granted) {
        if(granted) {
            enableLocationComponent(mapBoxMap.getStyle());
        } else {
            Toast.makeText(getApplicationContext(), "Permission not granted", Toast.LENGTH_LONG).show();
//            finish();
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
    //MapEnd

    /** Overriding Mapview Lifecycle methods **/
    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(parent.getId()){
            case R.id.spinnerTransportMode:
                spinerTransportModeStr = parent.getItemAtPosition(position).toString();
                break;
            case R.id.spinnerCapacity:
                spinerGroupCapacityStr = parent.getItemAtPosition(position).toString();
                break;
            case R.id.spinnerFemaleOnly:
                spinnerFemaleOnlyStr = parent.getItemAtPosition(position).toString();
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private TransportMode getTransportModeValue(String UserTransportMode) {
        if(UserTransportMode.equals("WALK"))
            return TransportMode.WALK;
        else if(UserTransportMode.equals("BIKE"))
            return TransportMode.BIKE;
        else if(UserTransportMode.equals("CAR"))
            return TransportMode.CAR;
        else
            return TransportMode.TAXI;
    }

}