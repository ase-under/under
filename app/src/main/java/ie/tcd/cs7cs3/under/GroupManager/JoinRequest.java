package ie.tcd.cs7cs3.under.GroupManager;

import ie.tcd.cs7cs3.under.storage.UserPojo;

/**
 * JoinRequest represents a request to join a group.
 * It consists of a joiner and a group owner ID.
 */
public class JoinRequest {

  final UserPojo joiner;
  final String ownerID;
  private boolean done;

  public JoinRequest(final UserPojo joiner, final String ownerID) {
    this.joiner = joiner;
    this.ownerID = ownerID;
    this.done = false;
  }

  public UserPojo getJoiner() {
    return joiner;
  }

  public String getOwnerID() {
    return ownerID;
  }

  public boolean isDone() {
    return done;
  }

  public void done() {
    this.done = true;
  }

  public static class Builder {

    private UserPojo joiner;
    private String ownerID;

    public Builder setJoiner(final UserPojo joiner) {
      this.joiner = joiner;
      return this;
    }

    public Builder setOwnerID(final String ownerID) {
      this.ownerID = ownerID;
      return this;
    }

    public JoinRequest build() {
      return new JoinRequest(joiner, ownerID);
    }
  }
}
