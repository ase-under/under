package ie.tcd.cs7cs3.under.storage;

import com.google.common.base.Optional;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.Group.Builder;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOException;
import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.util.GeometryUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.locationtech.jts.io.ParseException;

/**
 * GroupPojo is a plain old Java object (POJO) that represents a {@link Group}.
 * It is intended for use in ser/deserialization only, and only has members that cannot be
 * represented as a string or as bytes.
 */
public class GroupPojo {

  public long groupId;
  public String groupState;
  public String points;
  public List<String> memberUUIDs;
  public long createTime;
  public long depTime;
  public Map<String, Integer> restrictions;

  public Group toGroup() throws GroupIOException {
    final Group.Builder builder = new Builder();

    Optional<Long> maybeGroupId;
    if (groupId < 0) {
      maybeGroupId = Optional.absent(); // XXX
    } else {
      maybeGroupId = Optional.of(groupId);
    }

    final List<Location> locations;
    try {
      locations = GeometryUtil.stringToLocations(this.points);
    } catch (final ParseException e) {
      throw new GroupIOException("parsing group: " + e.getMessage());
    }
    if (locations.size() == 0) {
      throw new GroupIOException("group has no points");
    }

    Location start = locations.get(0);
    // XXX: this can result in a group with start == dest
    Location dest = locations.get(locations.size() - 1);

    int maxPeople;
    try {
      maxPeople = this.restrictions.get("MaxPeople");
    } catch (final NullPointerException e) {
      maxPeople = 1;
    }

    boolean femaleOnly;
    try {
      femaleOnly = this.restrictions.get("FemaleOnly") == 1;
    } catch (final NullPointerException e) {
      femaleOnly = false;
    }

    String interval = ""; // TODO: support this
    int transportModeInt;
    try {
      transportModeInt = this.restrictions.get("TransportMode");
    } catch (final NullPointerException e) {
      transportModeInt = 0;
    }
    TransportMode transportMode = TransportMode.fromOrdinal(transportModeInt);

    UUID owner;
    try {
      owner = UUID.fromString(this.memberUUIDs.get(0));
    } catch (final IndexOutOfBoundsException e) {
      throw new GroupIOException("invalid group: has no owner");
    }

    List<UUID> members = new ArrayList<>();
    for (final String s : this.memberUUIDs.subList(1, this.memberUUIDs.size())) {
      members.add(UUID.fromString(s));
    }

    return builder
        .setID(maybeGroupId)
        .setStart(start)
        .setDestination(dest)
        .setCapacity(maxPeople)
        .setFemaleOnly(femaleOnly)
        .setInterval(interval)
        .setOwner(owner)
        .setMembers(members)
        .setTransportMode(transportMode)
        .build();
  }

  public static GroupPojo fromGroup(Group g) throws GroupIOException {
    final GroupPojo gp = new GroupPojo();

    if (g.getId().isPresent()) {
      gp.groupId = g.getId().get();
    } else {
      gp.groupId = -1L;
    }
    gp.createTime = g.getCreatedAt().getTime() / 1000;
    if (g.getDepartedAt().isPresent()) {
      gp.depTime = g.getDepartedAt().get().getTime() / 1000;
    } else {
      gp.depTime = 0;
    }
    gp.groupState = g.getState();
    gp.memberUUIDs = new ArrayList<>();
    for (final UUID member : g.getUsersOfGroup()) {
      gp.memberUUIDs.add(member.toString());
    }
    gp.restrictions = new HashMap<>();
    gp.restrictions.put("MaxPeople", g.getCapacity());
    gp.restrictions.put("FemaleOnly", g.isFemaleOnly() ? 1 : 0);
    gp.restrictions.put("TransportMode", g.getTransportMode().ordinal());
    gp.points = GeometryUtil.locationsToString(g.getStart(), g.getDestination());
    return gp;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GroupPojo groupPojo = (GroupPojo) o;
    return createTime == groupPojo.createTime &&
        depTime == groupPojo.depTime &&
        Objects.equals(groupId, groupPojo.groupId) &&
        Objects.equals(groupState, groupPojo.groupState) &&
        Objects.equals(points, groupPojo.points) &&
        Objects.equals(memberUUIDs, groupPojo.memberUUIDs) &&
        Objects.equals(restrictions, groupPojo.restrictions);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(groupId, groupState, points, memberUUIDs, createTime, depTime, restrictions);
  }

  @Override
  public String toString() {
    return "GroupPojo{" +
        "groupId=" + groupId +
        ", groupState='" + groupState + '\'' +
        ", points='" + points + '\'' +
        ", memberUUIDs=" + memberUUIDs +
        ", createTime=" + createTime +
        ", depTime=" + depTime +
        ", restrictions=" + restrictions +
        '}';
  }
}
