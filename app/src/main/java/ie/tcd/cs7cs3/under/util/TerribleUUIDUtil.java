package ie.tcd.cs7cs3.under.util;

import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class TerribleUUIDUtil {
  private final static String TAG = "TerribleUUIDUtil";

  /**
   * terribleUUIDFromString takes {@param string} as input, and creates an MD5 checksum of it.
   * It then takes the hex digest of this checksum and shoe-horns it into a UUID.
   *
   * This is a bad idea and any sane person would never do this.
   *
   * If something goes wrong, returns a random UUID.
   *
   * @param string
   * @return a terrible UUID.
   */
  public static UUID terribleUUIDFromString(final String string) {
    Log.d(TAG,"terribleUUIDFromString() string="+string);
    try {
      final MessageDigest md = MessageDigest.getInstance("MD5");
      final byte[] hash = md.digest(string.getBytes("UTF-8"));
      final StringBuilder sb = new StringBuilder();
      for (final byte b : hash) {
        sb.append(String.format("%02x", b&0xff));
      }
      final String digest = sb.toString();
      final String uuidString = String.format("%s-%s-%s-%s-%s",
          digest.substring(0, 8),
          digest.substring(8, 12),
          digest.substring(12, 16),
          digest.substring(16, 20),
          digest.substring(20, 32));
      return UUID.fromString(uuidString);
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException | StringIndexOutOfBoundsException e) {
      Log.e(TAG, "error making terrible uuid: " + e.getMessage());
      return UUID.randomUUID();
    }
  }
}
