package ie.tcd.cs7cs3.under.appViews.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import ie.tcd.cs7cs3.under.R;

public class Ratings extends AppCompatActivity {





    @Override
    protected void onCreate(@NonNull  Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);

        final RatingBar ratingRatingBar = (RatingBar) findViewById(R.id.rating_rating_bar);
        Button submitButton = (Button) findViewById(R.id.submit_rate_button);
        final TextView ratingDisplayTextView = (TextView) findViewById(R.id.rating_display_text_View);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingDisplayTextView.setText("Your rating for GROUP with GROUPOWNER ID : " +getIntent().getStringExtra("groupownerid")+" : " +ratingRatingBar.getRating());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        ratingDisplayTextView.setText("Your rating for GROUP with GROUPOWNER ID : " +getIntent().getStringExtra("groupownerid")+" SUBMITTED .");

                    }
                },2000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent= new Intent(getApplicationContext(),UserHome.class);
                        intent.putExtra("CURUSERINFO",getIntent().getStringExtra("CURUSERINFO"));
                        startActivity(intent);

                    }
                },3000);

            }
        });
    }
}
