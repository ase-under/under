package ie.tcd.cs7cs3.under.GroupManager;

public class GroupIOException extends Exception {
  public GroupIOException(final String msg) {
    super(msg);
  }
}
