package ie.tcd.cs7cs3.under.storage;

import com.google.gson.annotations.SerializedName;

public class GroupInfo_POJO {


    @SerializedName("groupId")
    private Integer groupId;

    @SerializedName("groupState")
    private String groupState;

    public GroupInfo_POJO(Integer groupId, String groupState) {
        this.groupId = groupId;
        this.groupState = groupState;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupState() {
        return groupState;
    }

    public void setGroupState(String groupState) {
        this.groupState = groupState;
    }
}
