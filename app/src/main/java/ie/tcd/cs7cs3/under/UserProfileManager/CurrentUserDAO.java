package ie.tcd.cs7cs3.under.UserProfileManager;

import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ie.tcd.cs7cs3.under.storage.UserEntity;

public interface CurrentUserDAO {
    @Insert
    void insert(final UserEntity... UserEntities);
}