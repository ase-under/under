package ie.tcd.cs7cs3.under.appViews.model;

import com.mapbox.geojson.Point;

import ie.tcd.cs7cs3.under.Location;

//list foramt of data received to set into adapter
public class groupData {
    public String groupId;
    public String groupOwnerId;
    public String tranportMode;
    public Location groupStartLoc;
    public Location groupDestLoc;
    public String capacityVal;
    public boolean femaleOnlyFlag;

    public groupData(String groupId, String groupOwnerId, String tranportMode, Location groupStartLoc, Location groupDestLoc, String capacityVal, boolean femaleOnlyFlag) {
        this.groupId = groupId;
        this.groupOwnerId = groupOwnerId;
        this.tranportMode = tranportMode;
        this.groupStartLoc = groupStartLoc;
        this.groupDestLoc = groupDestLoc;
        this.capacityVal = capacityVal;
        this.femaleOnlyFlag = femaleOnlyFlag;
    }
}


