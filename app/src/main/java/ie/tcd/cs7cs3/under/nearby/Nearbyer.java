package ie.tcd.cs7cs3.under.nearby;

import static ie.tcd.cs7cs3.under.nearby.NearbyMsg.GROUP_AVAILABLE;
import static ie.tcd.cs7cs3.under.nearby.NearbyMsg.GROUP_JOIN_APPROVED;
import static ie.tcd.cs7cs3.under.nearby.NearbyMsg.GROUP_JOIN_DENIED;
import static ie.tcd.cs7cs3.under.nearby.NearbyMsg.GROUP_LEAVE_APPROVED;
import static ie.tcd.cs7cs3.under.nearby.NearbyMsg.REQUEST_GROUP;
import static ie.tcd.cs7cs3.under.nearby.NearbyMsg.REQUEST_JOIN_GROUP;
import static ie.tcd.cs7cs3.under.nearby.NearbyMsg.REQUEST_LEAVE_GROUP;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.base.Optional;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.GroupIO;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOException;
import ie.tcd.cs7cs3.under.GroupManager.JoinRequest;
import ie.tcd.cs7cs3.under.GroupManager.LeaveRequest;
import ie.tcd.cs7cs3.under.storage.GroupPojo;
import ie.tcd.cs7cs3.under.storage.UserPojo;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.jetbrains.annotations.NotNull;

/**
 * Nearbyer implements @link{GroupIO} for the Google Nearby protocol.
 */
@SuppressLint("LogNotTimber") // no. just no.
public class Nearbyer implements GroupIO {

  /**
   * Nearbyer has 3 states: INACTIVE, LISTENING, ADVERTISING.
   *
   * INACTIVE means we are not listening or advertising.
   * LISTENING means we are actively listening for (and connecting to) available endpoints.
   * ADVERTISING means we are accepting connections from peers in the LISTENING state, but not LISTENING ourselves.
   */
  enum State {
    INACTIVE,
    LISTENING,
    ADVERTISING,
  }

  // Nearbyer starts out in an inactive state.
  private State mState = State.INACTIVE;

  private static final String TAG = "Nearbyer";

  // This is our service identifier. We only care about this and nothing else.
  static final String SERVICE_ID = "ie.tcd.cs7cs3.under.group";

  // This is the Nearby connection strategy we use.
  static final Strategy P2P_STRATEGY = Strategy.P2P_STAR;

  // This is how we are known on the network.
  private final String mUserName;

  // If we have a group, this is where it lives.
  private Optional<Group> mGroup = Optional.absent();

  // We keep a local cache of the groups we know about.
  private Map<String, Group> discoveredGroups;

  // Some various JSON ser/deser utilities here
  private final Moshi moshi = new Moshi.Builder().build();
  private final JsonAdapter<GroupPojo> groupPojoJsonAdapter = moshi.adapter(GroupPojo.class);
  private final JsonAdapter<NearbyMsg> nearbyMsgJsonAdapter = moshi.adapter(NearbyMsg.class);
  private final JsonAdapter<JoinRequest> joinRequestJsonAdapter = moshi.adapter(JoinRequest.class);
  private final JsonAdapter<LeaveRequest> leaveRequestJsonAdapter = moshi.adapter(LeaveRequest.class);
  private final JsonAdapter<UserPojo> userPojoJsonAdapter = moshi.adapter(UserPojo.class);

  public static final int SUCCESS = 0;
  public static final int NOT_CONNECTED = 1;
  public static final int BUSY = 2;

  private HandlerThread handlerThread;
  private Handler handler;

  /**
   * Callbacks for connections to other devices.
   */
  private final ConnectionLifecycleCallback mConnectionLifecycleCallback =
      new ConnectionLifecycleCallback() {
        @Override
        public void onConnectionInitiated(String endpointId, ConnectionInfo connectionInfo) {
          logD(
              String.format(
                  "onConnectionInitiated(endpointId=%s, endpointName=%s)",
                  endpointId, connectionInfo.getEndpointName()));
          Endpoint endpoint = new Endpoint(endpointId, connectionInfo.getEndpointName());
          mPendingConnections.put(endpointId, endpoint);
          Nearbyer.this.onConnectionInitiated(endpoint, connectionInfo);
        }

        @Override
        public void onConnectionResult(String endpointId, ConnectionResolution result) {
          logD(String.format("onConnectionResponse(endpointId=%s, result=%s)", endpointId, result));

          // We're no longer connecting
          mIsConnecting = false;

          if (!result.getStatus().isSuccess()) {
            logW(
                String.format(
                    "Connection failed. Received status %s.",
                    Nearbyer.toString(result.getStatus())));
            onConnectionFailed(mPendingConnections.remove(endpointId));
            return;
          }
          connectedToEndpoint(mPendingConnections.remove(endpointId));
        }

        @Override
        public void onDisconnected(String endpointId) {
          logD(String.format("onDisconnected(endpointId=%s)", endpointId));
          if (!mEstablishedConnections.containsKey(endpointId)) {
            logW("Unexpected disconnection from endpoint " + endpointId);
            return;
          }
          disconnectedFromEndpoint(mEstablishedConnections.get(endpointId));
        }
      };

  /**
   * Callbacks for payloads (bytes of data) sent from another device to us.
   */
  private final PayloadCallback mPayloadCallback =
      new PayloadCallback() {
        @Override
        public void onPayloadReceived(String endpointId, Payload payload) {
          logD(String.format("onPayloadReceived(endpointId=%s, payload=%s)", endpointId, payload));
          onReceive(mEstablishedConnections.get(endpointId), payload);
        }

        @Override
        public void onPayloadTransferUpdate(String endpointId, PayloadTransferUpdate update) {
          logD(
              String.format(
                  "onPayloadTransferUpdate(endpointId=%s, update=%s)", endpointId, update));
        }
      };

  /**
   * Our handler to Nearby Connections.
   */
  private ConnectionsClient mConnectionsClient;

  /**
   * The devices we've discovered near us.
   */
  private final Map<String, Endpoint> mDiscoveredEndpoints = new HashMap<>();

  /**
   * The devices we have pending connections to. They will stay pending until we call {@link
   * #acceptConnection(Endpoint)} or {@link #rejectConnection(Endpoint)}.
   */
  private final Map<String, Endpoint> mPendingConnections = new HashMap<>();

  /**
   * The devices we are currently connected to. For advertisers, this may be large. For discoverers,
   * there will only be one entry in this map.
   */
  private final Map<String, Endpoint> mEstablishedConnections = new HashMap<>();

  /**
   * True if we are asking a discovered device to connect to us. While we ask, we cannot ask another
   * device.
   */
  private boolean mIsConnecting = false;

  /**
   * True if we are discovering.
   */
  private boolean mIsDiscovering = false;

  /**
   * True if we are advertising.
   */
  private boolean mIsAdvertising = false;


  public Nearbyer(final String userName, final ConnectionsClient connectionsClient) {
    this.mUserName = userName;
    this.discoveredGroups = new HashMap<>();
    this.mConnectionsClient = connectionsClient;
    this.handlerThread  = new HandlerThread("nearbyerHandlerThread");
    this.handlerThread.start();
//    this.mConnectionsActivity = new ConnectionsActivity(userName, connectionsClient, mGroup);
  }

  @Override
  public boolean isOffline() {
    return true;
  }

  @Override
  public void setOffline(boolean offline) {
    // this is a no-op. nearbyer is always 'offline'
  }

  /***************************************
   * PUBLIC INTERFACE METHODS BEGIN HERE *
   ***************************************/
  @Override
  public List<Group> nearbyGroups() {
    return new ArrayList<>(discoveredGroups.values());
  }

  @Override
  public void startListening() {
    if (mState != State.LISTENING) {
      setState(State.LISTENING);
    }
  }

  @Override
  public void stopListening() {
    if (mState != State.INACTIVE) {
      setState(State.INACTIVE);
    }
  }

  @Override
  public void startAdvertising(Group myGroup) {
    this.mGroup = Optional.of(myGroup);
    if (mState != State.ADVERTISING) {
      setState(State.ADVERTISING);
    }
  }

  @Override
  public void stopAdvertising(Group myGroup) {
    this.mGroup = Optional.absent();
    if (mState != State.INACTIVE) {
      setState(State.INACTIVE);
    }
  }

  @Override
  public void requestJoinGroup(JoinRequest req, Handler.Callback onSuccess,
      Handler.Callback onFailure) {
    if (this.handler != null) {
      // we already have a request in flight
      final Message failureMsg = Message.obtain();
      failureMsg.what = BUSY;
      onFailure.handleMessage(failureMsg);
    }

    final String joinRequestStr = joinRequestJsonAdapter.toJson(req);
    final NearbyMsg joinMsg = new NearbyMsg(REQUEST_JOIN_GROUP, joinRequestStr);
    final String joinMsgStr = nearbyMsgJsonAdapter.toJson(joinMsg);
    send(joinMsgStr);

    // Wait for the response in a separate thread.
    handler = new Handler(handlerThread.getLooper(), (Callback) message -> {
      switch (message.what) {
        case SUCCESS:
          final Message successMsg = Message.obtain();
          new Handler(Looper.getMainLooper()).post(() -> {
            onSuccess.handleMessage(successMsg);
          });
          break;
        default:
          final Message failureMsg = Message.obtain();
          failureMsg.what = message.what;
          new Handler(Looper.getMainLooper()).post(() -> {
            onFailure.handleMessage(failureMsg);
          });
          break;
      }
      handler.removeCallbacksAndMessages(null);
      handler = null;
      return true;
    });
  }

  @Override
  public void requestLeaveGroup(LeaveRequest req, Handler.Callback onSuccess,
      Handler.Callback onFailure) {

    if (this.handler != null) {
      // we already have a request in flight
      final Message failureMsg = Message.obtain();
      failureMsg.what = BUSY;
      onFailure.handleMessage(failureMsg);
    }

    final String leaveRequestStr = leaveRequestJsonAdapter.toJson(req);
    final NearbyMsg leaveMsg = new NearbyMsg(REQUEST_LEAVE_GROUP, leaveRequestStr);
    final String leaveMsgStr = nearbyMsgJsonAdapter.toJson(leaveMsg);
    send(leaveMsgStr);

    // Wait for the response in a separate thread.
    handler = new Handler(handlerThread.getLooper(), (Callback) message -> {
      switch (message.what) {
        case SUCCESS:
          final Message successMsg = Message.obtain();
          new Handler(Looper.getMainLooper()).post(() -> {
            onSuccess.handleMessage(successMsg);
          });
          break;
        default:
          final Message failureMsg = Message.obtain();
          failureMsg.what = message.what;
          new Handler(Looper.getMainLooper()).post(() -> {
            onFailure.handleMessage(failureMsg);
          });
          break;
      }
      handler.removeCallbacksAndMessages(null);
      handler = null;
      return true;
    });
  }

  /**********************
  * PRIVATE STUFF BELOW  *
  ***********************/

  /**
   * setState sets the {@link State} of the application to {@param newState}. Depending on the
   * state, some cleanup actions may be performed:
   *
   * <p>* {@link State#INACTIVE}: disconnect from all endpoints, stop advertising, stop discovering.
   * <p>* {@link State#LISTENING}: disconnect from all endpoints, stop advertising, start discovering.
   * <p>* {@link State#ADVERTISING}: stop discovering, and start advertising.
   */
  private void setState(final State newState) {
    logD("setState: new state: " + newState);
    final State oldState = this.mState;
    this.mState = newState;
    switch (newState) {
      case INACTIVE:
        disconnectFromAllEndpoints();
        stopAllEndpoints();
        stopDiscovering();
        stopAdvertising();
        break;
      case LISTENING:
        disconnectFromAllEndpoints();
        stopAdvertising();
        startDiscovering();
        break;
      case ADVERTISING:
        stopDiscovering();
        startAdvertising();
        break;
      default:
        break;
    }
  }

  @Deprecated
  void setStateNoChange(final State newState) {
    this.mState = newState;
  }

  private void requestGroupInfo() {
    // TODO: also gossip information about other groups we saw in the past.
    logV("requesting group info");
    if (mGroup.isPresent()) {
      logV("i already have a group, not asking for others");
      return;
    }
    final String msgJson = nearbyMsgJsonAdapter.toJson(new NearbyMsg(REQUEST_GROUP, ""));
    send(msgJson);
  }

  /**
   * Sets the device to advertising mode. It will broadcast to other devices in discovery mode.
   * Either {@link #onAdvertisingStarted()} or {@link #onAdvertisingFailed()} will be called once
   * we've found out if we successfully entered this mode.
   */
  private void startAdvertising() {
    mIsAdvertising = true;
    final String localEndpointName = mUserName;
    logD(String.format("startAdvertising(endpointId=%s)", localEndpointName));

    AdvertisingOptions.Builder advertisingOptions = new AdvertisingOptions.Builder();
    advertisingOptions.setStrategy(P2P_STRATEGY);

    mConnectionsClient
        .startAdvertising(
            localEndpointName,
            SERVICE_ID,
            mConnectionLifecycleCallback,
            advertisingOptions.build())
        .addOnSuccessListener(
            new OnSuccessListener<Void>() {
              @Override
              public void onSuccess(Void unusedResult) {
                logV("Now advertising endpoint " + localEndpointName);
                onAdvertisingStarted();
              }
            })
        .addOnFailureListener(
            new OnFailureListener() {
              @Override
              public void onFailure(@NonNull Exception e) {
                mIsAdvertising = false;
                logW("startAdvertising() failed: " + e.getMessage());
                onAdvertisingFailed();
              }
            });
  }

  /**
   * Stops advertising.
   */
  private void stopAdvertising() {
    logD("stopAdvertising()");
    mIsAdvertising = false;
    mConnectionsClient.stopAdvertising();
  }

  /**
   * Called when advertising successfully starts.
   */
  private void onAdvertisingStarted() {
    logD("onAdvertisingStarted()");
  }

  /**
   * Called when advertising fails to start.
   */
  private void onAdvertisingFailed() {
    logD("onAdvertisingFailed()");
  }

  /**
   * Called when a pending connection with a remote endpoint is created. Use {@link ConnectionInfo}
   * for metadata about the connection (like incoming vs outgoing, or the authentication token). If
   * we want to continue with the connection, call {@link #acceptConnection(Endpoint)}. Otherwise,
   * call {@link #rejectConnection(Endpoint)}.
   */
  private void onConnectionInitiated(Endpoint endpoint, ConnectionInfo connectionInfo) {
    logD("initate connection to endpoint: " + endpoint.getId());
    acceptConnection(endpoint);
  }

  /**
   * Accepts a connection request.
   */
  private void acceptConnection(final Endpoint endpoint) {
    logD(String.format("acceptConnection(endpointId=%s endpointName=%s)", endpoint.getId(), endpoint.getName()));
    mConnectionsClient
        .acceptConnection(endpoint.getId(), mPayloadCallback)
        .addOnFailureListener(
            new OnFailureListener() {
              @Override
              public void onFailure(@NonNull Exception e) {
                logW("acceptConnection() failed: " + e.getMessage());
              }
            });
  }

  /**
   * Rejects a connection request. We never do this.
   * TODO: reject connection if our group is full or has already embarked.
   */
  private void rejectConnection(Endpoint endpoint) {
    logD(String.format("rejectConnection(endpointId=%s endpointName=%s)", endpoint.getId(), endpoint.getName()));
    mConnectionsClient
        .rejectConnection(endpoint.getId())
        .addOnFailureListener(
            new OnFailureListener() {
              @Override
              public void onFailure(@NonNull Exception e) {
                logW("rejectConnection() failed: " + e.getMessage());
              }
            });
  }

  /**
   * Sets the device to discovery mode. It will now listen for devices in advertising mode. Either
   * {@link #onDiscoveryStarted()} or {@link #onDiscoveryFailed()} will be called once we've found
   * out if we successfully entered this mode.
   */
  private void startDiscovering() {
    logD("startDiscovering()");
    mIsDiscovering = true;
    mDiscoveredEndpoints.clear();
    DiscoveryOptions.Builder discoveryOptions = new DiscoveryOptions.Builder();
    discoveryOptions.setStrategy(P2P_STRATEGY);
    final Task<Void> discoveryTask = mConnectionsClient
        .startDiscovery(
            SERVICE_ID,
            new EndpointDiscoveryCallback() {
              @Override
              public void onEndpointFound(String endpointId, DiscoveredEndpointInfo info) {
                logD(
                    String.format(
                        "onEndpointFound(endpointId=%s, serviceId=%s, endpointName=%s)",
                        endpointId, info.getServiceId(), info.getEndpointName()));

                if (!SERVICE_ID.equals(info.getServiceId())) {
                  return;
                }
                Endpoint endpoint = new Endpoint(endpointId, info.getEndpointName());
                mDiscoveredEndpoints.put(endpointId, endpoint);
                onEndpointDiscovered(endpoint);
              }

              @Override
              public void onEndpointLost(String endpointId) {
                logD(String.format("onEndpointLost(endpointId=%s)", endpointId));
              }
            },
            discoveryOptions.build());
        discoveryTask.addOnSuccessListener(
            new OnSuccessListener<Void>() {
              @Override
              public void onSuccess(Void unusedResult) {
                onDiscoveryStarted();
              }
            })
        .addOnFailureListener(
            new OnFailureListener() {
              @Override
              public void onFailure(@NonNull Exception e) {
                mIsDiscovering = false;
                logW("startDiscovering() failed: " + e.getMessage());
                onDiscoveryFailed();
              }
            });
  }

  /**
   * Stops discovery.
   */
  private void stopDiscovering() {
    logD("stopDiscovering()");
    mIsDiscovering = false;
    mConnectionsClient.stopDiscovery();
  }

  /**
   * Called when discovery successfully starts. Override this method to act on the event.
   */
  private void onDiscoveryStarted() {
    logD("discovery started");
  }

  /**
   * Called when discovery fails to start. Override this method to act on the event.
   */
  private void onDiscoveryFailed() {
    logD("discovery failed");
  }

  /**
   * Called when a remote endpoint is discovered. To connect to the device, call {@link
   * #connectToEndpoint(Endpoint)}.
   */
  private void onEndpointDiscovered(Endpoint endpoint) {
    logD("discovered endpoint: " + endpoint.getId());
    if (mGroup.isPresent()) {
      logD("not connecting to endpoint: " + endpoint.getId());
      logD("already advertising");
      return;
    }
    connectToEndpoint(endpoint);
  }

  /**
   * Disconnects from the given endpoint.
   */
  protected void disconnect(Endpoint endpoint) {
    logD("disconnecting from endpoint: " + endpoint.getId());
    mConnectionsClient.disconnectFromEndpoint(endpoint.getId());
    mEstablishedConnections.remove(endpoint.getId());
  }

  /**
   * Disconnects from all currently connected endpoints.
   */
  private void disconnectFromAllEndpoints() {
    logD("disconnecting from all endpoints");
    for (Endpoint endpoint : mEstablishedConnections.values()) {
      mConnectionsClient.disconnectFromEndpoint(endpoint.getId());
    }
    mEstablishedConnections.clear();
  }

  /**
   * Resets and clears all state in Nearby Connections.
   */
  private void stopAllEndpoints() {
    logD("stopping all endpoints");
    mConnectionsClient.stopAllEndpoints();
    mIsAdvertising = false;
    mIsDiscovering = false;
    mIsConnecting = false;
    mDiscoveredEndpoints.clear();
    mPendingConnections.clear();
    mEstablishedConnections.clear();
  }

  /**
   * Sends a connection request to the endpoint. Either {@link #onConnectionInitiated(Endpoint,
   * ConnectionInfo)} or {@link #onConnectionFailed(Endpoint)} will be called once we've found out
   * if we successfully reached the device.
   */
  private void connectToEndpoint(final Endpoint endpoint) {
    logV("Sending a connection request to endpoint " + endpoint);
    // Mark ourselves as connecting so we don't connect multiple times
    mIsConnecting = true;

    // Ask to connect
    mConnectionsClient
        .requestConnection(mUserName, endpoint.getId(), mConnectionLifecycleCallback)
        .addOnFailureListener(
            new OnFailureListener() {
              @Override
              public void onFailure(@NonNull Exception e) {
                logW("requestConnection() failed: " + e.getMessage());
                mIsConnecting = false;
                onConnectionFailed(endpoint);
              }
            });
  }

  private void connectedToEndpoint(Endpoint endpoint) {
    logD(String.format("connectedToEndpoint(endpoint=%s)", endpoint));
    mEstablishedConnections.put(endpoint.getId(), endpoint);
    onEndpointConnected(endpoint);
  }

  private void disconnectedFromEndpoint(Endpoint endpoint) {
    logD(String.format("disconnectedFromEndpoint(endpoint=%s)", endpoint));
    mEstablishedConnections.remove(endpoint.getId());
    onEndpointDisconnected(endpoint);
  }

  /**
   * Called when a connection with this endpoint has failed. Override this method to act on the
   * event.
   */
  private void onConnectionFailed(Endpoint endpoint) {
    logD("connection to endpoint failed: " + endpoint.getId());
  }

  /**
   * Called when someone has connected to us. Override this method to act on the event.
   */
  private void onEndpointConnected(Endpoint endpoint) {
    logD("connected to endpoint: " + endpoint.getId());
    requestGroupInfo();
  }

  /**
   * Called when someone has disconnected. Override this method to act on the event.
   */
  private void onEndpointDisconnected(Endpoint endpoint) {
    logD("disconnected from endpoint: " + endpoint.getId());
    logD("attempting to reconnect");
    connectToEndpoint(endpoint);
  }

  /**
   * Sends a {@link Payload} to all currently connected endpoints.
   *
   * @param payload The data you want to send.
   */
  private void send(Payload payload) {
    send(payload, mEstablishedConnections.keySet());
  }

  /**
   * Convenience method for sending a String as a {@link Payload} to all currently connected endpoints.
   *
   * @param payload the string to be sent
   */
  private void send(String payload) {
    send(Payload.fromBytes(payload.getBytes(StandardCharsets.UTF_8)));
  }

  private void send(Payload payload, Set<String> endpoints) {
    logD(String.format("Sending payload: %s", payload.toString()));
    mConnectionsClient
        .sendPayload(new ArrayList<>(endpoints), payload)
        .addOnFailureListener(
            new OnFailureListener() {
              @Override
              public void onFailure(@NonNull Exception e) {
                logW("sendPayload() failed: " + e.getMessage());
              }
            });
  }

  /**
   * Someone connected to us has sent us a NearbyMsg. Dispatch it to a handler!
   *
   * @param endpoint The sender.
   * @param payload The data.
   */
  private void onReceive(Endpoint endpoint, Payload payload) {
    if (payload.getType() != Payload.Type.BYTES) {
      logW(String.format("Got a payload of type %s. No idea what to do with this. Ignoring it.",
          payload.getType()));
      return;
    }

    final byte[] payloadBytes = payload.asBytes();
    if (payloadBytes == null) {
      logW("got null payload, ignoring");
      return;
    }
    final String received = new String(payloadBytes);

    final NearbyMsg msg;
    try {
      msg = nearbyMsgJsonAdapter.fromJson(received);
    } catch (IOException e) {
      logE("unable to unmarshal message from json: " + received);
      return;
    }

    //noinspection ConstantConditions
    final String msgType = msg.getMsgType();
    switch (msgType) {
      case REQUEST_GROUP:
        this.handleRequestGroup(endpoint, msg);
        break;
      case GROUP_AVAILABLE:
        this.handleGroupAvailable(endpoint, msg);
        break;
      case REQUEST_JOIN_GROUP:
        handleRequestJoinGroup(endpoint, msg);
        break;
      case REQUEST_LEAVE_GROUP:
        handleRequestLeaveGroup(endpoint, msg);
        break;
      case GROUP_JOIN_APPROVED:
        handleApproveJoinGroup(endpoint, msg);
        break;
      case GROUP_LEAVE_APPROVED:
        handleApproveLeaveGroup(endpoint, msg);
        break;
      default:
        logW("No idea what to do with this mesage:" + msg.getMsgType() + " " + msg.getData());
        break;
    }
  }

  ///////////////////////////
  // MESSAGE HANDLERS HERE //
  ///////////////////////////

  /**
   * Called when we get a REQUEST_GROUP message. We respond with our current group, if available.
   *
   * @param src: the Endpoint who sent us the message.
   * @param msg: the NearbyMsg sent.
   */
  private void handleRequestGroup(final Endpoint src, final NearbyMsg msg) {
    logD("got request for groups from " + src.getId());
    if (!mGroup.isPresent()) {
      logD("don't have any group");
      return;
    }
    try {
      final String groupJson = groupPojoJsonAdapter.toJson(GroupPojo.fromGroup(mGroup.get()));
      final String msgJson = nearbyMsgJsonAdapter.toJson(new NearbyMsg(GROUP_AVAILABLE, groupJson));
      send(msgJson);
    } catch (final GroupIOException e) {
      logE("unable to serialize group: " + e.getMessage());
    }
  }

  /**
   * Called when we get a REQUEST_JOIN_GROUP message.
   * We approve if our userID matches with the ownerID in the request.
   *
   * @param src: the Endpoint who sent us the message.
   * @param msg: the NearbyMsg sent.
   */
  private void handleRequestJoinGroup(final Endpoint src, final NearbyMsg msg) {
    JoinRequest req;
    try {
      req = joinRequestJsonAdapter.fromJson(msg.getData());
    } catch (IOException e) {
      logE("error handling join request: " + e.getMessage());
      logE("message: " + msg.getData());
      logE("source: " + src.getId());
      return;
    }

    if (req == null) {
      logE("didn't expect join request to be null, bailing");
      return;
    }

    if (!mUserName.equals(req.getOwnerID())) {
      logD("ignoring join request to " + req.getOwnerID() + " from " + req.getJoiner().toString());
      logD("mUserName: " + mUserName);
      return;
    }

    if (!mGroup.isPresent()) {
      logE("got a group join request but we have no group, bailing");
      return;
    }

    final boolean joined = mGroup.get().addUserToGroup(req.getJoiner());
    if (!joined) {
      final NearbyMsg denyMsg = new NearbyMsg(GROUP_JOIN_DENIED, msg.getData());
      final String denyMsgStr = nearbyMsgJsonAdapter.toJson(denyMsg);
      send(denyMsgStr);
      return;
    }

    // TODO: prompt the user to approve or deny the request in some asynchronous fashion.
    // For now, just sending an approved message with the original payload
    final NearbyMsg approveMsg = new NearbyMsg(GROUP_JOIN_APPROVED, msg.getData());
    final String approveMsgStr = nearbyMsgJsonAdapter.toJson(approveMsg);
    send(approveMsgStr); // TODO: just send it to the requestor
  }

  /**
   * Called when we get a REQUEST_LEAVE_GROUP message.
   * We approve if our userID matches with the ownerID in the request.
   * @param src: the Endpoint who sent us the message.
   * @param msg: the NearbyMsg sent.
   */
  private void handleRequestLeaveGroup(final Endpoint src, final NearbyMsg msg) {
    LeaveRequest req;
    try {
      req = leaveRequestJsonAdapter.fromJson(msg.getData());
    } catch (IOException e) {
      logE("error handling leave request: " + e.getMessage());
      logE("message: " + msg.getData());
      logE("source: " + src.getId());
      return;
    }

    if (req == null) {
      logE("didn't expect leave request to be null, bailing");
      return;
    }

    if (!mUserName.equals(req.getOwnerID())) {
      logD("ignoring leave request to " + req.getOwnerID() + " from " + req.getLeaver());
      logD("mUserName: " + mUserName);
      return;
    }

    if (!mGroup.isPresent()) {
      logE("got a group leave request but we have no group, bailing");
      return;
    }

    final boolean left = mGroup.get().removeUserFromGroup(req.getLeaver());
    if (!left) {
      // this probably means they aren't actually a member
      return;
    }
    // TODO: prompt the user to approve or deny the request in some asynchronous fashion.
    // For now, just sending an approved message with the original payload
    final NearbyMsg approveMsg = new NearbyMsg(GROUP_LEAVE_APPROVED, msg.getData());
    final String approveMsgStr = nearbyMsgJsonAdapter.toJson(approveMsg);
    send(approveMsgStr); // TODO: just send it to the requestor
  }

  /**
   * Called when we get an APPROVE_JOIN_GROUP message.
   * This indicates that the owner of the group has added us to their group.
   *
   * @param src: the Endpoint who sent us the message.
   * @param msg: the NearbyMsg sent.
   */
  private void handleApproveJoinGroup(final Endpoint src, final NearbyMsg msg) {
    JoinRequest req;
    try {
      req = joinRequestJsonAdapter.fromJson(msg.getData());
    } catch (IOException e) {
      logE("error handling join approval: " + e.getMessage());
      logE("message: " + msg.getData());
      logE("source: " + src.getId());
      return;
    }

    if (req == null) {
      logE("didn't expect join approval to be null, bailing");
      return;
    }

    if (!mUserName.equals(req.getJoiner().uuid)) {
      logD("ignoring join approval from " + req.getOwnerID() + " to " + req.getJoiner().toString());
      logD("mUserName: " + mUserName);
      return;
    }

    if (this.handler == null) {
      logE("our handler is gone, bailing");
      return;
    }

    // TODO: actually add the user to the group, need to refactor Group and User
    final Message handlerMessage = Message.obtain();
    handlerMessage.what = SUCCESS;
    handler.sendMessage(handlerMessage);
  }

  /**
   * Called when we get an APPROVE_LEAVE_GROUP message.
   * This indicates that the owner of the group has removed us from their group.
   *
   * @param src: the Endpoint who sent us the message.
   * @param msg: the NearbyMsg sent.
   */
  private void handleApproveLeaveGroup(final Endpoint src, final NearbyMsg msg) {
    LeaveRequest req;
    try {
      req = leaveRequestJsonAdapter.fromJson(msg.getData());
    } catch (IOException e) {
      logE("error handling leave approval: " + e.getMessage());
      logE("message: " + msg.getData());
      logE("source: " + src.getId());
      return;
    }

    if (req == null) {
      logE("didn't expect leave approval to be null, bailing");
      return;
    }

    if (!mUserName.equals(req.getLeaver().uuid)) {
      logD("ignoring leave approval from " + req.getOwnerID() + " to " + req.getLeaver());
      logD("mUserName: " + mUserName);
      return;
    }

    // TODO: under which circumstances should leaving a group be denied?
    // TODO: actually remove the user from the group, need to refactor Group and User
    if (this.handler == null) {
      logE("our handler is gone, bailing");
      return;
    }
    final Message handlerMessage = Message.obtain();
    handlerMessage.what = SUCCESS;
    handler.sendMessage(handlerMessage);
  }


  /**
   * Called when we get a GROUP_AVAILABLE message in response to a REQUEST_GROUP message.
   * This should have the details of the group embedded inside the message data.
   *
   * @param src: the Endpoint who sent us the message.
   * @param msg: the NearbyMsg sent.
   */
  private void handleGroupAvailable(final Endpoint src, final NearbyMsg msg) {
    final GroupPojo groupPojo;
    try {
      groupPojo = groupPojoJsonAdapter.fromJson(msg.getData());
    } catch (IOException e) {
      logE("unmarshaling group available from message: " + e.getMessage());
      return;
    }

    if (groupPojo == null) {
      logE("GroupPojo should not be null here. bailing.");
      return;
    }

    final Group group;
    try {
      group = groupPojo.toGroup();
    } catch (GroupIOException e) {
      logE("converting Group POJO to Group: " + e.getMessage());
      return;
    }

    addDiscoveredGroup(src, group);
    logD(String.format("got group from device %s (%s)", src.getId(), src.getName()));
  }

  /**
   * Transforms a {@link Status} into a English-readable message for logging.
   *
   * @param status The current status
   * @return A readable String. eg. [404]File not found.
   */
  private static String toString(Status status) {
    return String.format(
        Locale.US,
        "[%d]%s",
        status.getStatusCode(),
        status.getStatusMessage() != null
            ? status.getStatusMessage()
            : ConnectionsStatusCodes.getStatusCodeString(status.getStatusCode()));
  }

  void addDiscoveredGroup(Endpoint src, Group group) {
    discoveredGroups.put(src.getId(), group);
  }

  // Helper methods for logging.
  private void logV(String msg) {
    Log.v(TAG, msg);
  }

  private void logD(String msg) {
    Log.d(TAG, msg);
  }

  private void logW(String msg) {
    Log.w(TAG, msg);
  }

  private void logE(String msg) {
    Log.e(TAG, msg);
  }

  /**
   * Endpoint represents a device we can talk to.
   */
  protected static class Endpoint {

    @NonNull
    private final String id;
    @NonNull
    private final String name;

    Endpoint(@NonNull String id, @NonNull String name) {
      this.id = id;
      this.name = name;
    }

    @NonNull
    public String getId() {
      return id;
    }

    @NonNull
    public String getName() {
      return name;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj instanceof Endpoint) {
        Endpoint other = (Endpoint) obj;
        return id.equals(other.id);
      }
      return false;
    }

    @Override
    public int hashCode() {
      return id.hashCode();
    }

    @NotNull
    @Override
    public String toString() {
      return String.format("Endpoint{id=%s, name=%s}", id, name);
    }
  }
}
