package ie.tcd.cs7cs3.under.locationmanager;

import android.os.Bundle;

import androidx.annotation.NonNull;

import com.mapbox.mapboxsdk.geometry.LatLng;

public interface MapsManagerActivityInterface {
    /* This is the main function that loads the map from the mapbox SDK and gets the accesss token for it.
     * This is called whenever the user needs to see the map */
    void onCreate(Bundle savedInstanceState);

    /* Handles a user click on the map to pinpoint to a destination */
    boolean onMapClick(@NonNull LatLng point);
}
