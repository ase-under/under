package ie.tcd.cs7cs3.under.GroupManager;

import android.os.Handler;
import android.util.Log;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.common.base.Optional;
import ie.tcd.cs7cs3.under.storage.APIClient;
import ie.tcd.cs7cs3.under.storage.GroupPojo;
import ie.tcd.cs7cs3.under.nearby.Nearbyer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * GroupIOImpl wraps both {@link APIClient} and {@link Nearbyer} to provide a unified and consistent
 * interface to group operations both globally and locally.
 *
 * Groups are fetched in a background thread which is started by calling {@link GroupIOImpl#startListening()}.
 * To stop this background thread, call {@link GroupIOImpl#stopListening()}.
 *
 * To advertise a group both via {@link APIClient} and {@link Nearbyer}, call {@link GroupIOImpl#startAdvertising(Group)}.
 * To stop advertising this group, call {@link GroupIOImpl#stopAdvertising(Group)}.
 */
public class GroupIOImpl implements GroupIO {

  private final static String TAG = "GroupIOImpl";

  private final APIClient apiClient;
  private final Nearbyer nearbyer;
  private final Handler apiUpdateHandler;
  private Optional<Group> myGroup = Optional.absent();
  private Set<Group> storedGroups = new HashSet<>();
  private boolean offline = false;

  public GroupIOImpl(final String localName, final ConnectionsClient connectionsClient) {
    apiClient = new APIClient();
    nearbyer = new Nearbyer(localName, connectionsClient);
    apiUpdateHandler = new Handler();
  }

  @Deprecated
  public GroupIOImpl(APIClient apiClient, Nearbyer nearbyer, Handler apiUpdateHandler) {
    this.apiClient = apiClient;
    this.nearbyer = nearbyer;
    this.apiUpdateHandler = apiUpdateHandler;
  }

  @Override
  public boolean isOffline() {
    return this.offline;
  }

  @Override
  public void setOffline(boolean offline) {
    this.offline = offline;
    this.storedGroups.clear();
  }

  /**
   * nearbyGroups returns the known groups from both {@link APIClient} and {@link Nearbyer}.
   * @return a List of {@link Group}.
   * @throws GroupIOException if there is an issue fetching groups from {@link APIClient}.
   */
  @Override
  public List<Group> nearbyGroups() throws GroupIOException {
    return new ArrayList<>(storedGroups);
  }

  /**
   * startListening starts a background thread to update the cached list of {@link Group} and
   * sets {@link Nearbyer} into LISTENING state.
   */
  @Override
  public void startListening() {
    nearbyer.startListening();
    final Runnable groupUpdater = new GroupUpdater();
    apiUpdateHandler.post(groupUpdater);
  }

  /**
   * stopListening stops the background thread to update the cached list of {@link Group} and takes
   * {@link Nearbyer} out of LISTENING state.
   */
  @Override
  public void stopListening() {
    nearbyer.stopListening();
    apiUpdateHandler.removeCallbacksAndMessages(null);
  }

  /**
   * startAdvertising advertises {@param myGroup} both via {@link APIClient} and {@link Nearbyer}.
   * {@link APIClient#create} is called to create the group on the central server.
   * {@link Nearbyer#startAdvertising(Group)} is called to start advertising the group on P2P.
   *
   * @param myGroup the group to be advertised.
   * @throws GroupIOException if there is an issue communicating with the central server.
   */
  @Override
  public void startAdvertising(Group myGroup) throws GroupIOException {
    this.myGroup = Optional.of(myGroup);
    final GroupPojo gp = GroupPojo.fromGroup(myGroup);
    if (!offline && !myGroup.getId().isPresent()) {
      final GroupPojo created;
      try {
        created = apiClient.create(gp);
        this.myGroup.get().setID(created.groupId);
      } catch (IOException e) {

        Log.e(TAG, "unable to create group on central server, will try again later");
      }
    }
    nearbyer.startAdvertising(myGroup);
  }

  /**
   * stopAdvertising stops advertising {@param myGroup} both via {@link APIClient} and {@link Nearbyer}.
   * {@link APIClient#delete} is called to delete the group on the central server.
   * {@link Nearbyer#stopAdvertising(Group)} is called to stop advertising the group on P2P.
   *
   *
   * @param myGroup the group to be de-advertised.
   * @throws GroupIOException if there is an issue communicating with the central server.
   */
  @Override
  public void stopAdvertising(Group myGroup) throws GroupIOException {
    this.myGroup = Optional.absent();
    nearbyer.stopAdvertising(myGroup);
    final GroupPojo gp = GroupPojo.fromGroup(myGroup);
    try {
      if (!isOffline()) apiClient.delete(gp);
    } catch (final IOException e) {
      // TODO: this should eventually be cleaned up by undersvc.
      Log.e(TAG, "failed to delete group on central server:" + e.getMessage());
    }
  }

  /**
   * requestJoinGroup attempts to send {@param JoinRequest} to the group's owner, if in P2P range.
   * If the request is successful, {@param onSuccess} is sent a message.
   * If the request is denied or fails, {@param onFailure} is sent a message.
   *
   * @param req: the request to join the group.
   * @param onSuccess: called upon success.
   * @param onFailure: called upon failure.
   */
  @Override
  public void requestJoinGroup(JoinRequest req, Handler.Callback onSuccess,
      Handler.Callback onFailure) {
    // group operations are done via p2p
    nearbyer.requestJoinGroup(req, onSuccess, onFailure);
  }

  /**
   * requestLeaveGroup attempts to send {@param LeaveRequest} to the group's owner, if in P2P range.
   * If the request is successful, {@param onSuccess} is sent a message.
   * If the request is denied or fails, {@param onFailure} is sent a message.
   *
   * @param req: the request to leave the group.
   * @param onSuccess: called upon success.
   * @param onFailure: called upon failure.
   */
  @Override
  public void requestLeaveGroup(LeaveRequest req, Handler.Callback onSuccess,
      Handler.Callback onFailure) {
    // group operations are done via p2p
    nearbyer.requestLeaveGroup(req, onSuccess, onFailure);
  }

  private void logD(final String msg, final Object... args) {
    Log.d(TAG, String.format(msg, args));
  }

  private void logI(final String msg, final Object... args) {
    Log.i(TAG, String.format(msg, args));
  }

  private void logE(final String msg, final Object... args) {
    Log.e(TAG, String.format(msg, args));
  }

  class GroupUpdater implements Runnable {
    private static final String TAG = "GroupUpdater";
    void putMyGroup() throws GroupIOException, IOException {
      if (!myGroup.isPresent()) {
        Log.d(TAG, "i don't have any group to update");
        return;
      }

      if (offline) {
        Log.d(TAG, "offline mode enabled, not doing anything");
        return;
      }

      final boolean idPresent = myGroup.get().getId().isPresent();
      final GroupPojo gp = GroupPojo.fromGroup(myGroup.get());
      if (!idPresent) {
        Log.d(TAG, "groupID not present, creating");
        final GroupPojo created = apiClient.create(gp);
        myGroup.get().setID(created.groupId);
      }

      // as the owner of this group, our word is law
      apiClient.update(gp);
    }

    void fetchGroups() throws GroupIOException, IOException {
      final Set<Group> updatedGroups = new HashSet<>();
      if (!isOffline()) {
        final List<GroupPojo> apiGroups = apiClient.list();
        final List<Group> tmp = new ArrayList<>();
        for (final GroupPojo gp : apiGroups) {
          tmp.add(gp.toGroup());
        }
        updatedGroups.addAll(tmp);
      }

      updatedGroups.addAll(nearbyer.nearbyGroups());
      storedGroups = updatedGroups;
    }

    @Override
    public void run() {
      if (isOffline()) {
        Log.d(TAG, "running in offline mode - only information is from P2P");
      }
      try {
        putMyGroup();
      } catch (IOException | GroupIOException e) {
        GroupIOImpl.this.logE("apiClient updating my group: %s", e.getMessage());
      }

      try {
        fetchGroups();
      } catch (IOException | GroupIOException e) {
        GroupIOImpl.this.logE("apiClient fetching groups: %s", e.getMessage());
      }
      apiUpdateHandler.postDelayed(this, 10000);
    }
  }
}
