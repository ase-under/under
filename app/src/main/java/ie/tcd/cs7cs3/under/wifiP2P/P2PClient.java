package ie.tcd.cs7cs3.under.wifiP2P;

import android.util.Log;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;

public class P2PClient implements Runnable {

  final String TAG = "P2PClient";
  String mHost;
  int mPort;
  Socket mSocket;

  public P2PClient(InetAddress hostAddress, int port) throws IOException {
    this.mHost = hostAddress.getHostAddress();
    this.mPort = port;
    this.mSocket = new Socket();
  }

  public void reconnect() throws IOException {
    Log.i(TAG, String.format("connecting to %s:%s", this.mHost, this.mPort));
    this.mSocket.connect(new InetSocketAddress(this.mHost, this.mPort), 1000);

  }

  public String echo(final String msg) throws IOException {
    Log.i(TAG, String.format("send %s to %s:%s", msg, mHost, mPort));
    if (!mSocket.isConnected()) {
      reconnect();
    }
    final String fullMsg = String.format("echo %s", msg);
    this.mSocket.getOutputStream().write(fullMsg.getBytes());
    byte[] buf = new byte[1024];
    this.mSocket.getInputStream().read(buf);
    this.mSocket.getInputStream().close();
    this.mSocket.getOutputStream().close();
    this.mSocket.close();
    return Arrays.toString(buf);
  }

  @Override
  public void run() {
    this.mSocket = new Socket();
    try {
      reconnect();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
