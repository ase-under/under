package ie.tcd.cs7cs3.under;

import java.util.Objects;

/**
 * Location represents a named set of latitude-longitude coordinates.
 */
public class Location {
    private String address;
    private double lat;
    private double lng;

    /**
     * Creates a new Location from the given parametrs.
     * @param address
     * @param lat
     * @param lng
     */
    public Location(final String address, final double lat, final double lng) {
        this.address = address;
        this.lat = lat;
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Double.compare(location.lat, lat) == 0 &&
                Double.compare(location.lng, lng) == 0 &&
                Objects.equals(address, location.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, lat, lng);
    }
}
