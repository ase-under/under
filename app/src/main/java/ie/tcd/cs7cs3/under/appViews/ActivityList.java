package ie.tcd.cs7cs3.under.appViews;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView ;

import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.appViews.adapter.ListAdapter;
import ie.tcd.cs7cs3.under.appViews.model.ListModel;


public class ActivityList extends AppCompatActivity {
    private ListAdapter listAdapter;
    private RecyclerView recyclerview;
    private ArrayList<ListModel> listModelArrayList;

    String txt[] = {"Login",
            "Signup",
            "Home",
            "In-ride(Active ride)",
            "Ride Complete Rating",
            "Ride History",
            "Menu",
            "Old code integration"
    };
    private static final String[] INITIAL_PERMS = { Manifest.permission.ACCESS_FINE_LOCATION};
    private static final int INITIAL_REQUEST = 1337;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        recyclerview = findViewById(R.id.recyclerView1);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ActivityList.this);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());

        listModelArrayList = new ArrayList<>();

        for (int idx = 0; idx < txt.length; idx++) {
            ListModel listModel = new ListModel(txt[idx]);
            listModelArrayList.add(listModel);
        }
        listAdapter = new ListAdapter(ActivityList.this, listModelArrayList);
        recyclerview.setAdapter(listAdapter);

        checkPermission();
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M) {
            requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
        }
    }

    public void checkPermission() {
        Log.v("location", "inside checkpermission()");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {//Can add more as per requirement

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    3857);
        }
    }
}
