package ie.tcd.cs7cs3.under.GroupManager;

import com.google.common.base.Optional;
import ie.tcd.cs7cs3.under.storage.TransportMode;
import ie.tcd.cs7cs3.under.storage.UserPojo;
import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import ie.tcd.cs7cs3.under.Location;

public class Stubs {

  private GroupManager allGroups;

  /* TODO: add method to create a hard-coded group. This is called from front-end. */
  /* when sb clicks "Create Group" Button */
  public void createExampleGroup() {
    final Location exampleLocation1 = new Location("somewhere", -1, -1);
    final Location exampleLocation2 = new Location("somewhere else", 1, 1);
    User exampleUser1 = new User(
        UUID.fromString("deadbeef-dead-beef-dead-deadbeefdead"),
        50,
        -6,
        10,
        10,
        "female",
        ""
    );
    Group exampleGroup = new Group(
        Optional.of(1L),
        exampleLocation1,
        exampleLocation2,
        4,
        "",
        TransportMode.CAR,
        false,
        exampleUser1.getUid(),
        new Date(0),
        Optional.absent()
    );

    allGroups = new GroupManager();
    allGroups.addGroup(exampleGroup);

  }

  /* TODO: add method to join a hard-coded group. This is called from front-end. */
  public void joinExampleGroup() {
    /* Create a group we can join and a user to do so :) */
    createExampleGroup();
    UserPojo exampleUser2 = new UserPojo(TerribleUUIDUtil.terribleUUIDFromString("user2").toString(), "gender2");

    /* Get groups from GroupManager */
    List<Group> groupsWeCanJoin = allGroups.findSimilarDestinationGroups(51, 51);

    /* Join it */
    groupsWeCanJoin.get(0).addUserToGroup(exampleUser2);
  }

}
