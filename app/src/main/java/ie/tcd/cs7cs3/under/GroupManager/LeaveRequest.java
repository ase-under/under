package ie.tcd.cs7cs3.under.GroupManager;

import ie.tcd.cs7cs3.under.storage.UserPojo;

/**
 * LeaveRequest represents a request to leave a group.
 * It consists of a leaver and a group owner ID.
 */
public class LeaveRequest {

  final UserPojo leaver;
  final String ownerID;
  private boolean done;

  public LeaveRequest(final UserPojo leaver, final String ownerID) {
    this.leaver = leaver;
    this.ownerID = ownerID;
    this.done = false;
  }

  public UserPojo getLeaver() {
    return leaver;
  }

  public String getOwnerID() {
    return ownerID;
  }

  public boolean isDone() {
    return done;
  }

  public void done() {
    this.done = true;
  }

  public static class Builder {

    private UserPojo leaver;
    private String ownerID;

    public Builder setLeaver(final UserPojo leaver) {
      this.leaver = leaver;
      return this;
    }

    public Builder setOwnerID(final String ownerID) {
      this.ownerID = ownerID;
      return this;
    }

    public LeaveRequest build() {
      return new LeaveRequest(leaver, ownerID);
    }
  }
}
