package ie.tcd.cs7cs3.under.GroupManager;

/**
 * JoinLeaveResponse is sent by a group owner in response to a JoinRequest or a LeaveRequest.
 */
public class JoinLeaveResponse {
  boolean success;
  String message;

  public JoinLeaveResponse success(String message) {
    JoinLeaveResponse resp = new JoinLeaveResponse();
    resp.success = true;
    resp.message = message;
    return resp;
  }

  public JoinLeaveResponse failure(String message) {
    JoinLeaveResponse resp = new JoinLeaveResponse();
    resp.success = false;
    resp.message = message;
    return resp;
  }
}
