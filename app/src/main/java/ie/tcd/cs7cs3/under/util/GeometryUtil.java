package ie.tcd.cs7cs3.under.util;

import ie.tcd.cs7cs3.under.Location;
import java.util.ArrayList;
import java.util.List;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPoint;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.io.WKTWriter;

/**
 * GeometryUtil contains helper methods for reading and writing Well-Known-Text (WKT)
 * representations of MULTIPOINT geometry strings.
 */
public class GeometryUtil {

  private static final WKTReader wktReader = new WKTReader();
  private static final WKTWriter wktWriter = new WKTWriter();

  /**
   * locationsToString converts {@param locations} to a Well-Known-Text representation.
   *
   * @param locations: the list of {@link Location} to be converted to text.
   * @return The WKT representation of {@param locations} as a string.
   */
  public static String locationsToString(final Location... locations) {
    final GeometryFactory gf = new GeometryFactory();
    final List<Point> points = new ArrayList<>();
    for (final Location location : locations) {
      points.add(gf.createPoint(new Coordinate(location.getLng(), location.getLat())));
    }
    return wktWriter.write(gf.createMultiPoint(points.toArray(new Point[0])));
  }

  /**
   * stringToLocations takes a Well-Known-Text (WKT) representation of a MultiPoint and returns a
   * list of {@link Location}.
   *
   * @param wkt: the Well-Known-Text representation of a MultiPoint
   * @return the list of {@link Location} contained in {@param wkt}.
   * @throws ParseException if {@param wkt} is not a well-formed MultiPoint WKT representation.
   */
  public static List<Location> stringToLocations(final String wkt) throws ParseException {
    final org.locationtech.jts.geom.Geometry g = wktReader.read(wkt);
    final MultiPoint points = (MultiPoint) g;
    final List<Location> locations = new ArrayList<>();
    for (final Coordinate coordinate : points.getCoordinates()) {
      final double lng = coordinate.getX(); // longitude corresponds to the east-west position
      final double lat = coordinate.getY(); // latitude corresponds to the north-south position
      locations.add(new Location("", lat, lng)); // TODO: support address resolution somehow
    }
    return locations;
  }
}
