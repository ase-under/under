package ie.tcd.cs7cs3.under.wifiP2P;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import androidx.core.app.ActivityCompat;
import com.google.common.base.Optional;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.User;
import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.storage.TransportMode;
import ie.tcd.cs7cs3.under.wifiP2P.P2PHelper.GroupService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * DiscoveryActivity is a proof of concept activity to show WifiP2P-Service Discovery working.
 */
public class DiscoveryActivity extends AppCompatActivity {

  private static final int MY_PERMISSIONS_REQUEST_INTERNET = 1;
  private TextView textView;
  private ListView listView;

  private P2PHelper p2pHelper;

  private static final String TAG = "DiscoveryActivity";
  private Handler groupsUpdater;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestPermission();
    p2pHelper = new P2PHelper(this);
    setContentView(R.layout.activity_discovery);
    textView = findViewById(R.id.infodump);
    listView = findViewById(R.id.group_device_list);
    // XXX: we should request a random port by calling
    // serverSocket = new ServerSocket(0);
    // port = serverSocket.getLocalPort();

    final Location startLoc = new Location("somewhere", -1, -1);
    final Location destLoc = new Location("somewhere else", 1, 1);
    User exampleUser1 =
        new User(
            UUID.fromString("deadbeef-dead-beef-dead-deadbeefdead"), 50, -6, 10, 10, "female", "");
    final Group g = new Group(Optional.of(1L), startLoc, destLoc, 3, "", TransportMode.CAR, false, exampleUser1.getUid(),
        new Date(0), Optional.absent());

    p2pHelper.advertiseService(g, 8888);
    updateGroupsInBackground();
  }

  @Override
  protected void onStart() {
    Log.i(TAG, "starting");
    p2pHelper = new P2PHelper(this);
    super.onStart();
  }

  @Override
  public void onPause() {
    Log.i(TAG, "pausing");
    if (p2pHelper != null) {
      p2pHelper.cancelAdvertiseService();
      p2pHelper.cancelDiscoverServices();
    }
    stopUpdatingGroupsInBackground();
    super.onPause();
  }

  @Override
  public void onResume() {
    Log.i(TAG, "resuming");
    super.onResume();
    if (p2pHelper != null) {
      p2pHelper.discoverServices("_undergroup");
    }
    updateGroupsInBackground();
  }

  @Override
  protected void onStop() {
    Log.i(TAG, "being stopped");
    p2pHelper.cancelAdvertiseService();
    p2pHelper.cancelDiscoverServices();
    stopUpdatingGroupsInBackground();
    p2pHelper = null;
    super.onStop();
  }

  @Override
  protected void onDestroy() {
    Log.i(TAG, "being destroyed");
    p2pHelper.cancelAdvertiseService();
    p2pHelper.cancelDiscoverServices();
    super.onDestroy();
  }

  /**
   * updateGroupsInBackground launches a background handler to query the groups discovered by
   * P2PHelper.
   */
  void updateGroupsInBackground() {
    groupsUpdater = new Handler();
    groupsUpdater.postDelayed(
        new Runnable() {
          @Override
          public void run() {
            if (p2pHelper == null) {
              // try again later, we may have just woken up
              return;
            }
            final List<String> services = new ArrayList<>();
            for (final GroupService s : p2pHelper.getDiscoveredServices()) {
              services.add(s.toString());
            }
            ArrayAdapter<String> listViewAdapter =
                new ArrayAdapter<>(
                    getApplicationContext(), android.R.layout.simple_list_item_1, services);
            listView.setAdapter(listViewAdapter);
            groupsUpdater.postDelayed(this, 1000);
          }
        },
        0);
  }

  void stopUpdatingGroupsInBackground() {
    groupsUpdater.removeCallbacksAndMessages(null);
  }

  public void requestPermission() {
    final int result = ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
    if (result == PackageManager.PERMISSION_GRANTED) {
      return;
    }
    final String[] wanted = new String[]{Manifest.permission.INTERNET};
    ActivityCompat.requestPermissions(this, wanted, MY_PERMISSIONS_REQUEST_INTERNET);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] perms, int[] grants) {
    if (requestCode != MY_PERMISSIONS_REQUEST_INTERNET) {
      return;
    }
    if (grants.length <= 0 || grants[0] != PackageManager.PERMISSION_GRANTED) {
      Log.d(TAG, "Permission denied");
      return;
    }
    Log.d(TAG, "Permission granted");
    return;
  }
}
