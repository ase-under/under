package ie.tcd.cs7cs3.under.storage;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ie.tcd.cs7cs3.under.R;

public class CloudStorageActivity extends AppCompatActivity {
  private Handler handler;
  private UserEntityDAO users;
  private UserRatingEntityDAO ratings;
  private ListView listView;
  private static final String TAG = AddUserActivity.class.getName();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_groups);

    Toolbar toolbar = findViewById(R.id.toolbar);
    listView=(ListView) findViewById(R.id.groupInfo);

    final APIClient client = new APIClient();
    try {
      final List<GroupPojo> groups = client.list();
      final ArrayList<String> grpList = new ArrayList<>();
      for (GroupPojo g : groups) {
        grpList.add("GroupId " +g.groupId + " "+ "Group State "+g.groupState);
      }
      ArrayAdapter<String> adapter=new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_list_item_1, grpList);
      listView.setAdapter(adapter);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
