package ie.tcd.cs7cs3.under.appViews.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.optimization.v1.MapboxOptimization;
import com.mapbox.api.optimization.v1.models.OptimizationResponse;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import ie.tcd.cs7cs3.under.GroupManager.GroupIOImpl;
import ie.tcd.cs7cs3.under.GroupManager.JoinRequest;
import ie.tcd.cs7cs3.under.GroupManager.LeaveRequest;
import ie.tcd.cs7cs3.under.storage.UserPojo;
import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ie.tcd.cs7cs3.under.Location;
import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.storage.TransportMode;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.mapbox.core.constants.Constants.PRECISION_6;
import static com.mapbox.mapboxsdk.Mapbox.getAccessToken;
import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.lineWidth;
import static ie.tcd.cs7cs3.under.appViews.activities.UserHome.mGroupIOImpl;

public class GroupDetailFragment extends Fragment implements MapboxMap.OnMapClickListener, PermissionsListener, NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {
    String TAG = "GroupDetailFragment";
    Context mContext = getApplicationContext();
    View view;

    NavigationMapRoute navigationMapRoute;
    DirectionsRoute currentRoute;
    GroupDetailFragment groupDetailFragment_mock;
    //Map
    private MapView mapView;
    MapboxMap mapBoxMap;
    LocationComponent locationComponent;
    PermissionsManager permissionManager;
    Button btnJoinLeaveGroup;
    Button startNavigationButton;
    Button rateTrip;

    Point source, destination;

    Gson gson = null;
    private String groupID = null;
    private String groupOwnerId= null;
    private String groupSrc= null;
    private String groupDest= null;
    private String groupTransportMode= null;
    private String groupCapacityAllowed= null;
    private String groupfemaleOnly= null;
    private Location groupSourcePtLoc = null;
    private Location groupDestPtLoc = null;
    private String mUserName= null;
    private Point mUserSourcePt = null;
    private Point mUserDestPt = null;
    private Point mGroupSourcePt = null;
    private Point mGroupDestPt = null;


    TextView groupIDView;
    TextView ownerIDView;
    TextView tranportModeView;
    TextView srcAddressView;
    TextView destAddressView;
    TextView capacityValView;
    TextView femaleOnlyFlagView;

    //    Adding stops and optimization
    private List<Point> stops = new ArrayList<>();
    private static final String ICON_GEOJSON_SOURCE_ID = "icon-source-id";
    private MapboxOptimization optimizedClient;
    private DirectionsRoute optimizedRoute;


    public Point getSource() {
        return source;
    }

    public void setSource(Point source) {
        this.source = source;
    }

    public Point getDestination() {
        return destination;
    }

    public void setDestination(Point destination) {
        this.destination = destination;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        gson = new Gson();
        groupID = getArguments().getString("GR_ID");
        groupOwnerId=getArguments().getString("GR_OWNERID");
        groupSrc=getArguments().getString("GR_SRC");
        groupDest=getArguments().getString("GR_DEST");
        groupTransportMode=getArguments().getString("GR_TRNS");
        groupCapacityAllowed=getArguments().getString("GR_CAP");
        groupfemaleOnly=getArguments().getString("GR_FMLO");
        groupSourcePtLoc = gson.fromJson(getArguments().getString("GR_SRC_PT_LOC"), Location.class);
        groupDestPtLoc = gson.fromJson(getArguments().getString("GR_DEST_PT_LOC"), Location.class);

        //create point from Location type
        mGroupSourcePt = Point.fromLngLat(groupSourcePtLoc.getLng(),groupSourcePtLoc.getLat());
        mGroupDestPt = Point.fromLngLat(groupDestPtLoc.getLng(),groupDestPtLoc.getLat());

        mUserName = getArguments().getString("USER_NAME");
        mUserSourcePt = gson.fromJson(getArguments().getString("USER_SRC_PT"), Point.class);
        mUserDestPt = gson.fromJson(getArguments().getString("USER_DEST_PT"), Point.class);

        Log.d(TAG,"Joined Group Details: " +
                "groupId="+ groupID +
                "groupOwnerId="+groupOwnerId+
//                " groupSrc"+groupSrc+
//                " groupDest="+groupDest+
                " groupTransportMode="+groupTransportMode+
                " TransportMode="+ TransportMode.WALK+
                " GroupCapacityAllowed="+groupCapacityAllowed+
                " GroupfemaleOnly="+groupfemaleOnly+
                " groupSourcePtLoc Lat="+groupSourcePtLoc.getLat()+" Lng="+groupSourcePtLoc.getLng()+
                " groupDestPtLoc Lat="+groupDestPtLoc.getLat()+" Lng="+groupDestPtLoc.getLng()+
                " mUserName="+mUserName+
                " mUserSourcePt="+mUserSourcePt.coordinates().toString()+
                " mUserDestPt="+mUserDestPt.coordinates().toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //MOCKED FOR PATH
        GroupDetailFragment groupDetailFragment = new GroupDetailFragment();
//        groupDetailFragment.setDestination(Point.fromLngLat(-6.27261132495903, 53.33633738340339));
//        groupDetailFragment.setSource(Point.fromLngLat(-6.2727432, 53.3399345));
        groupDetailFragment.setSource(mUserSourcePt);
        groupDetailFragment.setDestination(Point.fromLngLat(GetCentralGeoCoordinate(geoCoordinates()).getLatitude(),
                GetCentralGeoCoordinate(geoCoordinates()).getLongitude()));

//        GeoJsonSource source1 = mapBoxMap.getStyle().getSourceAs("destination-source-id");
//        if (source1 != null)
//        {
//                source1.setGeoJson(Feature.fromGeometry(destination));
//        }
        groupDetailFragment_mock = groupDetailFragment;

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_group_detail, container, false);
        groupIDView = view.findViewById(R.id.GroupId);
        ownerIDView = view.findViewById(R.id.grpOwnerId);
        tranportModeView = view.findViewById(R.id.tranportMode);
        srcAddressView = view.findViewById(R.id.srcAddress);
        destAddressView = view.findViewById(R.id.destAddress);
        capacityValView = view.findViewById(R.id.capacityVal);
        femaleOnlyFlagView = view.findViewById(R.id.femaleOnlyFlag);

        groupIDView.setText(groupID);
        ownerIDView.setText(groupOwnerId);
        tranportModeView.setText(groupTransportMode);

        try {
            groupSrc = UserHome.reverseGeocoding(mGroupSourcePt.latitude(),mGroupSourcePt.longitude(),getContext());
            groupDest = UserHome.reverseGeocoding(mGroupDestPt.latitude(),mGroupDestPt.longitude(),getContext());
        } catch (IOException e) {
            groupSrc = String.format("Lat:%f Lng:%f", mGroupSourcePt.latitude(), mGroupSourcePt.longitude());
            groupDest = String.format("Lat:%f Lng:%f", mGroupDestPt.latitude(), mGroupDestPt.longitude());
            e.printStackTrace();
        }

        srcAddressView.setText(groupSrc);
        destAddressView.setText(groupDest);
        capacityValView.setText(groupCapacityAllowed);
        femaleOnlyFlagView.setText(groupfemaleOnly);

        //Map
        Mapbox.getInstance(getActivity(), getString(R.string.access_token));

        // Add the origin Point to the list for Optimization
        addFirstStopToStopsList();

        mapView = view.findViewById(R.id.groupMapView);
        btnJoinLeaveGroup = view.findViewById(R.id.appCompatJoinLeaveGrBtn);
        startNavigationButton = view.findViewById(R.id.appCompatUpdateGroupBtn);
        rateTrip = view.findViewById(R.id.rate);
        rateTrip.setVisibility(View.GONE);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

//        // For starting Navigation
//        startNavigationButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NavigationLauncherOptions options = NavigationLauncherOptions.builder()
//                        .directionsRoute(currentRoute)
//                        .shouldSimulateRoute(true)
//                        .build();
//                NavigationLauncher.startNavigation(getActivity(), options);
//            }
//        });

        btnJoinLeaveGroup.setText("Request to Join");
        btnJoinLeaveGroup.setOnClickListener(v -> {
            final String notReallyUUID = TerribleUUIDUtil.terribleUUIDFromString(mUserName).toString();
            final UserPojo userPojo = new UserPojo(notReallyUUID, "test"); // TODO: test
            final String currentOwnerId = groupOwnerId;
            final JoinRequest joinRequest = new JoinRequest(userPojo, currentOwnerId);
            final LeaveRequest leaveRequest = new LeaveRequest(userPojo, currentOwnerId);

            // pre-preparing a bunch of handlers...
            // 4. Called when we leave the group successfully.
            final Handler.Callback cbLeaveOnSuccess = (leaveMsg) -> {
                // at this point, assuming this is the owner's final decision and not re-enabling
                btnJoinLeaveGroup.setText("Left Group");
                return true;
            };

            // 3. Called when we fail to leave the group.
            final Handler.Callback cbLeaveOnFailure = (leaveMsg) -> {
                Log.d(TAG, "cbLeaveOnFailure: msg: " + leaveMsg.toString());
                Toast.makeText(getApplicationContext(), "Failed to leave group: " + leaveMsg.what, Toast.LENGTH_SHORT).show();
                btnJoinLeaveGroup.setEnabled(true);
                btnJoinLeaveGroup.setText("Request to Leave");
                return true;
            };

            // 2. Called when we request to leave the group
            final OnClickListener requestLeaveGroupListener = (v_) -> {
                btnJoinLeaveGroup.setEnabled(false);
                btnJoinLeaveGroup.setText("LEAVING...");
                mGroupIOImpl.requestLeaveGroup(leaveRequest, cbLeaveOnSuccess, cbLeaveOnFailure);
                Toast.makeText(getApplicationContext(), "Sent leave request", Toast.LENGTH_SHORT).show();
            };

            // 1. Called when we successfully join the group
            final Handler.Callback cbJoinOnSuccess = (joinMsg) -> {
                Log.d(TAG, "cbOnSuccess: msg: " + joinMsg.toString());
                Toast.makeText(getApplicationContext(), "Successfully joined group!", Toast.LENGTH_SHORT).show();
                btnJoinLeaveGroup.setEnabled(true);
                btnJoinLeaveGroup.setText("Leave Group");
                btnJoinLeaveGroup.setOnClickListener(requestLeaveGroupListener);
                return true;
            };

            // Called when we fail to join the group
            final Handler.Callback cbJoinOnFailure = (msg) -> {
                Log.d(TAG, "cbOnFailure: msg: " + msg.toString());
                Toast.makeText(getApplicationContext(), "Failed to join group: " + msg.what, Toast.LENGTH_SHORT).show();
                btnJoinLeaveGroup.setEnabled(true);
                btnJoinLeaveGroup.setText("Request to Join");
                return true;
            };

            mGroupIOImpl.requestJoinGroup(joinRequest, cbJoinOnSuccess, cbJoinOnFailure);
            Toast.makeText(getApplicationContext(), "Sent join request", Toast.LENGTH_SHORT).show();
            btnJoinLeaveGroup.setEnabled(false);
            btnJoinLeaveGroup.setText("JOINING...");
        });


        // For starting Navigation
        startNavigationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rateTrip.setVisibility(View.VISIBLE);
                NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                        .directionsRoute(currentRoute)
                        .shouldSimulateRoute(true)
                        .build();
                NavigationLauncher.startNavigation(getActivity(), options);
            }
        });



        rateTrip.setOnClickListener(new View.OnClickListener()
        {

            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                Intent intentRegister = new Intent(getApplicationContext(), Ratings.class);
                intentRegister.putExtra("groupownerid",groupID) ;
                intentRegister.putExtra("CURUSERINFO",getArguments().getString("CURUSERINFO"));
                startActivity(intentRegister);
            }
        });

        // get detail about group ownerID again
        return view;
    }

    private void addFirstStopToStopsList() {
        // Set first stop
        stops.add(mUserSourcePt);
    }

    //Map start
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        return false;
    }


    private void getRoute(Point origin, Point destination) {
        Log.d(TAG, "getRoute started ");
        NavigationRoute.builder(getApplicationContext()).accessToken(Mapbox.getAccessToken()).origin(origin).destination(destination).build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        if (response.body() == null) {
                            Log.e(TAG, "No routes foundm check right user and access token");
                            return;
                        } else if (response.body().routes().size() == 0) {
                            Log.e(TAG, "No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);
                        if (navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapBoxMap);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                        Log.e(TAG, "Error: " + t.getMessage());
                    }
                });
    }

    private void getOptimizedRoute() {
        Log.d(TAG, "getRoute started ");
        NavigationRoute.Builder builder = NavigationRoute.builder(getApplicationContext())
                .accessToken(Mapbox.getAccessToken())
                .origin(mUserSourcePt)
                .destination(mGroupDestPt);
        for (Point waypoint : stops) {
            if(waypoint!=mUserSourcePt && waypoint!=mGroupDestPt) {
                builder.addWaypoint(waypoint);
            }
        }

        builder.build().getRoute(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                if (response.body() == null) {
                    Log.e(TAG, "No routes foundm check right user and access token");
                    return;
                } else if (response.body().routes().size() == 0) {
                    Log.e(TAG, "No routes found");
                    return;
                }
                currentRoute = response.body().routes().get(0);
                if (navigationMapRoute != null) {
                    navigationMapRoute.removeRoute();
                } else {
                    Log.d(TAG, "Failed Route");
                    navigationMapRoute = new NavigationMapRoute(null, mapView, mapBoxMap);
                }
                navigationMapRoute.addRoute(currentRoute);
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                Log.e(TAG, "Error: " + t.getMessage());
            }
        });
    }

//    private void getOptimizedRoute() {
//        Log.d(TAG, "getRoute started ");
//        System.out.println("List of stops: "+stops);
//        optimizedClient = MapboxOptimization.builder()
//                .coordinates(stops)
//                .profile(DirectionsCriteria.PROFILE_DRIVING)
//                .accessToken(Mapbox.getAccessToken())
//                .source("first")
//                .destination("last")
//                .steps(true)
//                .roundTrip(false)
//                .build();
//
//        optimizedClient.enqueueCall(new Callback<OptimizationResponse>() {
//            @Override
//            public void onResponse(Call<OptimizationResponse> call, Response<OptimizationResponse> response) {
//
//                if (!response.isSuccessful()) {
//                    Log.d(TAG, "optimization call not successful");
//                    return;
//                } else {
//                    if (response.body().trips().isEmpty()) {
//                        Log.d(TAG, "optimization call successful but no routes");
//                        return;
//                    }
//                }
//                optimizedRoute = response.body().trips().get(0);
//                if (navigationMapRoute != null) {
//                    navigationMapRoute.removeRoute();
//                } else {
//                    Log.d(TAG, "Failed Route");
//                    navigationMapRoute = new NavigationMapRoute(null, mapView, mapBoxMap);
//                }
//                navigationMapRoute.addRoute(optimizedRoute);
//            }
//
//            @Override
//            public void onFailure(Call<OptimizationResponse> call, Throwable throwable) {
//                Log.d(TAG, "Error: " + throwable.getMessage());
//            }
//        });
//    }
    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapBoxMap = mapboxMap;
        this.mapBoxMap.setMaxZoomPreference(22);
        this.mapBoxMap.setMinZoomPreference(10);

        mapboxMap.setStyle(Style.LIGHT, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                // Add origin and destination to the mapboxMap
                initMarkerIconSymbolLayer(style);

                style = mapboxMap.getStyle();
                if (style != null) {
                    Point startPoint = Point.fromLngLat(GetCentralGeoCoordinate(geoCoordinates()).getLatitude(),
                            GetCentralGeoCoordinate(geoCoordinates()).getLongitude());
//                    addPointToStopsList(new LatLng(startPoint.latitude(), startPoint.longitude()));
                    addPointToStopsList(new LatLng(mUserDestPt.latitude(),mUserDestPt.longitude()));
//                    addPointToStopsList(new LatLng(53.33804452400483, -6.275438169972858));
                    addDestinationMarker(style);
//                    getOptimizedRoute(style, stops);
                }
                enableLocationComponent(style);
                addDestinationIconLayer(style);
//                getRoute(mUserSourcePt,mUserDestPt);
                getOptimizedRoute();
//                getOptimizedRoute();


                startNavigationButton.setEnabled(true);
//                mapboxMap.addOnMapClickListener(GroupDetailFragment.this);
            }
        });
    }

    private void addDestinationMarker(@NonNull Style style) {
        List<Feature> destinationMarkerList = new ArrayList<>();
        if (stops.size() != 0) {
            System.out.println("Stop is not null");
            for (Point singlePoint : stops) {
                destinationMarkerList.add(Feature.fromGeometry(
                        Point.fromLngLat(singlePoint.longitude(), singlePoint.latitude())));
            }
        }
//        stops.add(Point.fromLngLat(mGroupDestPt.longitude(), mGroupDestPt.latitude()));
        destinationMarkerList.add(Feature.fromGeometry(Point.fromLngLat(mGroupDestPt.longitude(), mGroupDestPt.latitude())));
        GeoJsonSource iconSource = style.getSourceAs(ICON_GEOJSON_SOURCE_ID);
        if (iconSource != null) {
            iconSource.setGeoJson(FeatureCollection.fromFeatures(destinationMarkerList));
        }
    }

    private void addPointToStopsList(LatLng point) {
        stops.add(Point.fromLngLat(point.getLongitude(), point.getLatitude()));
    }

    private void initMarkerIconSymbolLayer(@NonNull Style loadedMapStyle) {
        // Add the marker image to map
        loadedMapStyle.addImage("icon-image", BitmapFactory.decodeResource(
                this.getResources(), R.drawable.mapbox_marker_icon_default));
        // Add the source to the map
        loadedMapStyle.addSource(new GeoJsonSource(ICON_GEOJSON_SOURCE_ID,
                Feature.fromGeometry(mUserSourcePt)));
        loadedMapStyle.addLayer(new SymbolLayer("icon-layer-id", ICON_GEOJSON_SOURCE_ID).withProperties(
                iconImage("icon-image"),
                iconSize(1f),
                iconAllowOverlap(true),
                iconIgnorePlacement(true),
                iconOffset(new Float[]{0f, -7f})
        ));
    }

    private void addDestinationIconLayer(Style style) {

        Log.d(TAG, "addDestinationIconLayer style=" + style);
        style.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));

        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        style.addSource(geoJsonSource);

        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id", "destination-source-id");

        destinationSymbolLayer.withProperties(iconImage("destination-icon-id"), iconAllowOverlap(true),
                iconIgnorePlacement(true));

        style.addLayer(destinationSymbolLayer);
    }

    private void enableLocationComponent(Style loadedMapStyle) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(mContext)) {
            // Get an instance of the component
            locationComponent = mapBoxMap.getLocationComponent();
            // Activate with a built LocationComponentActivationOptions object
            locationComponent.activateLocationComponent(mContext, loadedMapStyle);

            // Enable to make component visible, change to false to hide device location
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionManager = new PermissionsManager(this);
            permissionManager.requestLocationPermissions(getActivity());
        }
    }

    /**
     * Adding origin Point of each user into a list of type LatLng
     *
     * @return
     */

    public static List<LatLng> geoCoordinates() {
        List<LatLng> geoCord = new ArrayList<>();
        geoCord.add(new LatLng(-6.276353861116689, 53.34302810607804));
        geoCord.add(new LatLng(-6.274990821171798, 53.34072704614172));
        geoCord.add(new LatLng(-6.268737688313905, 53.341280857051316));
        return geoCord;
    }

    /**
     * @param geoCoordinates : List of originPoint of each member of the group
     *                       Calculating the starting point for each group
     * @return
     * @author : Chavvi Chandani
     */
    public static LatLng GetCentralGeoCoordinate(List<LatLng> geoCoordinates) {
        if (geoCoordinates.size() == 1) {
            return geoCoordinates.get(0);
        }

        double x = 0;
        double y = 0;
        double z = 0;

        for (LatLng geoCoordinate : geoCoordinates) {
            double latitude = geoCoordinate.getLatitude() * Math.PI / 180;
            double longitude = geoCoordinate.getLongitude() * Math.PI / 180;

            x += Math.cos(latitude) * Math.cos(longitude);
            y += Math.cos(latitude) * Math.sin(longitude);
            z += Math.sin(latitude);
        }

        int total = geoCoordinates.size();

        x = x / total;
        y = y / total;
        z = z / total;

        double centralLongitude = Math.atan2(y, x);
        double centralSquareRoot = Math.sqrt(x * x + y * y);
        double centralLatitude = Math.atan2(z, centralSquareRoot);

        return new LatLng(centralLatitude * 180 / Math.PI, centralLongitude * 180 / Math.PI);

    }
//    private  void addDestinationIconLayer(Style style)
//    {
//        Log.d(TAG,"addDestinationIconLayer style="+style);
//        style.addImage("destination-icon-id",
//                BitmapFactory.decodeResource(this.getResources(),R.drawable.mapbox_marker_icon_default));
//
//        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
//        style.addSource(geoJsonSource);
//
//        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id","destination-source-id");
//
//        destinationSymbolLayer.withProperties(iconImage("destination-icon-id" ),iconAllowOverlap(true),
//                iconIgnorePlacement(true));
//
//        style.addLayer(destinationSymbolLayer);
//    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        // user denies the access to location the first time and when second time you request it you can present a dialogue to explain why the access is needed
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocationComponent(mapBoxMap.getStyle());
        } else {
            Toast.makeText(getApplicationContext(), "Permission not granted", Toast.LENGTH_LONG).show();
//            finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
    //MapEnd

    /**
     * Overriding Mapview Lifecycle methods
     **/
    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    /**
     * Overriding Mapview Lifecycle methods
     **/
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * Overriding Mapview Lifecycle methods
     **/
    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /**
     * Overriding Mapview Lifecycle methods
     **/
    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


}