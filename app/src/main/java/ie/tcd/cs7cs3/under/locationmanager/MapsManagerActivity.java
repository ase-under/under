package ie.tcd.cs7cs3.under.locationmanager;
import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineRegionError;
import com.mapbox.mapboxsdk.offline.OfflineRegionStatus;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import ie.tcd.cs7cs3.under.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;

import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.optimization.v1.MapboxOptimization;
import com.mapbox.api.optimization.v1.models.OptimizationResponse;
import com.mapbox.geojson.Point;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.Objects;





public class MapsManagerActivity extends AppCompatActivity implements OnMapReadyCallback,
        MapboxMap.OnMapClickListener, PermissionsListener
{
    /**
     * Creating Mapview Variables
     * @uthor : Chavvi Chandani
     **/


    /**
     * Variables for search
     */
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    String symbolIconId = "symbolIconId";
    FloatingActionButton searchLocationButton;

    MapView mapView;
    MapboxMap mapBoxMap;
    Button startButton;
    //private CarmenFeature home;
    //private CarmenFeature work;
    private String geojsonSourceLayerId = "geojsonSourceLayerId";
    LocationComponent locationComponent;
    PermissionsManager permissionManager;
    DirectionsRoute currentRoute;
    Point originPoint, destinationPoint;
    NavigationMapRoute navigationMapRoute;
    private static final String TAG = "NavigationActivity";
    //TextView labelLongLat;
    TextView address;

    private List<Point> pointsOnRoute = new ArrayList<>();
    double destinationLatitude;
    double destinationLongitude;


    /** Creating varibales for Offline map
     * @uthor : Chavvi Chandani */
    private static final String OFF = "OffManActivity";
    // JSON encoding/decoding
    public static final String JSON_CHARSET = "UTF-8";
    public static final String JSON_FIELD_REGION_NAME = "FIELD_REGION_NAME";

    ProgressBar progressBar;
    Button downloadButton;
    boolean isEndNotified;
    int regionSelected;

    // Offline objects
    private OfflineManager offlineManager;
    private OfflineRegion offlineRegion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Passing context and access token to Mapbox */
        Mapbox.getInstance(this, getString(R.string.access_token));
        /** This contains the MapView in XML and needs to be called after the access token is configured */
        setContentView(R.layout.activity_maps_manager);

        startButton = findViewById(R.id.startButton);

        /* Getting Mapview */
        mapView = findViewById(R.id.mapView);

        /**Mapview has its own Lifecycle methods of Maintaining open GL Lifecycle which must be called directly from the containing activity
         * Hence, Overriding Activity lifecycle methods by adding corresponding lifecycle method here **/
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(this);

        //labelLongLat = findViewById(R.id.textView);
        address = findViewById(R.id.address);

        startButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                        .directionsRoute(currentRoute)
                        .shouldSimulateRoute(true)
                        .build();
                NavigationLauncher.startNavigation(MapsManagerActivity.this, options);
            }
        });
        searchLocationButton = findViewById(R.id.fab_location_search);
        searchLocationButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken(Mapbox.getAccessToken() != null ? Mapbox.getAccessToken() :
                                getString(R.string.access_token))
                        .placeOptions(PlaceOptions.builder()
                                .backgroundColor(Color.parseColor("#EEEEEE"))
                                .limit(10)
                                .proximity(destinationPoint)
                                .build(PlaceOptions.MODE_CARDS))
                        .build(MapsManagerActivity.this);
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
            }
        });


    }

    public Point getDestinationPoint(@NonNull LatLng point) {
        destinationPoint = Point.fromLngLat(point.getLongitude(), point.getLatitude());
        return destinationPoint;
    }

    public double getDestinationLatitude(LatLng destinationPoint) {
        destinationLatitude = getDestinationPoint(destinationPoint).latitude();
        return destinationLatitude;
    }

    public double getDestinationLongitude(LatLng destinationPoint) {
        destinationLongitude = getDestinationPoint(destinationPoint).longitude();
        return destinationLongitude;

    }
    public String reverseGeocoding(double lat, double lon) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        addresses = geocoder.getFromLocation(lat, lon, 1);
        String address = addresses.get(0).getAddressLine(0);
        return address;
    }

    /**
     * Sets a point as destinantion when the user clicks on the map
     * Calling the getRoute function  to get a route between the current location and destinantion selected
     * Enables the Start Navigation button
     *
     * @uthor : Chavvi Chandani
     **/
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        /**point gives the location of anywhere you tap on screen - latitude and longitude */
        getDestinationPoint(point);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //TO DO
        }
        originPoint = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                locationComponent.getLastKnownLocation().getLatitude());

        GeoJsonSource source = mapBoxMap.getStyle().getSourceAs("destination-source-id");
        if (source != null) {
            source.setGeoJson(Feature.fromGeometry(destinationPoint));
        }
        destinationLatitude = getDestinationLatitude(point);
        destinationLongitude = getDestinationLongitude(point);
        try {
            address.setText(reverseGeocoding(destinationLatitude,destinationLongitude));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.println("my location name%%%%%%%%-->"+reverseGeocoding(destinationLatitude,destinationLongitude));
        //labelLongLat.setText("Latitude " + destinationLatitude + "\n Longitude " + destinationLongitude);


        getRoute(originPoint,destinationPoint);
        startButton.setEnabled(true);
        startButton.setBackgroundResource(R.color.mapbox_blue);
        return true;
    }

    private void setUpSource(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addSource(new GeoJsonSource(geojsonSourceLayerId));
    }

    private void setupLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addLayer(new SymbolLayer("SYMBOL_LAYER_ID", geojsonSourceLayerId).withProperties(iconImage(symbolIconId), iconOffset(new Float[]{0f, -8f})));
    }

    /** Find a route between the origin and the destination
     * @author: Chavvi Chandani **/
    private void getRoute(Point origin , Point destination) {
        NavigationRoute.builder(this).accessToken(Mapbox.getAccessToken()).origin(origin).destination(destination).build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        if (response.body() == null) {
                            Log.e(TAG, "No routes foundm check right user and access token");
                            return;
                        } else if (response.body().routes().size() == 0) {
                            Log.e(TAG, "No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);
                        if(navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapBoxMap);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                        Log.e(TAG, "Error: " + t.getMessage());
                    }
                });
    }

    /**
     * @author Stefan Spirkl
     * Should calculate an optimized route using MapBox SDK. Our goal here is to find the shortest route for all passengers with a fixed start and end point.
     * The order of the points in between can be optimized, with the target here to minimize travel duration time.
     * We may use a internet connection for this and build on Mapbox SDK.

     @param coordinates      -   a list of stops, with the first element the start point and the last the end point.
     By default, the first element is the start point of the trip (source: https://docs.mapbox.com/api/navigation/#optimization).
     We add an additional .destination parameter to define the last element as the last stop.
     @returns optimizedRoute -   a DirectionsRoute object for further processing within MapBox SDK. It will return null if the optimization failed,
     most likely due to a lack of an internet connection. The calling function is responsible to handle that and degrade gracefully.

     */
    static DirectionsRoute getOptimizedRoute(final List<Point> coordinates) {
        System.out.println("Start getting an optimized route ...");

        final DirectionsRoute[] optimizedRoute = new DirectionsRoute[1];

        /* Call to MapBox SDK */
        MapboxOptimization optimizedClient = MapboxOptimization.builder()
                .coordinates(coordinates)
                .destination("last")
                .profile(DirectionsCriteria.PROFILE_DRIVING)
                .accessToken("sk.eyJ1Ijoia2FuaWthZ2hpbG9yaWEiLCJhIjoiY2szMDB6cnp2MGt4dTNobW02NXlhdHVtMSJ9.eLO0R_12hy1g3aMcMNla2Q")
                .build();

        System.out.println("Halfway through");

        /* Receive and handle response :-) */
        optimizedClient.enqueueCall(new Callback<OptimizationResponse>() {
            @Override
            public void onResponse(@NotNull Call<OptimizationResponse> call, @NotNull Response<OptimizationResponse> response) {
                System.out.println("onResponse");

                if (!response.isSuccessful()) {
                    System.out.println("optimization call not successful");
                    return;
                } else {
                    assert response.body() != null;
                    if (Objects.requireNonNull(response.body().trips()).isEmpty()) {
                        System.out.println("optimization call successful but no routes");
                        return;
                    }
                }
                /* this is a bit patchy, we need to use an array so we can access from inner class */
                System.out.println("Output: " + Objects.requireNonNull(response.body().trips()).get(0));
                optimizedRoute[0] = Objects.requireNonNull(response.body().trips()).get(0);
            }

            @Override
            public void onFailure(@NotNull Call<OptimizationResponse> call, @NotNull Throwable throwable) {
                System.out.println("Error: %s" + throwable.getMessage());
            }
        });
        System.out.println("... finished getting an optimized route.");
        return optimizedRoute[0];
    }


    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap)
    {
        this.mapBoxMap = mapboxMap;
        this.mapBoxMap.setMinZoomPreference(15);
        //mapBoxMap.setStyle(getString(R.string.navigation_guidance_day), new Style.OnStyleLoaded()
        mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded()
        {
            @Override
            public void onStyleLoaded(@NonNull Style style) {

                searchLocationButton.setEnabled(true);
                enableLocationComponent(style);
                addDestinationIconLayer(style);
                mapboxMap.addOnMapClickListener(MapsManagerActivity.this);

                // Add the symbol layer icon to map for future use
                style.addImage(symbolIconId, BitmapFactory.decodeResource(
                        MapsManagerActivity.this.getResources(), R.drawable.map_default_map_marker));

                // Assign progressBar for later use
                progressBar = findViewById(R.id.progress_bar);

                // Set up the offlineManager
                offlineManager = OfflineManager.getInstance(MapsManagerActivity.this);

                // Bottom navigation bar button clicks are handled here.
                // Download offline button
                downloadButton = findViewById(R.id.download_button);
                downloadButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downloadRegionDialog();
                    }
                });

                // Create an empty GeoJSON source using the empty feature collection
                setUpSource(style);

                // Set up a new symbol layer for displaying the searched location's feature coordinates
                setupLayer(style);
            }
        });
    }

    private void downloadRegionDialog()
    {
        // Set up download interaction. Display a dialog
        // when the user clicks download button and require
        // a user-provided region name
        AlertDialog.Builder builder = new AlertDialog.Builder(MapsManagerActivity.this);
        final EditText regionNameEdit = new EditText(MapsManagerActivity.this);
        regionNameEdit.setHint(getString(R.string.set_region_name_hint));

        // Build the dialog box
        builder.setTitle(getString(R.string.dialog_title))
                .setView(regionNameEdit)
                .setMessage(getString(R.string.dialog_message))
                .setPositiveButton(getString(R.string.dialog_positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String regionName = regionNameEdit.getText().toString();
                        // Require a region name to begin the download.
                        // If the user-provided string is empty, display
                        // a toast message and do not begin download.
                        if (regionName.length() == 0) {
                            Toast.makeText(MapsManagerActivity.this, getString(R.string.dialog_toast), Toast.LENGTH_SHORT).show();
                        } else {
                            // Begin download process
                            downloadRegion(regionName);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.dialog_negative_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // Display the dialog
        builder.show();
    }

    private void downloadRegion(final String regionName) {
        // Define offline region parameters, including bounds,
        // min/max zoom, and metadata

        // Start the progressBar
        startProgress();

        // Create offline definition using the current
        // style and boundaries of visible map area

        mapBoxMap.getStyle(new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                String styleUrl = style.getUri();
                LatLngBounds bounds = mapBoxMap.getProjection().getVisibleRegion().latLngBounds;
                double minZoom = mapBoxMap.getCameraPosition().zoom;
                double maxZoom = mapBoxMap.getMaxZoomLevel();
                float pixelRatio = MapsManagerActivity.this.getResources().getDisplayMetrics().density;
                OfflineTilePyramidRegionDefinition definition = new OfflineTilePyramidRegionDefinition(
                        styleUrl, bounds, minZoom, maxZoom, pixelRatio);
                // Build a JSONObject using the user-defined offline region title,
                // convert it into string, and use it to create a metadata variable.
                // The metadata variable will later be passed to createOfflineRegion()
                byte[] metadata;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(JSON_FIELD_REGION_NAME, regionName);
                    String json = jsonObject.toString();
                    metadata = json.getBytes(JSON_CHARSET);
                } catch (Exception exception) {
                    Timber.e("Failed to encode metadata: %s", exception.getMessage());
                    metadata = null;
                }

                // Create the offline region and launch the download
                offlineManager.createOfflineRegion(definition, metadata, new OfflineManager.CreateOfflineRegionCallback() {
                    @Override
                    public void onCreate(OfflineRegion offlineRegion) {
                        Timber.d( "Offline region created: %s" , regionName);
                        MapsManagerActivity.this.offlineRegion = offlineRegion;
                        launchDownload();
                    }

                    @Override
                    public void onError(String error) {
                        Timber.e( "Error: %s" , error);
                    }
                });
            }
        });
    }
    // Progress bar methods
    private void startProgress()
    {
        // Disable buttons
        downloadButton.setEnabled(false);

        // Start and show the progress bar
        isEndNotified = false;
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void launchDownload()
    {
        // Set up an observer to handle download progress and
        // notify the user when the region is finished downloading
        offlineRegion.setObserver(new OfflineRegion.OfflineRegionObserver()
        {
            @Override
            public void onStatusChanged(OfflineRegionStatus status) {
                // Compute a percentage
                double percentage = status.getRequiredResourceCount() >= 0
                        ? (100.0 * status.getCompletedResourceCount() / status.getRequiredResourceCount()) :
                        0.0;

                if (status.isComplete()) {
                    // Download complete
                    endProgress(getString(R.string.end_progress_success));
                    return;
                } else if (status.isRequiredResourceCountPrecise()) {
                    // Switch to determinate state
                    setPercentage((int) Math.round(percentage));
                }

                // Log what is being currently downloaded
                Timber.d("%s/%s resources; %s bytes downloaded.",
                        String.valueOf(status.getCompletedResourceCount()),
                        String.valueOf(status.getRequiredResourceCount()),
                        String.valueOf(status.getCompletedResourceSize()));
            }

            @Override
            public void onError(OfflineRegionError error) {
                Timber.e("onError reason: %s", error.getReason());
                Timber.e("onError message: %s", error.getMessage());
            }

            @Override
            public void mapboxTileCountLimitExceeded(long limit) {
                Timber.e("Mapbox tile count limit exceeded: %s", limit);
            }

        });

        // Change the region state
        offlineRegion.setDownloadState(OfflineRegion.STATE_ACTIVE);
    }

    private void endProgress(final String message) {
        // Don't notify more than once
        Toast.makeText(MapsManagerActivity.this,"Done", Toast.LENGTH_SHORT).show();
        if (isEndNotified) {
            return;
        }
    }
    private void setPercentage(final int percentage) {
        progressBar.setIndeterminate(false);
        progressBar.setProgress(percentage);
    }


    private  void addDestinationIconLayer(Style style)
    {
        style.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(),R.drawable.mapbox_marker_icon_default));

        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        style.addSource(geoJsonSource);

        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id","destination-source-id");

        destinationSymbolLayer.withProperties(iconImage("destination-icon-id" ),iconAllowOverlap(true),
                iconIgnorePlacement(true));

        style.addLayer(destinationSymbolLayer);
    }

    private void enableLocationComponent(Style loadedMapStyle) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            // Get an instance of the component
            locationComponent = mapBoxMap.getLocationComponent();
            // Activate with a built LocationComponentActivationOptions object
            locationComponent.activateLocationComponent(this, loadedMapStyle);

            // Enable to make component visible, change to false to hide device location
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } else {
            permissionManager = new PermissionsManager(this);
            permissionManager.requestLocationPermissions(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            //Retrieve selected location's CarmenFeature
            CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
            // Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above.
            // Then retrieve and update the source designated for showing a selected location's symbol layer icon
            if (mapBoxMap != null) {
                Style style = mapBoxMap.getStyle();
                if (style != null) {
                    GeoJsonSource source = style.getSourceAs(geojsonSourceLayerId);
                    if (source != null) {
                        source.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{
                                Feature.fromJson(selectedCarmenFeature.toJson())}));
                    }
                    // Move map camera to the selected location
                    mapBoxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                            (((Point) selectedCarmenFeature.geometry()).longitude())))
                                    .zoom(14)
                                    .build()), 4000);
                }
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults); // takes care of permissions
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain)
    {
        // user denies the access to location the first time and when second time you request it you can present a dialogue to explain why the access is needed
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if(granted) {
            enableLocationComponent(mapBoxMap.getStyle());
        } else {
            Toast.makeText(getApplicationContext(), "Permission not granted", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    /* Overriding Mapview Lifecycle methods */
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    /* Overriding Mapview Lifecycle methods */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /* Overriding Mapview Lifecycle methods */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /* Overriding Mapview Lifecycle methods */
    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    /* Overriding Mapview Lifecycle methods */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mapView.onSaveInstanceState(outState);
    }

    /* Overriding Mapview Lifecycle methods */
    @Override
    protected void onDestroy() {

        super.onDestroy();
        mapView.onDestroy();
        // mapboxGeocoding.cancelCall();
    }

    /* Overriding Mapview Lifecycle methods */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}