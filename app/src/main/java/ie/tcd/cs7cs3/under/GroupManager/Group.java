/**
 * The Group  class instantiates and handles activities related
 * to a SINGLE group object.
 * @author Stefan Spirkl
 * @author Ashwin Ramasubramanian
 */

package ie.tcd.cs7cs3.under.GroupManager;

import com.google.common.base.Optional;
import ie.tcd.cs7cs3.under.storage.TransportMode;
import ie.tcd.cs7cs3.under.storage.UserPojo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import ie.tcd.cs7cs3.under.Location;

public class Group implements GroupInterface {
    private String recurringInterval;
    private TransportMode transportMode;
    private boolean femaleOnly;
    private boolean groupIsAcceptingNewMembersAndNotMoving = true;
    private int capacity;
    private Location start;
    private Location destination;

    private Optional<Long> id;
    private UUID owner;
    private List<UUID> usersInThatGroup;
    private Date createdAt;
    private Optional<Date> departedAt;

    public Optional<Long> getId() {
        return id;
    }

    public Optional<Date> getDepartedAt() {
        return departedAt;
    }

    /** Constructor
     * @author Stefan Spirkl
     * @author Ashwin Ramasubramanian
     */
    public Group(
        final Optional<Long> id, // group may or may not have a global identifier assigned.
        final Location start,
        final Location destination,
        final int capacity,
        final String recurringInterval,
        final TransportMode transportMode,
        final boolean femaleOnly,
        final UUID ownerUUID,
        final Date createdAt,
        final Optional<Date> departedAt // group may or may not have departed yet.
    ) {
        this.start = start;
        this.destination = destination;
        this.capacity = capacity;
        this.recurringInterval = recurringInterval;
        this.transportMode = transportMode;
        this.femaleOnly = femaleOnly;
        this.createdAt = createdAt;
        this.departedAt = departedAt;

        //addUserToGroup(firstUserOfGroup);
        this.usersInThatGroup = new ArrayList<>();
        this.usersInThatGroup.add(ownerUUID);
        this.owner = ownerUUID;
        this.id = id;
    }

    /** Adds a single User to the group object that owns the method.
     * We call this method when a user joins a group.
     * @author Stefan Spirkl
     * @author Ashwin Ramasubramanian
     * @param userToAddToThisGroup a User object that should be attached
     * @return true if the join was successful, false if it failed and the user did not join this group
     */
    public boolean addUserToGroup(final UserPojo userToAddToThisGroup) {
      /* basic check whether the group accepts this user */
      if (!groupIsAcceptingNewMembersAndNotMoving) {
        return false;
      }

      final UUID theirUUID = UUID.fromString(userToAddToThisGroup.uuid);
      final boolean alreadyThere = this.usersInThatGroup.contains(theirUUID);
      if (alreadyThere) {
        return false;
      }

      final boolean enoughCapacity = (this.capacity > usersInThatGroup.size());
      if (!enoughCapacity) {
        return false;
      }

      final boolean femaleOnlyAllowed = (!femaleOnly || userToAddToThisGroup.gender.equals("female"));
      if (!femaleOnlyAllowed) {
        return false;
      }

      return usersInThatGroup.add(theirUUID);
    }

    /** Method that removes a single User from the group object that owns the method.
     * @author Stefan Spirkl
     * @author Ashwin Ramasubramanian
     * @param userToRemoveFromThisGroup a User object that should be attached
     */
    public boolean removeUserFromGroup(final UserPojo userToRemoveFromThisGroup) {
        return usersInThatGroup.remove(UUID.fromString(userToRemoveFromThisGroup.uuid));
    }

    /** Method that starts the journey for the group object that owns the method.
     * We call this method when a group decides to move in space.
     * @author Stefan Spirkl
     * @author Ashwin Ramasubramanian
     */
    public void startJourney() {
        groupIsAcceptingNewMembersAndNotMoving = false;
    }

    /** getUsersOfGroup returns the UUIDs of the members of the group.
     * @author Stefan Spirkl
     * @author Ashwin Ramasubramanian
     */
    public List<UUID> getUsersOfGroup() {
        return usersInThatGroup;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getState() {
        if (groupIsAcceptingNewMembersAndNotMoving) {
            return "FORMING";
        }
        return "MOVING";
    }


    /**
     * getOwner returns the UUID of the owner of the group.
     * @return Current owner of the group.
     */
    public UUID getOwner() {
        return owner;
    }

    /**
     * getDestination returns the destination of the group.
     * @return the String representation of the group's destination.
     */
    public Location getDestination() {
        return destination;
    }

    /**
     * getCapacity returns the available capacity of the group.
     * @return the number of members the group may accept
     */
    public int getCapacity() {
        return this.capacity;
    }

    public Location getStart() {
        return start;
    }

    /**
     * sets the GroupID, which may not be present at the time of creation.
     * @param id: global groupID.
     */
    public void setID(final long id) {
        this.id = Optional.of(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return  femaleOnly == group.femaleOnly &&
                groupIsAcceptingNewMembersAndNotMoving == group.groupIsAcceptingNewMembersAndNotMoving &&
                capacity == group.capacity &&
                Objects.equals(recurringInterval, group.recurringInterval) &&
                Objects.equals(transportMode, group.transportMode) &&
                Objects.equals(start, group.start) &&
                Objects.equals(destination, group.destination) &&
                Objects.equals(owner, group.owner) &&
                Objects.equals(usersInThatGroup, group.usersInThatGroup);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, recurringInterval, transportMode, femaleOnly, groupIsAcceptingNewMembersAndNotMoving, capacity, destination, owner, usersInThatGroup);
    }

    public String getRecurringInterval() {
        return recurringInterval;
    }

    public TransportMode getTransportMode() {
        return transportMode;
    }

    public boolean isFemaleOnly() {
        return femaleOnly;
    }

    public static class Builder {
        private Optional<Long> id = Optional.absent();
        private Location start;
        private Location destination;
        private UUID owner;
        private int capacity;
        private List<UUID> usersInThatGroup = new ArrayList<>();
        private String recurringInterval= "";
        private TransportMode transportMode  = TransportMode.WALK;
        private boolean femaleOnly = false;
        private Date createdAt;
        private Optional<Date> departedAt = Optional.absent();

        public Builder setID(final Optional<Long> id) {
            this.id = id;
            return this;
        }

        public Builder setStart(final Location start) {
            this.start = start;
            return this;
        }

        public Builder setDestination(final Location destination) {
            this.destination = destination;
            return this;
        }

        public Builder setOwner(final UUID ownerUUID) {
            this.owner = ownerUUID;
            this.usersInThatGroup.add(ownerUUID);
            return this;
        }

        public Builder setCapacity(final int capacity) {
            this.capacity = capacity;
            return this;
        }

        public Builder setMembers(final List<UUID> members) {
            usersInThatGroup.addAll(members);
            return this;
        }

        public Builder setInterval(final String interval) {
            this.recurringInterval = interval;
            return this;
        }

        public Builder setTransportMode(final TransportMode transportMode) {
            this.transportMode = transportMode;
            return this;
        }

        public Builder setFemaleOnly(final boolean femaleOnly) {
            this.femaleOnly = femaleOnly;
            return this;
        }

        public Builder setCreatedAt(final Date createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder setDepartedAt(final Date departedAt) {
            this.departedAt = Optional.of(departedAt);
            return this;
        }

        public Group build() {
            return new Group(
                this.id,
                this.start,
                this.destination,
                this.capacity,
                this.recurringInterval,
                this.transportMode,
                this.femaleOnly,
                this.owner,
                this.createdAt,
                this.departedAt
            );
        }
    }
}
