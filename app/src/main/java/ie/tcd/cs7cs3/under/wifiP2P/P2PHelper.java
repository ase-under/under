package ie.tcd.cs7cs3.under.wifiP2P;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.util.Log;

import androidx.core.content.ContextCompat;
import com.google.common.base.Strings;
import ie.tcd.cs7cs3.under.GroupManager.Group;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * P2PHelper is a wrapper class for insulating users from the eldritch horrors contained within
 * Android's WiFi-Direct implementation.
 *
 * Implementation is mostly based off https://git.cs.usask.ca/406tablet/FeudalDoodles/-/blob/1be02b259846ff42fb64efc9285d532481bd897a/AndroidStudioProjects/WifiP2P/wifip2p/src/main/java/com/example/wifip2p/ServiceDiscovery.java
 *
 * <p>Broadly speaking, you want to first create a new instance:
 *
 * <p>p2pHelper = new P2PHelper(yourActivity);
 *
 * <p>Then you probably want to start advertising a group on a given port:
 *
 * <p>p2pHelper.advertiseService(myTravelGroup, somePort);
 *
 * <p>This automatically starts service discovery. You can get then get the list of discovered
 * services by calling:
 *
 * <p>final List<GroupService> services = p2pHelper.getDiscoveredServices();
 *
 * <p>Note: this may take a while to populate fully.
 *
 * <p>When you're done, you should stop advertising services and stop service discovery:
 *
 * <p>p2pHelper.cancelAdvertiseService(); p2pHelper.cancelDiscoverServices();
 */
public class P2PHelper {

  public static final String SERVICE_TYPE = "_undergroup";

  /**
   * GroupService is a class to represent the fields of a Group as they appear when serialized as a
   * TXT record.
   */
  public class GroupService {

    public String mInstanceName;
    public String mIpAddress;
    public String mMacAddress;
    public String mListenPort;
    public String mOwner;
    public String mStartLat;
    public String mStartLng;
    public String mDest;
    public String mDestLat;
    public String mDestLng;
    public String mCapacity;

    GroupService() {
    }

    /**
     * toString() returns the human-readable string representation of a GroupService
     */
    public String toString() {
      return String.format(
          "<Service instanceName=%s ipAddress=%s macAddress=%s listenPort=%s owner=%s startLat=%s startLng=%s dest=%s destLat=%s destLng=%s capacity=%s",
          Strings.nullToEmpty(mInstanceName),
          Strings.nullToEmpty(mIpAddress),
          Strings.nullToEmpty(mMacAddress),
          Strings.nullToEmpty(mListenPort),
          Strings.nullToEmpty(mOwner),
          Strings.nullToEmpty(mStartLat),
          Strings.nullToEmpty(mStartLng),
          Strings.nullToEmpty(mDest),
          Strings.nullToEmpty(mDestLat),
          Strings.nullToEmpty(mDestLng),
          Strings.nullToEmpty(mCapacity));
    }

    /**
     * clone() returns a deep copy of a given GroupService.
     *
     * @return the cloned GroupService.
     */
    public GroupService clone() {
      GroupService clonedService = new GroupService();
      clonedService.mInstanceName = mInstanceName;
      clonedService.mIpAddress = mIpAddress;
      clonedService.mMacAddress = mMacAddress;
      clonedService.mListenPort = mListenPort;
      clonedService.mOwner = mOwner;
      clonedService.mStartLat = mStartLat;
      clonedService.mStartLng = mStartLng;
      clonedService.mDest = mDest;
      clonedService.mDestLat = mDestLat;
      clonedService.mDestLng = mDestLng;
      clonedService.mCapacity = mCapacity;
      return clonedService;
    }
  }

  private static final String DEBUG_TAG = "P2PHelper";

  private Activity mActivity;
  private WifiManager mWifiManager;
  private WifiP2pManager mManager;
  private WifiP2pManager.Channel mChannel;
  private WifiDirectBroadcastReceiver mReceiver;
  private IntentFilter mIntentFilter;

  private Map<String, GroupService> mDiscoveredServices;

  // In order to be able to delete the service request, we need to hold a reference to it.
  private WifiP2pDnsSdServiceRequest mServiceRequest;

  /**
   * Constructor. Creates a new P2PHelper for the given activity.
   */
  public P2PHelper(Activity activity) {
    mActivity = activity;
    mWifiManager = (WifiManager) mActivity.getApplicationContext()
        .getSystemService(Context.WIFI_SERVICE);
    mManager = (WifiP2pManager) mActivity.getSystemService(Context.WIFI_P2P_SERVICE);
    mChannel = mManager.initialize(mActivity, mActivity.getMainLooper(), null);
    mReceiver = new WifiDirectBroadcastReceiver(mManager, mChannel,
        (WifiP2PListenerActivity) mActivity);
    mIntentFilter = new IntentFilter();
    mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
    mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
    mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
    mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
    mActivity.registerReceiver(mReceiver, mIntentFilter);

    mDiscoveredServices = new HashMap<String, GroupService>();
  }

  /**
   * advertiseService() advertises a Group via WiFi-Direct.
   *
   * <p>Remote devices will be able to see it by calling discoverServices().
   *
   * <p>Note: we automatically call discoverServices() on success. For some reason, this is needed
   * for the service to be discoverable. It may also require a blood sacrifice.
   *
   * @param g the group to be advertised.
   * @param listenPort the port on which clients should connect
   */
  public void advertiseService(Group g, int listenPort) {

    Map serviceRecord = new HashMap<String, String>();
    serviceRecord.put("owner", g.getOwner().toString());
    serviceRecord.put("listenPort", String.valueOf(listenPort));
    serviceRecord.put("startLat", String.valueOf(g.getStart().getLat()));
    serviceRecord.put("startLng", String.valueOf(g.getStart().getLng()));
    serviceRecord.put("dest", g.getDestination().getAddress());
    serviceRecord.put("destLat", String.valueOf(g.getDestination().getLat()));
    serviceRecord.put("destLng", String.valueOf(g.getDestination().getLng()));
    serviceRecord.put("capacity", String.valueOf(g.getCapacity()));

    final WifiP2pDnsSdServiceInfo serviceInfo =
        WifiP2pDnsSdServiceInfo.newInstance(
            g.getOwner().toString(), SERVICE_TYPE + "._tcp", serviceRecord);

    mManager.clearLocalServices(
        mChannel,
        new WifiP2pManager.ActionListener() {
          @Override
          public void onSuccess() {
            Log.d(DEBUG_TAG, "clearLocalServices: success!");

            if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
              Log.e(DEBUG_TAG, "permission not granted");
              return;
            }
            mManager.addLocalService(
                mChannel,
                serviceInfo,
                new WifiP2pManager.ActionListener() {
                  @Override
                  public void onSuccess() {
                    Log.d(DEBUG_TAG, "startService: success");
                    discoverServices(SERVICE_TYPE);
                  }

                  @Override
                  public void onFailure(int reason) {
                    Log.d(DEBUG_TAG, "startService: failure: " + reason);
                  }
                });
          }

          @Override
          public void onFailure(int reason) {
            Log.d(DEBUG_TAG, "clearLocalServices: failure: " + reason);
          }
        });
  }

  /**
   * discoverServices() discovers all nearly services matching @param filterInServiceType
   *
   * <p>Use getDiscoveredServices() afterwards to retrieve queried discovered services.
   *
   * @param filterInServiceType: type of service to discover (e.g. "_undergroup")
   */
  public void discoverServices(final String filterInServiceType) {
    // First, clear all services that we know about.
    mDiscoveredServices.clear();

    // This gets invoked when the WifiP2p Framework sends us an SRV record.
    // XXX: this actually does nothing, but Android's WiFi-Direct framework wants one.
    WifiP2pManager.DnsSdServiceResponseListener serviceListener =
        new WifiP2pManager.DnsSdServiceResponseListener() {
          @Override
          public void onDnsSdServiceAvailable(
              String serviceInstanceName, String serviceType, WifiP2pDevice serviceDevice) {
            Log.d(
                DEBUG_TAG,
                String.format("got DnsSdService: %s.%s", serviceInstanceName, serviceType));

            if (serviceType.contains(filterInServiceType)) {
              Log.d(
                  DEBUG_TAG,
                  String.format(
                      "DnsSdService got through filter: %s.%s ", serviceInstanceName, serviceType));
            }
          }
        };

    // This gets invoked when the WifiP2P framework sends us a TXT record
    WifiP2pManager.DnsSdTxtRecordListener txtListener =
        new WifiP2pManager.DnsSdTxtRecordListener() {
          @Override
          public void onDnsSdTxtRecordAvailable(
              String serviceDomain,
              Map<String, String> serviceRecord,
              WifiP2pDevice serviceDevice) {
            // If this fires, we have a TXT record that describes a Wifi-P2P service.
            Log.d(
                DEBUG_TAG,
                String.format(
                    "got DnsSdTxtRecord: serviceDomain:%s macAddr:%s record:%s",
                    serviceDomain, serviceDevice.deviceAddress, serviceRecord.toString()));

            // Ensure that we actually care about this service type.
            if (!Pattern.compile(Pattern.quote(filterInServiceType), Pattern.CASE_INSENSITIVE)
                .matcher(serviceDomain)
                .find()) {
              return;
            }

            Log.d(DEBUG_TAG, "DnsSdTxtRecord got through filter: " + serviceDomain);

            // Otherwise, we do care about this TXT record and should take a peek inside
            GroupService incomingService = new GroupService();
            incomingService.mOwner = serviceRecord.get("owner");
            incomingService.mListenPort = serviceRecord.get("listenPort");
            incomingService.mMacAddress = serviceDevice.deviceAddress;
            incomingService.mInstanceName = serviceRecord.get("owner");
            incomingService.mStartLat = serviceRecord.get("startLat");
            incomingService.mStartLng = serviceRecord.get("startLng");
            incomingService.mDest = serviceRecord.get("dest");
            incomingService.mDestLat = serviceRecord.get("destLat");
            incomingService.mDestLng = serviceRecord.get("destLng");
            incomingService.mCapacity = serviceRecord.get("capacity");

            mDiscoveredServices.put(incomingService.mOwner, incomingService);
          }
        };

    // Listen to both the SRV records and TXT records that the WifiP2pManager gives us.
    // We only really need the TXT records.
    mManager.setDnsSdResponseListeners(mChannel, serviceListener, txtListener);

    // Store the reference to our stored service request so that it can be properly deleted
    // later if required.
    mServiceRequest = WifiP2pDnsSdServiceRequest.newInstance();

    // It is necessary to first clear active service requests before adding a new one.
    mManager.clearServiceRequests(
        mChannel,
        new WifiP2pManager.ActionListener() {
          @Override
          public void onSuccess() {
            Log.d(DEBUG_TAG, "clearServiceRequests: success");

            // If we get this far, we can add our service request
            mManager.addServiceRequest(
                mChannel,
                mServiceRequest,
                new WifiP2pManager.ActionListener() {
                  @Override
                  public void onSuccess() {
                    Log.d(DEBUG_TAG, "addServiceRequest: success");
                  }

                  @Override
                  public void onFailure(int reason) {
                    Log.d(DEBUG_TAG, "addServiceRequest: failure: " + reason);
                  }
                });

            if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
              Log.e(DEBUG_TAG, "permission not granted");
              return;
            }
            // Once we've added our service request, we can begin service discovery.
            mManager.discoverServices(
                mChannel,
                new WifiP2pManager.ActionListener() {
                  @Override
                  public void onSuccess() {
                    Log.d(DEBUG_TAG, "discoverServices: success.");
                  }

                  @Override
                  public void onFailure(int reason) {
                    Log.d(DEBUG_TAG, "discoverServices: failure:" + reason);
                  }
                });
          }

          @Override
          public void onFailure(int reason) {
            Log.d(DEBUG_TAG, "clearServiceRequests: Failure");
          }
        });
  }

  /**
   * connectToService() connects to a discovered service. The device will become a peer in the
   * service's WiFi-Direct Network.
   *
   * @param macAddress: the MAC address of the service owner. This can be retrieved from
   * getDiscoveredServices()
   */
  public void connectToService(String macAddress) {

    WifiP2pConfig cfg = new WifiP2pConfig();
    cfg.deviceAddress = macAddress;
    cfg.wps.setup = WpsInfo.PBC;

    if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.INTERNET)
        != PackageManager.PERMISSION_GRANTED) {
      Log.e(DEBUG_TAG, "permission not granted");
      return;
    }
    mManager.connect(
        mChannel,
        cfg,
        new WifiP2pManager.ActionListener() {
          @Override
          public void onSuccess() {
            Log.d(DEBUG_TAG, "connectToService: success");
          }

          @Override
          public void onFailure(int reason) {
            Log.d(DEBUG_TAG, "connectToService: failure: " + reason);
          }
        });
  }

  /**
   * disconnectFromService disconnects from the current Wi-Fi Direct Group.
   */
  public void disconnectFromService() {
    mManager.removeGroup(
        mChannel,
        new WifiP2pManager.ActionListener() {
          @Override
          public void onSuccess() {
            Log.d(DEBUG_TAG, "disconnectFromService: success");
          }

          @Override
          public void onFailure(int reason) {
            Log.d(DEBUG_TAG, "disconnectFromService: failure");
          }
        });
  }

  /**
   * getDiscoveredServices() returns the GroupServices currently known to P2PManager.
   *
   * <p>discoverServices() must have been called beforehand.
   */
  public List<GroupService> getDiscoveredServices() {
    List<GroupService> rv = new ArrayList<GroupService>();

    for (Map.Entry<String, GroupService> entry : mDiscoveredServices.entrySet()) {
      GroupService service = entry.getValue();
      rv.add(service.clone());
    }

    return rv;
  }

  /**
   * cancelDiscoverService() stops searching for services via WiFi-Direct.
   *
   * <p>This doesn't disconnect from any connected services. If you need to do this, call
   * disconnectFromService() instead.
   *
   * @precond discoverServices() was called beforehand.
   */
  public void cancelDiscoverServices() {
    mManager.clearServiceRequests(
        mChannel,
        new WifiP2pManager.ActionListener() {
          @Override
          public void onSuccess() {
            Log.d(DEBUG_TAG, "cancelDiscoverServices: success");
          }

          @Override
          public void onFailure(int reason) {
            Log.d(DEBUG_TAG, "cancelDiscoverServices: failure");
          }
        });
    mDiscoveredServices.clear();
  }

  /**
   * canedlAdvertiseService() stops advertising the service.
   *
   * <p>This doesn't disconnect from any connected services. If you need to do this, call
   * disconnectFromService() instead.
   *
   * @precond advertiseService() was called beforehand.
   */
  public void cancelAdvertiseService() {
    mManager.clearLocalServices(
        mChannel,
        new WifiP2pManager.ActionListener() {
          @Override
          public void onSuccess() {
            Log.d(DEBUG_TAG, "cancelAdvertiseService: success");
          }

          @Override
          public void onFailure(int reason) {
            Log.d(DEBUG_TAG, "cancelAdvertiseService: failure");
          }
        });
  }

  /**
   * connectToDevice wraps the stuff about connecting to another wifip2p device // TODO: figure out
   * what we need to do with ports? idk
   */
  public void connectToDevice(String deviceMacAddr, WifiP2pManager.ActionListener actionListener)
      throws SecurityException {
    final WifiP2pConfig wifiP2pConfig = new WifiP2pConfig();
    wifiP2pConfig.deviceAddress = deviceMacAddr;
    wifiP2pConfig.wps.setup = WpsInfo.PBC;
    this.mManager.connect(mChannel, wifiP2pConfig, actionListener);
  }

  // some wrapper methods around wifi-manager stuff
  public boolean isWifiEnabled() {
    return mWifiManager.isWifiEnabled();
  }

  public boolean enableWifi() {
    return mWifiManager.setWifiEnabled(true);
  }

  public boolean disableWifi() {
    return mWifiManager.setWifiEnabled(false);
  }

  public void registerReceiver() {
    mActivity.registerReceiver(mReceiver, mIntentFilter);
  }

  public void unregisterReceiver() {
    mActivity.unregisterReceiver(mReceiver);
  }

  public int getOpenPort() {
    ServerSocket s = null;
    try {
      s = new ServerSocket(0);
      int port = s.getLocalPort();
      Log.d(DEBUG_TAG, "getOpenPort: " + port);
      return port;
    } catch (IOException e) {
      e.printStackTrace();
      Log.d(DEBUG_TAG, "getOpenPort: failure");
      return -1;
    }
  }

  public boolean closePort(int port) {
    ServerSocket s = null;
    try {
      s = new ServerSocket(port);
      s.close();
      Log.d(DEBUG_TAG, "closePort: success");
      return true;
    } catch (IOException e) {
      e.printStackTrace();
      Log.d(DEBUG_TAG, "closePort: failure");
      return false;
    }
  }
}
