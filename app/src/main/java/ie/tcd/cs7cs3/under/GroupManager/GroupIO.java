package ie.tcd.cs7cs3.under.GroupManager;

import android.os.Handler;
import java.util.List;

/**
 * GroupIO is an abstraction layer over Group-related operations over P2P networks and the central service.
 *  - It maintains a cache of available Groups ({@link GroupIO#nearbyGroups})
 *  - It listens for nearby groups and updates its local cache ({@link GroupIO#startListening}, {@link GroupIO#stopListening})
 *  - It handles creation or deletion of groups ({@link GroupIO#startAdvertising}, {@link GroupIO#stopAdvertising})
 *  - It handles requests to join or leave groups ({@link GroupIO#requestJoinGroup}, {@link GroupIO#requestLeaveGroup})
 */
public interface GroupIO {

  public boolean isOffline();

  public void setOffline(final boolean offline);

  public List<Group> nearbyGroups() throws GroupIOException;

  public void startListening();

  public void stopListening();

  public void startAdvertising(final Group myGroup) throws GroupIOException;

  public void stopAdvertising(final Group myGroup) throws GroupIOException;

  public void requestJoinGroup(final JoinRequest req, final Handler.Callback onSuccess,
      final Handler.Callback onFailure);

  public void requestLeaveGroup(final LeaveRequest req, final Handler.Callback onSuccess,
      final Handler.Callback onFailure);
}
