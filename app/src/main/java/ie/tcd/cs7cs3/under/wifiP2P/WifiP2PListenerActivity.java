package ie.tcd.cs7cs3.under.wifiP2P;

import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;

public interface WifiP2PListenerActivity {

  //    void AddPeersAvailable(WifiP2pDeviceList availablePeers);
  WifiP2pManager.ConnectionInfoListener GetConnectionInfoListener();

  void SetConnectionStatusText(String text);

  // this gets called by P2PServer when a client sends it an echo message.
  void onEchoReceived(String msg);
}
