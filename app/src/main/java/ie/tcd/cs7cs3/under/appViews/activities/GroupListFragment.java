package ie.tcd.cs7cs3.under.appViews.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mapbox.geojson.Point;

import java.util.ArrayList;
import java.util.List;

import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOException;
import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.appViews.adapter.GroupListAdapter;
import ie.tcd.cs7cs3.under.appViews.model.groupData;

import static ie.tcd.cs7cs3.under.appViews.activities.UserHome.mGroupIOImpl;


public class GroupListFragment extends Fragment {
    String TAG = "GroupListFragment";
    List<groupData> list = new ArrayList<>();
    List<Group> groupList;

    private String mUserName=null;
    private String mUserGender=null;
    private String mUserSourcePt = null;
    private String mUserDestPt = null;
    private Gson gson = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG,"onCreateView");
        gson = new Gson();
        mUserName = getArguments().getString("NAME");
        mUserGender = getArguments().getString("GENDER");//TODO Vishal: gender others cant create female only
        mUserSourcePt = getArguments().getString("SRC_PT");
        mUserDestPt = getArguments().getString("DEST_PT");

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_group_list, container, false);
        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        // 2. set layoutManger
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // set data for recycler view ##get real data here

        list = getData();

        // 3. create an adapter
        GroupListAdapter grouplistAdapter = new GroupListAdapter(list, getActivity());
        // 4. set adapter
        recyclerView.setAdapter(grouplistAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        final GestureDetector gestureDetector = new GestureDetector(getActivity(),new GestureDetector.SimpleOnGestureListener() {
            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                View child =recyclerView.findChildViewUnder(e.getX(),e.getY());
                if(child!=null && gestureDetector.onTouchEvent(e)) {
                    int position=recyclerView.getChildLayoutPosition(child);
                    String groupID = list.get(position).groupId;
                    String groupOwnerID=list.get(position).groupOwnerId;
                    String groupSrcPtLoc=gson.toJson(list.get(position).groupStartLoc);
                    String groupDestPtLoc=gson.toJson(list.get(position).groupDestLoc);
                    String groupTransportMode = list.get(position).tranportMode;
                    String GroupCapacityAllowed = list.get(position).capacityVal;
                    Boolean GroupfemaleOnly = list.get(position).femaleOnlyFlag;

                    GroupDetailFragment fragmentB=new GroupDetailFragment();
                    Bundle bundle=new Bundle();
                    //Group detail
                    bundle.putString("GR_ID", groupID);
                    bundle.putString("GR_OWNERID",groupOwnerID);
                    bundle.putString("GR_SRC_PT_LOC",groupSrcPtLoc);//LOC type
                    bundle.putString("GR_DEST_PT_LOC",groupDestPtLoc);
                    bundle.putString("GR_TRNS",groupTransportMode);
                    bundle.putString("GR_CAP",GroupCapacityAllowed);
                    bundle.putString("GR_FMLO",String.valueOf(GroupfemaleOnly));
                    //Current User Detail
                    bundle.putString("USER_NAME",mUserName);
                    bundle.putString("USER_SRC_PT",mUserSourcePt);
                    bundle.putString("USER_DEST_PT",mUserDestPt);
                    bundle.putString("CURUSERINFO",getArguments().getString("CURUSERINFO"));

                    fragmentB.setArguments(bundle);

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.listcontainer, fragmentB).addToBackStack(null).commit();

                }
                return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        return rootView;
    }

    public void loadGroupDetailFragment(Fragment fragment, int position) {
        Log.d(TAG,"loadGroupDetailFragment");
        // Create a new Fragment to be placed in the activity layout
        GroupDetailFragment groupDetailFragment = new GroupDetailFragment();
        Bundle args = new Bundle();
        args.putInt("ARG_POSITION", position);
        groupDetailFragment.setArguments(args);

        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments
        //groupDetailFragment.setArguments(getIntent().getExtras());

        // Add the fragment to the 'fragment_container' FrameLayout
        // when perform fragment transactions, such as replace or remove one, it's often appropriate to
        // allow the user to navigate backward and "undo" the change. To allow the user to navigate
        // backward through the fragment transactions, must call addToBackStack() before
        // commit the FragmentTransaction.
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.listcontainer, groupDetailFragment).addToBackStack(null).commit();
    }

    // Sample data for RecyclerView
    private List<groupData> getData() {

        try {
            groupList = mGroupIOImpl.nearbyGroups();
            for (final Group gp : groupList) {
                if(gp.isFemaleOnly()==true && mUserGender.equals("Others")){
                    Log.d(TAG,"Not added Group owner"+gp.getOwner()+
                            " start Lat="+gp.getStart().getLat()+" Lng="+gp.getStart().getLng()+
                            " dest Lat="+gp.getDestination().getLat()+" Lng="+gp.getDestination().getLng()+
                            " mUserGender="+mUserGender);
                    continue;
                }
                list.add(new groupData(
                        gp.getId().isPresent() ? gp.getId().get().toString() : "Offline Group",
                        gp.getOwner().toString(),
                        gp.getTransportMode().toString(),
                        gp.getStart(),
                        gp.getDestination(),
                        String.valueOf(gp.getCapacity()),
                        gp.isFemaleOnly()));
                Log.d(TAG,"Group owner"+gp.getOwner()+
                        " start Lat="+gp.getStart().getLat()+" Lng="+gp.getStart().getLng()+
                        " dest Lat="+gp.getDestination().getLat()+" Lng="+gp.getDestination().getLng());
            }
        } catch (GroupIOException e) {
            e.printStackTrace();
        }
        return list;
    }


}