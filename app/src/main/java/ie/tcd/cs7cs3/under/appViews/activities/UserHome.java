package ie.tcd.cs7cs3.under.appViews.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;


import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.maps.UiSettings;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;
import com.mapbox.mapboxsdk.plugins.offline.model.NotificationOptions;
import com.mapbox.mapboxsdk.plugins.offline.model.OfflineDownloadOptions;
import com.mapbox.mapboxsdk.plugins.offline.offline.OfflinePlugin;
import com.mapbox.mapboxsdk.plugins.offline.utils.OfflineUtils;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import ie.tcd.cs7cs3.under.util.TerribleUUIDUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ie.tcd.cs7cs3.under.GroupManager.Group;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOException;
import ie.tcd.cs7cs3.under.GroupManager.GroupIOImpl;
import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.appViews.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset;

public class UserHome extends AppCompatActivity implements MapboxMap.OnMapClickListener,
        PermissionsListener,
        NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback{
    private final AppCompatActivity activity = UserHome.this;
    private double radius = 2000;


    private MapView mapView;
    MapboxMap mapBoxMap;
    private ImageView mapdownload;
    private long backPressed;
    private Toast backToast;

    private ActionBarDrawerToggle actionBarDrawerToggle;
    public static NavigationView navigationView;
    private TextView logoutView;

    private DrawerLayout drawer;
    private Toolbar toolbar;

    LocationComponent locationComponent;
    PermissionsManager permissionManager;
    DirectionsRoute currentRoute;
    Point originPoint, destinationPoint;
    NavigationMapRoute navigationMapRoute;

    double destinationLatitude;
    double destinationLongitude;

    double originLatitude, originLongitude;

    AppCompatEditText mEditTextDest;
    AppCompatEditText mEditTextorigin;
    AppCompatButton mAppCompatJoinGrBtn;
    AppCompatButton mAppCompatCreateGrBtn;

    Switch mOfflineSwitch;
    TextView mOfflineSwitchLabel;
    TextView textViewUserProfile;
    TextView textViewUserProfileDetail;


    private OfflineTilePyramidRegionDefinition offlineDefinition;

    /**
     * Variables for search
     *@uthor : Chavvi Chandani */
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    String symbolIconId = "symbolIconId";
    FloatingActionButton searchLocationButton;
    private String geojsonSourceLayerId = "geojsonSourceLayerId";


    private static final String TAG = "UserHome";
    static String mUserEmail;

    ConnectionsClient mConnectionsClient;
    static GroupIOImpl mGroupIOImpl;
    static boolean mCurrentUserJoined=false;
    static boolean mCurrentUserCreated=false;
    static String mCurrentUserSource=null;
    static String mCurrentUserDest=null;
    static boolean mGroupPublished=false;
    List<Group> groupList;
    String originJson = null;
    String destJson = null;
    private User curUserInfo;
    private String curUserUUID = null;
    private String curUserName=null;
    private String curUserGender=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO(cian): do network stuff async
        Log.d(TAG,"onCreate");
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);

        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_user_home);

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        mapdownload =findViewById(R.id.mapdownload);

        mEditTextDest = findViewById(R.id.editDest);
        mEditTextorigin = findViewById(R.id.originLoc);

        mAppCompatJoinGrBtn = findViewById(R.id.appCompatJoinGrBtn);
        mAppCompatCreateGrBtn = findViewById(R.id.appCompatCreateGrBtn);
        mAppCompatJoinGrBtn.setVisibility(View.GONE);
        mAppCompatCreateGrBtn.setVisibility(View.GONE);

        Gson gson = new Gson();
        curUserInfo = gson.fromJson(getIntent().getStringExtra("CURUSERINFO"), User.class);
        curUserName = curUserInfo.getName();
        curUserGender = curUserInfo.getGender();
        // TODO: Assign each user a unique UUID at creation.
        curUserUUID = TerribleUUIDUtil.terribleUUIDFromString(curUserName).toString();

        mConnectionsClient = Nearby.getConnectionsClient(this);
        mGroupIOImpl = new GroupIOImpl(curUserUUID, mConnectionsClient);

        mOfflineSwitch = findViewById(R.id.offline_switch);
        mOfflineSwitchLabel = findViewById(R.id.label_offline_switch);
        mOfflineSwitchLabel.setText(mGroupIOImpl.isOffline() ? "Offline" : "Online");

        textViewUserProfile = findViewById(R.id.user_name);
        textViewUserProfileDetail = findViewById(R.id.profileDetail);
        textViewUserProfile.setText(curUserInfo.getName());
        textViewUserProfileDetail.setText(curUserInfo.getEmail());

        mapdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NotificationOptions notificationOptions = NotificationOptions.builder(getApplicationContext())
                        .smallIconRes(R.drawable.mapbox_logo_icon)
                        .returnActivity(UserHome.class.getName())
                        .build();

                OfflinePlugin.getInstance(getApplicationContext()).startDownload(
                        OfflineDownloadOptions.builder()
                                .definition(offlineDefinition)
                                .metadata(OfflineUtils.convertRegionName("Dublin"))
                                .notificationOptions(notificationOptions)
                                .build()
                );

            }
        });

        Log.d(TAG,"onCreate curUserName="+curUserName+" curUserGender="+curUserGender);

        mOfflineSwitch.setOnClickListener(v -> {
            final boolean currOffline = mGroupIOImpl.isOffline();
            mGroupIOImpl.setOffline(!currOffline);
            mOfflineSwitchLabel.setText(mGroupIOImpl.isOffline() ? "Offline" : "Online");
            Log.d(TAG, String.format("Set offline %s -> %s", currOffline, !currOffline));
        });

        mAppCompatJoinGrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"onClick() mAppCompatJoinGrBtn");
                //start group join actvity;
                Intent intentRegister = new Intent(getApplicationContext(),JoinGroupList.class);// exam.class); //JoinGroupList
                intentRegister.putExtra("JOB","JOIN");
                intentRegister.putExtra("NAME",curUserName);
                intentRegister.putExtra("EMAIL",curUserInfo.getEmail());
                intentRegister.putExtra("GENDER",curUserGender);
                intentRegister.putExtra("CURUSERINFO",getIntent().getStringExtra("CURUSERINFO"));
                originJson = gson.toJson(originPoint);
                intentRegister.putExtra("SRC_PT",originJson);
                destJson= gson.toJson(destinationPoint);
                intentRegister.putExtra("DEST_PT",destJson);
                startActivity(intentRegister);
                mCurrentUserJoined=true;
            }
        });
        mAppCompatCreateGrBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"onClick() mAppCompatCreateGrBtn");
                mGroupIOImpl.stopListening();
                Intent intentRegister = new Intent(getApplicationContext(),JoinGroupList.class);
                intentRegister.putExtra("JOB","CREATE");
                intentRegister.putExtra("NAME",curUserName);
                intentRegister.putExtra("EMAIL",curUserInfo.getEmail());
                intentRegister.putExtra("GENDER",curUserGender);
                intentRegister.putExtra("SRC",mCurrentUserSource);
                intentRegister.putExtra("DEST",mCurrentUserDest);
                originJson = gson.toJson(originPoint);
                intentRegister.putExtra("SRC_PT",originJson);
                destJson= gson.toJson(destinationPoint);
                intentRegister.putExtra("DEST_PT",destJson);
                startActivity(intentRegister);
                mCurrentUserCreated=true;
            }
        });

        searchLocationButton = findViewById(R.id.fab_location_search);
        searchLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken(Mapbox.getAccessToken() != null ? Mapbox.getAccessToken() :
                                getString(R.string.access_token))
                        .placeOptions(PlaceOptions.builder()
                                .backgroundColor(Color.parseColor("#EEEEEE"))
                                .limit(10)
                                .proximity(destinationPoint)
                                .build(PlaceOptions.MODE_CARDS))
                        .build(UserHome.this);
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
            }
        });

        drawer = (DrawerLayout)findViewById(R.id.home_drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        String emailFromIntent = getIntent().getStringExtra("EMAIL");
        setUserInfo(emailFromIntent);


        //openDrawer();
        setToolbar();

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

        invalidateOptionsMenu();


        // start background thread to update the cached list of  group details
        Log.d(TAG,"startListening GroupIO");
        mGroupIOImpl.startListening();

    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {

        this.mapBoxMap = mapboxMap;
//        this.mapBoxMap.setMinZoomPreference(15);
        this.mapBoxMap.setMaxZoomPreference(22);
        this.mapBoxMap.setMinZoomPreference(12);
        this.mapBoxMap.getUiSettings().setCompassEnabled(false);

        mapboxMap.setStyle(Style.LIGHT, new Style.OnStyleLoaded()
        {
            @Override
            public void onStyleLoaded(@NonNull Style style)
            {
                searchLocationButton.setEnabled(true);




                //offline map integration begins here

                offlineDefinition = new OfflineTilePyramidRegionDefinition(
                        style.getUri(),

                        //lat long for dublin area HARDCODED as API provides 6000 tiles these locations with full zoom is around 5964 tiles
                        //estimator website - https://docs.mapbox.com/playground/offline-estimator/
                        new LatLngBounds.Builder()
                                .include(new LatLng(53.2551, -6.3948))
                                .include(new LatLng(53.4737, -6.0471))
                                .build(),
                        0, 22,
                        getResources().getDisplayMetrics().density
                );






                enableLocationComponent(style);
                addDestinationIconLayer(style);
                mapboxMap.addOnMapClickListener(UserHome.this);



                // Add the symbol layer icon to map for future use
                style.addImage(symbolIconId, BitmapFactory.decodeResource(
                        UserHome.this.getResources(), R.drawable.map_default_map_marker));

                // Create an empty GeoJSON source using the empty feature collection
                setUpSource(style);

                // Set up a new symbol layer for displaying the searched location's feature coordinates
                setupLayer(style);



            }
        });
    }

    /**
     * @param loadedMapStyle
     * Creates an empty GeoJSON source using the empty feature collection (for Search)
     * @ author : Chavvi Chandani
     */
    private void setUpSource(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addSource(new GeoJsonSource(geojsonSourceLayerId));
    }

    /**
     * @param loadedMapStyle
     * Set up a new symbol layer for displaying the searched location's feature coordinates
     * @author : Chavvi Chandani
     */
    private void setupLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addLayer(new SymbolLayer("SYMBOL_LAYER_ID", geojsonSourceLayerId).withProperties(iconImage(symbolIconId), iconOffset(new Float[]{0f, -8f})));
    }

    /**
     * Sets a point as destinantion when the user clicks on the map
     * Calling the getRoute function  to get a route between the current location and destinantion selected
     * Enables the Start Navigation button
     *
     * @uthor : Chavvi Chandani
     **/
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        //System.out.println("Reached Map CLick function");
        Log.d(TAG,"onMapClick Point="+point);
        //point gives the location of anywhere you tap on screen - latitude and longitude
        getDestinationPoint(point);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //TO DO
        }

        GeoJsonSource source = mapBoxMap.getStyle().getSourceAs("destination-source-id");
        if (source != null){
            source.setGeoJson(Feature.fromGeometry(destinationPoint));
        }
        destinationLatitude = getDestinationLatitude(point);
        destinationLongitude = getDestinationLongitude(point);

        originPoint = Point.fromLngLat(locationComponent.getLastKnownLocation().getLongitude(),
                locationComponent.getLastKnownLocation().getLatitude());

        // Extracting Origin Latitude and Longitude
        originLatitude = originPoint.latitude();
        originLongitude = originPoint.longitude();
        Log.d(TAG,"onMapClick originPoint="+originPoint.coordinates().toString()+" destinationPoint"+destinationPoint.coordinates().toString());
        getRoute(originPoint,destinationPoint);

        //Setting current location to show at current location.
        try {
            mCurrentUserSource = reverseGeocoding(originLatitude,originLongitude,this);
            mEditTextorigin.setText(mCurrentUserSource);
            Log.d(TAG,"Current User Origin Address:" +mCurrentUserSource);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        mEditTextorigin.setText(originPoint.coordinates().toString());
//        mEditTextDest.setText(destinationPoint.coordinates().toString());
        try {
            mCurrentUserDest = reverseGeocoding(destinationLatitude,destinationLongitude,this);
            mEditTextDest.setText(mCurrentUserDest);
            Log.d(TAG,"Current USer Destination Address:" +mCurrentUserDest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mAppCompatJoinGrBtn.setVisibility(View.VISIBLE);
        mAppCompatCreateGrBtn.setVisibility(View.VISIBLE);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        Log.d(TAG,"onOptionsItemSelected item="+item);
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.END); //OPEN Nav Drawer!
        }
        else if (backPressed+2000>System.currentTimeMillis())
        {
            backToast.cancel();
            super.onBackPressed();
            return;
        }
        else
        {
            backToast=Toast.makeText(getBaseContext(),"get press again to exit",Toast.LENGTH_SHORT);
            backToast.show();
            finish();
        }
        backPressed=System.currentTimeMillis();
        Log.d(TAG,"onBackPressed ");

    }

    /** Find a route between the origin and the destination
     * @author: Chavvi Chandani **/
    private void getRoute(Point origin , Point destination) {
        Log.d(TAG,"getRoute started ");
        NavigationRoute.builder(this).accessToken(Mapbox.getAccessToken()).origin(origin).destination(destination).build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        if (response.body() == null) {
                            Log.e(TAG, "No routes foundm check right user and access token");
                            return;
                        } else if (response.body().routes().size() == 0) {
                            Log.e(TAG, "No routes found");
                            return;
                        }
                        currentRoute = response.body().routes().get(0);
                        if(navigationMapRoute != null) {
                            navigationMapRoute.removeRoute();
                        } else {
                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapBoxMap);
                        }
                        navigationMapRoute.addRoute(currentRoute);
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                        Log.e(TAG, "Error: " + t.getMessage());
                    }
                });
    }

    private void setUserInfo(String userInfo) {
        mUserEmail = userInfo;
    }

    private void openDrawer() {
        Log.d(TAG,"drawer opened");

        Intent intentRegister = new Intent(getApplicationContext(), Menu.class);
        intentRegister.putExtra("EMAIL", "abc@xyz.com");//mUserEmail.toString().trim());
        startActivity(intentRegister);
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle("");

        toolbar.findViewById(R.id.navigation_menu).setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                logoutView = (TextView) findViewById(R.id.logout);
                Log.e("Click", "keryu");

                if (drawer.isDrawerOpen(navigationView)) {
                    drawer.closeDrawer(navigationView);
                } else {
                    drawer.openDrawer(navigationView);
                    logoutView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getApplicationContext(), Login.class);
                            startActivity(intent);
                        }
                    });

                    Log.e("abc","abc");
                }
            }
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.home_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults); // takes care of permissions
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain)
    {
        // user denies the access to location the first time and when second time you request it you can present a dialogue to explain why the access is needed
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if(granted) {
            enableLocationComponent(mapBoxMap.getStyle());
        } else {
            Toast.makeText(getApplicationContext(), "Permission not granted", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mapView.onSaveInstanceState(outState);
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    /** Overriding Mapview Lifecycle methods **/
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private  void addDestinationIconLayer(Style style)
    {
        Log.d(TAG,"addDestinationIconLayer style="+style);
        style.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(),R.drawable.mapbox_marker_icon_default));

        GeoJsonSource geoJsonSource = new GeoJsonSource("destination-source-id");
        style.addSource(geoJsonSource);

        SymbolLayer destinationSymbolLayer = new SymbolLayer("destination-symbol-layer-id","destination-source-id");

        destinationSymbolLayer.withProperties(iconImage("destination-icon-id" ),iconAllowOverlap(true),
                iconIgnorePlacement(true));

        style.addLayer(destinationSymbolLayer);
    }

    private void enableLocationComponent(Style loadedMapStyle)
    {
        // Check if permissions are enabled and if not request
        Boolean locPerm = PermissionsManager.areLocationPermissionsGranted(this);
        Log.d(TAG, "Location Permission enabled = " + locPerm);
        if(PermissionsManager.areLocationPermissionsGranted(this))
        {
            // Get an instance of the component
            locationComponent = mapBoxMap.getLocationComponent();
            // Activate with a built LocationComponentActivationOptions object
            locationComponent.activateLocationComponent(this, loadedMapStyle);

            // Enable to make component visible, change to false to hide device location
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        }
        else
        {
            permissionManager = new PermissionsManager(this);
            permissionManager.requestLocationPermissions(this);
        }
    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * To move the pointer to the search location
     * @author : Chavvi Chandani
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            //Retrieve selected location's CarmenFeature
            CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);
            // Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above.
            // Then retrieve and update the source designated for showing a selected location's symbol layer icon
            if (mapBoxMap != null) {
                Style style = mapBoxMap.getStyle();
                if (style != null) {
                    GeoJsonSource source = style.getSourceAs(geojsonSourceLayerId);
                    if (source != null) {
                        source.setGeoJson(FeatureCollection.fromFeatures(new Feature[]{
                                Feature.fromJson(selectedCarmenFeature.toJson())}));
                    }
                    // Move map camera to the selected location
                    mapBoxMap.animateCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                                            (((Point) selectedCarmenFeature.geometry()).longitude())))
                                    .zoom(14)
                                    .build()), 4000);
                }
            }
        }
    }

    /** Getting destination point
     * @autor : Chavvi Chandani */
    public Point getDestinationPoint(@NonNull LatLng point) {
        destinationPoint = Point.fromLngLat(point.getLongitude(), point.getLatitude());
        return destinationPoint;
    }
    /** Extracting Latitude from Destination Point
     * @autor : Chavvi Chandani */
    public double getDestinationLatitude(LatLng destinationPoint) {
        destinationLatitude = getDestinationPoint(destinationPoint).latitude();
        return destinationLatitude;
    }
    /** Extracting Longitude from Destination Point
     * @autor : Chavvi Chandani */
    public double getDestinationLongitude(LatLng destinationPoint) {
        destinationLongitude = getDestinationPoint(destinationPoint).longitude();
        return destinationLongitude;
    }
    /** Converting Latitiude and Longitude to String Address
     * @autor : Chavvi Chandani */
    public static String reverseGeocoding(double lat, double lon, Context context) throws IOException {
        Geocoder geocoder;
        List<Address> addresses= new ArrayList<>();
        geocoder = new Geocoder(context, Locale.getDefault());
        addresses = geocoder.getFromLocation(lat, lon, 1);
        if (addresses.size() == 0) {
            return String.format("Lat: %f Lng: %f", lat, lon);
        }
        String address = addresses.get(0).getAddressLine(0);
        return address;
    }
}
