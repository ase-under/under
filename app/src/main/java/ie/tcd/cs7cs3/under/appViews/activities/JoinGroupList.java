package ie.tcd.cs7cs3.under.appViews.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;;

import com.google.android.material.navigation.NavigationView;

import ie.tcd.cs7cs3.under.R;

public class JoinGroupList extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "JoinGroupList";

    private DrawerLayout drawer;
    private Toolbar toolbar;

    private ActionBarDrawerToggle actionBarDrawerToggle;
    public static NavigationView navigationView;
    private TextView logoutView;
    private long backPressed;
    private Toast backToast;
    private String mUserName=null;
    private static String mUserEmail;
    private String mUserGender=null;
    private String mUserSource = null;
    private String mUserDest = null;
    private String mUserSourcePt = null;
    private String mUserDestPt = null;
    private String CURUSERINFO=null;

    TextView textViewUserProfile;
    TextView textViewUserProfileDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String intentJob = getIntent().getStringExtra("JOB");
        mUserName = getIntent().getStringExtra("NAME");
        mUserEmail = getIntent().getStringExtra("EMAIL");
        mUserGender = getIntent().getStringExtra("GENDER");
        mUserSource = getIntent().getStringExtra("SRC");
        mUserDest = getIntent().getStringExtra("DEST");
        mUserSourcePt = getIntent().getStringExtra("SRC_PT"); //originPoint
        mUserDestPt = getIntent().getStringExtra("DEST_PT");
        CURUSERINFO=getIntent().getStringExtra("CURUSERINFO");

        setContentView(R.layout.activity_join_group);
        if(intentJob.equals("JOIN")) {
            loadGroupListFragment(new GroupListFragment());
        }
        else {
            loadGroupCreateFragment(new GroupCreateFragment());
        }


        drawer = (DrawerLayout)findViewById(R.id.group_drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        textViewUserProfile = findViewById(R.id.user_name);
        textViewUserProfileDetail = findViewById(R.id.profileDetail);
        textViewUserProfile.setText(mUserName);
        textViewUserProfileDetail.setText(mUserEmail);

        setToolbar();
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

        invalidateOptionsMenu();
    }
    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setTitle("");

        toolbar.findViewById(R.id.navigation_menu).setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                logoutView = (TextView) findViewById(R.id.logout);
                Log.e("Click", "keryu");

                if (drawer.isDrawerOpen(navigationView)) {
                    drawer.closeDrawer(navigationView);
                } else {
                    drawer.openDrawer(navigationView);
                    logoutView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getApplicationContext(), Login.class);
                            startActivity(intent);
                        }
                    });

                    Log.e("abc","abc");
                }
            }
        });
    }

    private void loadGroupListFragment(Fragment fragment) {
        // Create a new Fragment to be placed in the activity layout
        GroupListFragment groupListFragment = new GroupListFragment();

        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments
        groupListFragment.setArguments(getIntent().getExtras());
        Bundle bundle=new Bundle();
//        Log.d(TAG,"username="+"Current_User_"+mUserName);
//        bundle.putString("OWNERID","Current_User_"+mUserName);
        bundle.putString("NAME",mUserName);
        bundle.putString("GENDER",mUserGender);
//        bundle.putString("SRC",mUserSource);
//        bundle.putString("DEST",mUserDest);
        bundle.putString("SRC_PT",mUserSourcePt);
        bundle.putString("DEST_PT",mUserDestPt);
        bundle.putString("CURUSERINFO",CURUSERINFO);

        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.listcontainer, groupListFragment).commit();
    }
    private void loadGroupCreateFragment(Fragment fragment) {
        // Create a new Fragment to be placed in the activity layout
        GroupCreateFragment groupCreateFragment = new GroupCreateFragment();

        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments
        groupCreateFragment.setArguments(getIntent().getExtras());
        Bundle bundle=new Bundle();
        Log.d(TAG,"username="+"Current_User_"+mUserName);
        bundle.putString("OWNERID","Current_User_"+mUserName);
        bundle.putString("NAME",mUserName);
        bundle.putString("GENDER",mUserGender);
        bundle.putString("SRC",mUserSource);
        bundle.putString("DEST",mUserDest);
        bundle.putString("SRC_PT",mUserSourcePt);
        bundle.putString("DEST_PT",mUserDestPt);

        groupCreateFragment.setArguments(bundle);
        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.listcontainer, groupCreateFragment).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG,"onBackPressed ");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        Log.d(TAG,"onOptionsItemSelected item="+item);
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        actionBarDrawerToggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.group_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}