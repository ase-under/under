package ie.tcd.cs7cs3.under.storage;

import android.content.Context;
import android.content.SharedPreferences;

import static java.lang.Boolean.FALSE;

public class Prefs {
    private static String MY_STRING_PREF = "mystringpref";
    private static String MY_INT_PREF = "myintpref";
    private static String MY_BOOL_PREF = "myintpref";

    private static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences("myprefs", 0);
    }

    //Get preferences
    public static String getStringPref(Context context) {
        return getPrefs(context).getString(MY_STRING_PREF, "default");
    }

    public static int getIntPref(Context context) {
        return getPrefs(context).getInt(MY_INT_PREF, 42);
    }

    public static boolean getBooleanPref(Context context) {
        return getPrefs(context).getBoolean(MY_BOOL_PREF, FALSE);
    }

    //Set Preferences
    public static void setStringPref(Context context, String value) {
        // perform validation etc..
        getPrefs(context).edit().putString(MY_STRING_PREF, value).apply();
    }

    public static void setIntPref(Context context, int value) {
        // perform validation etc..
        getPrefs(context).edit().putInt(MY_INT_PREF, value).apply();
    }

    public static void setBooleanPref(Context context, boolean value) {
        // perform validation etc..
        getPrefs(context).edit().putBoolean(MY_BOOL_PREF, value).apply();
    }

}