package ie.tcd.cs7cs3.under.appViews.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.appViews.activities.UserHome;
import ie.tcd.cs7cs3.under.appViews.model.groupData;

public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.groupViewHolder> {

    String TAG = "GroupListAdapter";
    List<groupData> list = Collections.emptyList();

    Context context;

    public GroupListAdapter(List<groupData> list, Context context)
    {
        Log.d(TAG,"GroupListAdapter Constructor");
        this.list = list;
        this.context = context;
    }

    @Override
    public groupViewHolder  onCreateViewHolder(ViewGroup parent, int viewType)
    {
        Log.d(TAG,"onCreateViewHolder");
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the layout
        View photoView = inflater.inflate(R.layout.group_card, parent, false);

        groupViewHolder viewHolder = new groupViewHolder(photoView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final groupViewHolder viewHolder, final int position)
    {
        Log.d(TAG,"onBindViewHolder");
        double groupStartLocLat = list.get(position).groupStartLoc.getLat();
        double groupStartLocLng = list.get(position).groupStartLoc.getLng();
        double groupDestLocLat = list.get(position).groupDestLoc.getLat();
        double groupDestLocLng = list.get(position).groupDestLoc.getLng();

        viewHolder.groupId.setText(list.get(position).groupId);
        viewHolder.groupOwnerId.setText(list.get(position).groupOwnerId);
        viewHolder.groupTranportMode.setText(list.get(position).tranportMode);
        try {
            viewHolder.groupSrcAddress.setText(UserHome.reverseGeocoding(groupStartLocLat,groupStartLocLng,context));
            viewHolder.groupDestAddress.setText(UserHome.reverseGeocoding(groupDestLocLat,groupDestLocLng,context));
        } catch (IOException e) {
            e.printStackTrace();
        }

        viewHolder.groupCapacityVal.setText(list.get(position).capacityVal);
        viewHolder.femaleOnlyFlag.setText(String.valueOf(list.get(position).femaleOnlyFlag));
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        Log.d(TAG,"onAttachedToRecyclerView");
        super.onAttachedToRecyclerView(recyclerView);
    }

    //View Holder for adapter
    class groupViewHolder extends RecyclerView.ViewHolder {
        TextView groupId;
        TextView groupOwnerId;
        TextView groupTranportMode;
        TextView groupSrcAddress;
        TextView groupDestAddress;
        TextView groupCapacityVal;
        TextView femaleOnlyFlag;

        groupViewHolder(View itemView)
        {
            super(itemView);
            Log.d(TAG,"groupViewHolder Constructor");
            groupId = (TextView) itemView.findViewById(R.id.GroupId);
            groupOwnerId = (TextView)itemView.findViewById(R.id.ownerId);
            groupTranportMode = (TextView)itemView.findViewById(R.id.tranportMode);
            groupSrcAddress = (TextView)itemView.findViewById(R.id.srcAddress);
            groupDestAddress = (TextView)itemView.findViewById(R.id.destAddress);
            groupCapacityVal = (TextView)itemView.findViewById(R.id.capacityVal);
            femaleOnlyFlag = (TextView)itemView.findViewById(R.id.femaleOnlyFlag);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Item clicked from Holder : " + getAdapterPosition());

                }
            });
        }
    }
}