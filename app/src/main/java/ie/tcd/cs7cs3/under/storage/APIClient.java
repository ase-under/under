package ie.tcd.cs7cs3.under.storage;

import com.google.common.collect.ImmutableList;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class APIClient {

  private static final String BASE_URL = "http://35.240.41.174";
//  private static final String BASE_URL = "http://localhost:8080"; // uncomment to run against local server
  static final MediaType MEDIATYPE_APPLICATION_JSON = MediaType.parse("application/json");

  private OkHttpClient client;
  private Moshi moshi;
  private String baseURL;
  private JsonAdapter<GroupPojo> groupPojoJsonAdapter;
  private JsonAdapter<List<GroupPojo>> groupPojoListJsonAdapter;

  /**
   * APIClient creates a new instance of APIClient By default, this talks to BASE_URL.
   */
  public APIClient() {
    this.client = new OkHttpClient();
    this.baseURL = BASE_URL; // TODO: allow specifying URL
    this.moshi = new Moshi.Builder().build();
    this.groupPojoJsonAdapter = moshi.adapter(GroupPojo.class);
    this.groupPojoListJsonAdapter = moshi
        .adapter(Types.newParameterizedType(List.class, GroupPojo.class));

  }

  /**
   * APIClient creates a new instance of APIClient that talks to the URL specified.
   */
  public APIClient(final String baseURL) {
    this.client = new OkHttpClient();
    this.baseURL = baseURL;
    this.moshi = new Moshi.Builder().build();
    this.groupPojoJsonAdapter = moshi.adapter(GroupPojo.class);
    this.groupPojoListJsonAdapter = moshi
        .adapter(Types.newParameterizedType(List.class, GroupPojo.class));
  }

  /**
   * groups returns the webtarget for the groups resource
   *
   * @return the groups webtarget
   */
  private String groups() {
    return String.format("%s/groups/", baseURL);
  }

  /**
   * group returns the webtarget for the given group
   *
   * @return the group webtarget
   */
  private String group(final long groupID) {
    return String.format("%s/groups/%d", baseURL, groupID);
  }

  /**
   * listGroups returns a list of all the known groups.
   *
   * @return List<GroupPojo>
   */
  public List<GroupPojo> list() throws IOException {
    final Request req = new Request.Builder()
        .url(groups())
        .build();
    final Response resp = client.newCall(req).execute();
    if (!resp.isSuccessful()) {
      throw new IOException("got response " + resp.code());
    }
    return groupPojoListJsonAdapter.fromJson(resp.body().source());
  }

  /**
   * getGroup gets an existing group by ID
   *
   * @param groupID the groupID to fetch
   * @return the existing GroupPojo.
   */
  public GroupPojo get(final long groupID) throws IOException {
    Request req = new Request.Builder()
        .url(group(groupID))
        .build();
    final Response resp = client.newCall(req).execute();
    if (!resp.isSuccessful()) {
      throw new IOException("got response " + resp.code());
    }
    return groupPojoJsonAdapter.fromJson(resp.body().source());
  }

  /**
   * createGroup creates a new group
   *
   * @param g the group to be created. ID parameter is ignored for obvious reasons.
   * @return the created GroupResponse.
   */
  public GroupPojo create(final GroupPojo g) throws IOException {
    final String groupJson = groupPojoJsonAdapter.toJson(g);
    Request req = new Request.Builder()
        .url(groups())
        .post(RequestBody.create(MEDIATYPE_APPLICATION_JSON, groupJson))
        .build();
    final Response resp = client.newCall(req).execute();
    if (!resp.isSuccessful()) {
      throw new IOException("got response " + resp.code());
    }
    return groupPojoJsonAdapter.fromJson(resp.body().source());
  }

  /**
   * updateGroup updates the given group
   *
   * @param g the group to be updated.
   * @return the updated GroupPojo
   */
  public GroupPojo update(final GroupPojo g) throws IOException {
    final String groupJson = groupPojoJsonAdapter.toJson(g);
    Request req = new Request.Builder()
        .url(group(g.groupId))
        .post(RequestBody.create(MEDIATYPE_APPLICATION_JSON, groupJson))
        .build();
    final Response resp = client.newCall(req).execute();
    if (!resp.isSuccessful()) {
      throw new IOException("got response " + resp.code());
    }
    return groupPojoJsonAdapter.fromJson(resp.body().source());
  }

  /**
   * delete deletes the given group
   *
   * @param g the group to be deleted
   */
  public void delete(final GroupPojo g) throws IOException {
    Request req = new Request.Builder()
        .url(group(g.groupId))
        .delete()
        .build();
    final Response resp = client.newCall(req).execute();
    if (!resp.isSuccessful()) {
      throw new IOException("got response " + resp.code());
    }
  }

  // this is just for doing a quick and dirty test
  // to run it, uncomment and right-click -> run *with coverage*
//  public static void main(String[] args) throws IOException {
//    final APIClient c = new APIClient();
//    for (final GroupPojo g : c.list()) {
//      System.out.println(
//          String.format(
//              "existing: %d %s %s %s %d %d %s",
//              g.groupId,
//              g.groupState,
//              g.points,
//              g.memberUUIDs,
//              g.createTime,
//              g.depTime,
//              g.restrictions));
//    }
//    GroupPojo g = new GroupPojo();
//    g.groupId = 9999;
//    g.groupState = "FORMING";
//    g.points = "MULTIPOINT ((12 34))";
//    g.memberUUIDs = ImmutableList.of("cafed00d-cafe-d00d-cafe-d00dcafed00d");
//    g.createTime = System.currentTimeMillis() / 1000;
//    g.depTime = g.createTime + 3600;
//    g.restrictions = new HashMap<>();
//    g.restrictions.put("MaxPeople", 3);
//    g = c.create(g);
//    System.out.println(
//        String.format(
//            "create: %d %s %s %s %d %d %s",
//            g.groupId,
//            g.groupState,
//            g.points,
//            g.memberUUIDs,
//            g.createTime,
//            g.depTime,
//            g.restrictions));
//    c.get(g.groupId);
//    System.out.println(
//        String.format(
//            "get: %d %s %s %s %d %d %s",
//            g.groupId,
//            g.groupState,
//            g.points,
//            g.memberUUIDs,
//            g.createTime,
//            g.depTime,
//            g.restrictions));
//    g.groupState = "MOVING";
//    g = c.update(g);
//    System.out.println(
//        String.format(
//            "update: %d %s %s %s %d %d %s",
//            g.groupId,
//            g.groupState,
//            g.points,
//            g.memberUUIDs,
//            g.createTime,
//            g.depTime,
//            g.restrictions));
//    c.delete(g);
//  }
}
