package ie.tcd.cs7cs3.under.appViews.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.UnderMainActivity;
import ie.tcd.cs7cs3.under.appViews.activities.Login;
import ie.tcd.cs7cs3.under.appViews.activities.SignUp;
import ie.tcd.cs7cs3.under.appViews.activities.UserHome;
import ie.tcd.cs7cs3.under.appViews.model.ListModel;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    Context context;
    private ArrayList<ListModel> listModelArrayList;

    public ListAdapter(Context context, ArrayList<ListModel> listModelArrayList) {
        this.context = context;
        this.listModelArrayList = listModelArrayList;
    }

    @NonNull
    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.ViewHolder holder, final int position) {
        holder.title.setText(listModelArrayList.get(position).getTitle());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String txt[] = {"Login", 0
//                        "Signup", 1
//                        "Home", 2
//                        "In-ride(Active ride)", 3
//                        "Ride Complete Rating", 4
//                        "Ride History", 5
//                        "Menu", 6
//                        "Old code integration" 7
//                };

                if(position==0){
                    Intent i = new Intent(context,  Login.class);
                    context.startActivity(i);}
                if(position==1){
                    Intent i = new Intent(context, SignUp.class); //RegisterActivity.class);//
                    context.startActivity(i);}
                if(position==2){
                    Intent i = new Intent(context, UserHome.class);
                    context.startActivity(i);}
//                if(position==3){
//                    Intent i = new Intent(context, MenuHome.class);
//                    context.startActivity(i);}
//                if(position==4){
//                    Intent i = new Intent(context, RegisterActivity.class);
//                    context.startActivity(i);}
//                if(position==2){
//                    Intent i = new Intent(context, Home.class);
//                    context.startActivity(i);}
//                if(position==3){
//                    Intent i = new Intent(context, In_Ride.class);
//                    context.startActivity(i);}
//                if(position==4){
//                    Intent i = new Intent(context,Ride_complete_rating.class);
//                    context.startActivity(i);}
//                if(position==5){
//                    Intent i = new Intent(context, Ride_History.class);
//                    context.startActivity(i);}
//                if(position==6){
//                    Intent i = new Intent(context, Menu1.class);
//                    context.startActivity(i);}
                if(position==7){
                    Intent i = new Intent(context, UnderMainActivity.class);
                    context.startActivity(i);}

            }
        });

    }

    @Override
    public int getItemCount() {
        return listModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);

            title=itemView.findViewById(R.id.title);
        }
    }
}
