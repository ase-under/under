package ie.tcd.cs7cs3.under.nearby;

public class NearbyMsg {

  public static final String REQUEST_GROUP = "REQUEST_GROUP";
  public static final String GROUP_AVAILABLE = "GROUP_AVAILABLE";
  public static final String NO_GROUP_AVAILBLE = "NO_GROUP_AVAILABLE";
  public static final String REQUEST_JOIN_GROUP = "REQUEST_JOIN_GROUP";
  public static final String REQUEST_LEAVE_GROUP = "REQUEST_LEAVE_GROUP";
  public static final String GROUP_JOIN_APPROVED = "GROUP_JOIN_APPROVED";
  public static final String GROUP_JOIN_DENIED = "GROUP_JOIN_DENIED";
  public static final String GROUP_LEAVE_APPROVED = "GROUP_LEAVE_APPROVED";
  public static final String GROUP_LEAVE_DENIED = "GROUP_LEAVE_DENIED";

  private String msgType;
  private String data;

  public NearbyMsg(String msgType, String data) {
    this.msgType = msgType;
    this.data = data;
  }

  public String getMsgType() {
    return msgType;
  }

  public String getData() {
    return data;
  }
}
