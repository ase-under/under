package ie.tcd.cs7cs3.under.storage;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {
    @GET("/groups")
    Call<List<GroupInfo_POJO>> getAllGroups();
}