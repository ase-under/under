package ie.tcd.cs7cs3.under.storage;

import java.util.Objects;

public class UserPojo {
  public String uuid;
  public String gender;

  public UserPojo(String uuid, String gender) {
    this.uuid = uuid;
    this.gender = gender;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserPojo userPojo = (UserPojo) o;
    return Objects.equals(uuid, userPojo.uuid) &&
        Objects.equals(gender, userPojo.gender);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid, gender);
  }

  @Override
  public String toString() {
    return "UserPojo{" +
        "uuid='" + uuid + '\'' +
        ", gender='" + gender + '\'' +
        '}';
  }
}
