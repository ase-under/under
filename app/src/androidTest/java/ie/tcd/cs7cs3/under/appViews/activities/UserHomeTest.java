package ie.tcd.cs7cs3.under.appViews.activities;

import android.view.View;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import ie.tcd.cs7cs3.under.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isOpen;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static ie.tcd.cs7cs3.under.appViews.activities.LoginTest.hasTextInputLayoutHintText;
import static org.hamcrest.Matchers.startsWith;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class UserHomeTest {
    private int leftLimit = 97; // letter 'a'
    private int rightLimit = 122; // letter 'z'
    private int targetStringLength = 10;
    private Random random = new Random();
    private String stringToBetyped = random.ints(leftLimit, rightLimit + 1)
            .limit(targetStringLength)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();


    @Rule
    public ActivityTestRule<Login> loginActivityTestRule= new ActivityTestRule<>(Login.class);



    @Before
    public void setUp()  {
        loginActivityTestRule.getActivity();
        onView(withText(startsWith("Sign up for a new"))).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isEnabled(); // no constraints, they are checked above
            }

            @Override
            public String getDescription() {
                return "click plus button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });

        onView(withId(R.id.textInputEditTextName)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextEmail)).perform(typeText(stringToBetyped+"@"+stringToBetyped+".com"), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextConfirmPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonRegister)).perform(click());



        onView(withId(R.id.textInputEditTextName)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());

        onView(withText(startsWith("Log In"))).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isEnabled(); // no constraints, they are checked above
            }

            @Override
            public String getDescription() {
                return "click plus button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });
    }

    @Test
    public void test_launch_mapview()
    {
        try {
            Thread.sleep(3000);
            onView(withId(R.id.mapView)).check(matches(isDisplayed()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testNavigateToScrollingActivity()  {
        onView(withId(R.id.home_drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.home_drawer_layout)).check(matches(isOpen()));
        onView(withId(R.id.logout)).check((matches(isDisplayed())));

//        onView(withId(R.id.navigation_view)).perform(NavigationViewActions.navigateTo(R.id.navigation_view));
    }


    public void test_logout()
    {
        onView(withId(R.id.home_drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.home_drawer_layout)).check(matches(isOpen()));
        onView(withId(R.id.logout)).check((matches(isDisplayed())));
        onView(withId(R.id.logout)).perform(click());
        onView(withId(R.id.textInputLayoutName)).check(matches(hasTextInputLayoutHintText("Enter Name")));

    }
    @After
    public void tearDown() {
    }
}