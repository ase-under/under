package ie.tcd.cs7cs3.under.appViews.activities;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.google.android.material.textfield.TextInputLayout;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.appViews.ActivityList;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.core.internal.deps.dagger.internal.Preconditions.checkNotNull;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class LoginTest {


    private int leftLimit = 97; // letter 'a'
    private int rightLimit = 122; // letter 'z'
    private int targetStringLength = 10;
    private Random random = new Random();

    private String stringToBetyped = random.ints(leftLimit, rightLimit + 1)
            .limit(targetStringLength)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();

//
//    @Rule
//    public ActivityTestRule<SignUp> signUpActivityTestRule= new ActivityTestRule<>(SignUp.class);
//    private SignUp SignUp=null;
    @Rule
    public ActivityTestRule<Login> loginActivityTestRule= new ActivityTestRule<>(Login.class);
    private Login login=null;

    public static Matcher<View> atPosition(final int position, @NonNull final Matcher<View> itemMatcher) {
        checkNotNull(itemMatcher);
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
                if (viewHolder == null) {
                    // has no item on such position
                    return false;
                }
                return itemMatcher.matches(viewHolder.itemView);
            }
        };
    }




    public static Matcher<View> hasTextInputLayoutHintText(final String expectedErrorText) {
        return new TypeSafeMatcher<View>() {

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextInputLayout)) {
                    return false;
                }

                CharSequence error = ((TextInputLayout) view).getHint();

                if (error == null) {
                    return false;
                }

                String hint = error.toString();

                return expectedErrorText.equals(hint);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }



    public static Matcher<View> hasTextInputLayoutErrorText(final String expectedErrorText) {
        return new TypeSafeMatcher<View>() {

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextInputLayout)) {
                    return false;
                }

                CharSequence error = ((TextInputLayout) view).getError();

                if (error == null) {
                    return false;
                }

                String hint = error.toString();

                return expectedErrorText.equals(hint);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

            @Before
    public void setUp() {
//        SignUp= signUpActivityTestRule.getActivity();
            login =loginActivityTestRule.getActivity();
//                activityListActivityTestRule.getActivity();

//                onView(withId(R.id.recyclerView1)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

    }

@Test
    public void test_username_view(){
         View view1 = login.findViewById(R.id.textInputEditTextName);
         assertNotNull(view1);
    }

    @Test
    public void test_pass_view(){
        View view = login.findViewById(R.id.textInputEditTextPassword);
        assertNotNull(view);
    }

    @Test
    public void test_button_view(){
        View view = login.findViewById(R.id.appCompatButtonLogin);
        assertNotNull(view);
    }



    @Test
    public void test_InputLayoutName_changed_Displayed()
    {
        onView(withId(R.id.textInputLayoutName)).check
                (matches(hasTextInputLayoutHintText("Enter Name")));
    }
    @Test
    public void test_InputLayoutPassword_changed_Displayed()
    {
        onView(withId(R.id.textInputLayoutPassword)).check(matches(hasTextInputLayoutHintText("Password")));
    }


    @Test
    public void test_textInputEditTextName_changed_Displayed()
    {
        onView(withId(R.id.textInputEditTextName)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextName), withText(stringToBetyped))).check(matches(withText(stringToBetyped)));
    }
    @Test
    public void test_textInputEditTextPassword_Displayed()
    {

        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextPassword), withText(stringToBetyped))).check(matches(withText(stringToBetyped)));

    }

    @Test
    public void test_appCompatButtonLogin_text()
    {
        AppCompatButton appCompatButton = login.findViewById(R.id.appCompatButtonLogin);
        assertEquals("Button text is matched","Log In",appCompatButton.getText().toString());
    }

    @Test
    public void test_textViewLinkRegister_text()
    {
        AppCompatTextView appCompatTextView= login.findViewById(R.id.textViewLinkRegister);
        assertEquals("Link text matched","Sign up for a new account",appCompatTextView.getText().toString());
    }

    @Test
    public void test_clickSignup()
    {
//        AppCompatTextView appCompatTextView= login.findViewById(R.id.textViewLinkRegister);
//        onView(withId(R.id.textViewLinkRegister)).check(matches(withText("Sign up")));
        onView(withText(startsWith("Sign up for a new"))).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isEnabled(); // no constraints, they are checked above
            }

            @Override
            public String getDescription() {
                return "click plus button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });
//        appCompatTextView.performClick();
        onView(withId(R.id.welcome)).check(matches(isDisplayed()));
    }


    @Test
    public void test_snackbar_wrongcredential()
    {
        onView(withId(R.id.textInputEditTextName)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.appCompatButtonLogin)).perform(click());
        onView(withId(com.google.android.material.R.id.snackbar_text))
                .check(matches(withText(R.string.error_valid_name_password)));
    }

    @Test
    public void test_clickLogin_success()
    {
//        AppCompatTextView appCompatTextView= login.findViewById(R.id.textViewLinkRegister);
        onView(withText(startsWith("Sign up for a new"))).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isEnabled(); // no constraints, they are checked above
            }

            @Override
            public String getDescription() {
                return "click plus button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });

        onView(withId(R.id.textInputEditTextName)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextEmail)).perform(typeText(stringToBetyped+"@"+stringToBetyped+".com"), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextConfirmPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());

        onView(withId(R.id.appCompatButtonRegister)).perform(click());



        onView(withId(R.id.textInputEditTextName)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());

        onView(withText(startsWith("Log In"))).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isEnabled(); // no constraints, they are checked above
            }

            @Override
            public String getDescription() {
                return "click plus button";
            }

            @Override
            public void perform(UiController uiController, View view) {
                view.performClick();
            }
        });
//        appCompatTextView.performClick();
        onView(withId(R.id.mapView)).check(matches(isDisplayed()));
    }



    @After
    public void tearDown(){
                login=null;
    }
}