package ie.tcd.cs7cs3.under.appViews.activities;

import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Random;

import ie.tcd.cs7cs3.under.R;
import ie.tcd.cs7cs3.under.appViews.ActivityList;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.*;

public class SignUpTest {

    private int leftLimit = 97; // letter 'a'
    private int rightLimit = 122; // letter 'z'
    private int targetStringLength = 10;
    Random random = new Random();

    String stringToBetyped = random.ints(leftLimit, rightLimit + 1)
            .limit(targetStringLength)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();




    @Rule
    public ActivityTestRule<SignUp> signUpActivityTestRule= new ActivityTestRule<>(SignUp.class);
    private SignUp SignUp=null;

    @Before
    public void setUp() throws Exception {

        SignUp=signUpActivityTestRule.getActivity();
    }

    @Test
    public void text_InputLayouts(){
        View view1 = SignUp.findViewById(R.id.textInputLayoutName);
        assertNotNull(view1);
        view1 = SignUp.findViewById(R.id.textInputLayoutEmail);
        assertNotNull(view1);
        view1 = SignUp.findViewById(R.id.textInputLayoutPassword);
        assertNotNull(view1);
        view1 = SignUp.findViewById(R.id.textInputLayoutConfirmPassword);
        assertNotNull(view1);
        view1 = SignUp.findViewById(R.id.textInputEditTextName);
        assertNotNull(view1);
        view1 = SignUp.findViewById(R.id.textInputEditTextEmail);
        assertNotNull(view1);
        view1 = SignUp.findViewById(R.id.textInputEditTextPassword);
        assertNotNull(view1);
        view1 = SignUp.findViewById(R.id.textInputEditTextConfirmPassword);
        assertNotNull(view1);
        view1 = SignUp.findViewById(R.id.spinnerGender);
        assertNotNull(view1);
    }


    @Test
    public void test_SignUp_view(){
        View view = SignUp.findViewById(R.id.appCompatButtonRegister);
        assertNotNull(view);
    }

    @Test
    public void test_appCompatTextViewLoginLink_text()
    {
        AppCompatTextView appCompatTextView= SignUp.findViewById(R.id.appCompatTextViewLoginLink);
        assertEquals("Link text matched","Login now",appCompatTextView.getText().toString());
    }

    @Test
    public void test_welcome_text()
    {
        AppCompatTextView appCompatTextView= SignUp.findViewById(R.id.welcome);
        assertEquals("Link text matched","Welcome Aboard!",appCompatTextView.getText().toString());
    }

    @Test
    public void test_signup_enterdetails()
    {
        onView(withId(R.id.textInputEditTextName)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextName), withText(stringToBetyped))).check(matches(withText(stringToBetyped)));

        onView(withId(R.id.textInputEditTextEmail)).perform(typeText(stringToBetyped+"@"+stringToBetyped+".com"), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextEmail), withText(stringToBetyped+"@"+stringToBetyped+".com"))).check(matches(withText(stringToBetyped+"@"+stringToBetyped+".com")));

        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextPassword), withText(stringToBetyped))).check(matches(withText(stringToBetyped)));

        onView(withId(R.id.textInputEditTextConfirmPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextConfirmPassword), withText(stringToBetyped))).check(matches(withText(stringToBetyped)));


    }


    @Test
    public void test_spinner_other()
    {
        onView(withId(R.id.spinnerGender)).perform(click());
        onData(anything()).atPosition(1).perform(click());
        onView(withId(R.id.spinnerGender)).check(matches(withSpinnerText(containsString("Others"))));
    }

    @Test
    public void test_spinner_female()
    {
        onView(withId(R.id.spinnerGender)).perform(click());
        onData(anything()).atPosition(0).perform(click());
        onView(withId(R.id.spinnerGender)).check(matches(withSpinnerText(containsString("Female"))));
    }

    @Test
    public void test_signup_success()
    {
        onView(withId(R.id.textInputEditTextName)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextName), withText(stringToBetyped))).check(matches(withText(stringToBetyped)));

        onView(withId(R.id.textInputEditTextEmail)).perform(typeText(stringToBetyped+"@"+stringToBetyped+".com"), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextEmail), withText(stringToBetyped+"@"+stringToBetyped+".com"))).check(matches(withText(stringToBetyped+"@"+stringToBetyped+".com")));

        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextPassword), withText(stringToBetyped))).check(matches(withText(stringToBetyped)));

        onView(withId(R.id.textInputEditTextConfirmPassword)).perform(typeText(stringToBetyped), closeSoftKeyboard());
        onView(allOf(withId(R.id.textInputEditTextConfirmPassword), withText(stringToBetyped))).check(matches(withText(stringToBetyped)));


        onView(withId(R.id.appCompatButtonRegister)).perform(click());

        onView(withText(startsWith("Log In"))).check(matches(isDisplayed()));
    }









    @After
    public void tearDown() throws Exception {
        SignUp=null;
    }
}